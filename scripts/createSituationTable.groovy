import org.globalbraininstitute.challprop.utils.Utils

def t = System.currentTimeMillis()
def numberOfItems = 30000
Utils.generateSituationVectorTable(numberOfItems);
println "Generation of one vector took: " + (System.currentTimeMillis() - t)/numberOfItems + " milliseconds"
