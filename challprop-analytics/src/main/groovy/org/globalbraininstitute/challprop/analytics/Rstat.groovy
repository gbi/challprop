package org.globalbraininstitute.challprop.analytics

import org.globalbraininstitute.challprop.global.Parameters
import org.rosuda.REngine.RList
import org.rosuda.REngine.Rserve.RConnection
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Rstat {
	
	static Logger logger = LoggerFactory.getLogger(Rstat.class)
	
	public static boolean histogram(ArrayList data, String name, TreeMap pars) {
		RConnection re = new RConnection();
		
		re.assign("Data", ((double[]) data))
		re.assign("title",name)
		
		//logger.debug("Vector assigned to R variable: {}", re.eval("vector").asDoubleArray())
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			def analysisPath = Parameters.parameters.simulationHome + "analysis/"
			(new File(analysisPath)).mkdirs()
			re.assign("pathToFile", analysisPath + name.replaceAll("\\s","") + ".png")
			re.eval("png(filename=pathToFile)")
		}
		
		re.eval("hist(Data,"+\
			"main=paste(title),"+\
			"xlab = '',"+\
			"ylab = '',"+\
			"labels=TRUE,"+\
			"col='gray')")
		
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			re.eval("dev.off()")
		}
		
		re.close()
		
		return true
		
	}
	
	public static Map chiSquareGoodnessOfFit(ArrayList data, String name, TreeMap pars) {
		logger.debug("Trying chiSquare test on this: {}", data.toString())
		RConnection re = new RConnection();
		
		re.assign("Data", ((double[]) data))
		
		RList test = re.eval("test <- chisq.test(Data)").asList()
		String[] attributes = test.keys()
		def result = [:]
		for (int s = 0; s<3; s++) { // we need first three parameters of the test
			result.put(attributes[s],test.at(s).asDouble())
		}
		//and then the name of the test
		result.put(attributes[3],test.at(3).asString())
		//and original data
		result.put(attributes[5],test.at(5).asDoubles())
		
		re.close()
		logger.debug("Ran an R script for calculating Goodness Of Fit: {}", name)
		
		return result
		
	}

}
