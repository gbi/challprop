```{r get-configuration, echo=FALSE}
# Read challprop.conf from ramdisk
setwd('/media/ramdisk/')
conf.df <- read.table(file = "challprop.conf",header=F,sep='=')
v <-as.vector(conf.df[,2])
conf <-as.list(v)
names(conf) <- as.list(as.vector(conf.df[,1]))
Sys.setenv(SIM_HOME=conf$simulationHome)
Sys.setenv(EXP_PATH=conf$experimentPath)
```

# Experiment `r conf$simulationID`
Directory on disk: `r conf$simulationHome`


## Basic Statistics:
```{r groovy-basic, engine='groovy', echo=FALSE}
def analysisDataFile = '/media/ramdisk/analysisDataTemp.dat'
Map analysisData = new ConfigSlurper('analysisData').parse(new File(analysisDataFile).toURI().toURL())

println "\nNUMBER OF VERTIXES:"
analysisData.vertexes.each{
  println it.key + "=" + it.value
}
println "\nNUMBER OF EDGES:"
analysisData.edges.each{
  println it.key + "=" + it.value
}

println "\nPERFORMANCE:"
def simulationTime = analysisData.time.finishTimeSeconds.toInteger() -analysisData.time.startTimeSeconds.toInteger()
println "Simulation time in secs: " + simulationTime
def atomicEvents = 0
analysisData.edges.each{atomicEvents += it.key =="knowsLinks"?0:it.value.toInteger()}
println "Number of atomic events: "+ atomicEvents
println "Time per atomic event (secs): "+ simulationTime / atomicEvents
```

## Average benefit change:
```{r groovy-average, engine='groovy', echo=FALSE}
def analysisDataFile = '/media/ramdisk/analysisDataTemp.dat'
Map analysisData = new ConfigSlurper('analysisData').parse(new File(analysisDataFile).toURI().toURL())
println "\nAVERAGE TOTAL BENEFIT:"
println analysisData.averageBenefit
``` 

```{r plot-benefit, echo=FALSE}
setwd(Sys.getenv("EXP_PATH"))
source('analytics/avarage_benefit.R')
plot
```
## Average benefit by category:

### Cluster C1:

```{r groovy-c1, engine='groovy', echo=FALSE}
def analysisDataFile = '/media/ramdisk/analysisDataTemp.dat'
Map analysisData = new ConfigSlurper('analysisData').parse(new File(analysisDataFile).toURI().toURL())
println "\nAVERAGE BENEFIT CLUSTER #1:"
println analysisData.averageBenefitC1
```

```{r beenfit-c1, echo=FALSE}
setwd(Sys.getenv("EXP_PATH"))
source('analytics/average_benefitC1.R')
plotC1
```

### Cluster C2:

```{r groovy-c2, engine='groovy', echo=FALSE}
def analysisDataFile = '/media/ramdisk/analysisDataTemp.dat'
Map analysisData = new ConfigSlurper('analysisData').parse(new File(analysisDataFile).toURI().toURL())
println "\nAVERAGE BENEFIT CLUSTER #2:"
println analysisData.averageBenefitC2
```

```{r beenfit-c2, echo=FALSE}
setwd(Sys.getenv("EXP_PATH"))
source('analytics/average_benefitC2.R')
plotC2
```

## All simulation parameters:
```{r all-parameters, echo=FALSE}
# Read challprop.conf from ramdisk
setwd('/media/ramdisk/')
conf.df <- read.table(file = "challprop.conf",header=F,sep='=')
v <-as.vector(conf.df[,2])
conf <-as.list(v)
names(conf) <- as.list(as.vector(conf.df[,1]))
conf
```











