package org.globalbraininstitute.challprop.analytics

import com.thinkaurelius.titan.core.TitanFactory
import com.tinkerpop.pipes.util.structures.Table

import java.nio.file.Files

com.tinkerpop.gremlin.groovy.Gremlin.load()

print "Openning graph database.."
g = TitanFactory.open("../../graph")
println "...Done"

def analysisDataTemp = new File('/media/ramdisk/analysisDataTemp.dat')
analysisDataTemp << "// basicStatistics.groovy START\n\n"

print "Getting basic statistics + writing to file.."
m=[:]
g.V.type.groupCount(m).iterate()
analysisDataTemp << "vertexes { \n"
m.each{
	analysisDataTemp << it.key + "=" + it.value + "\n"
}
analysisDataTemp << "}\n"

analysisDataTemp << "edges { \n"

def knowsLinks = g.V('type','agent').outE('knows').unique.count()
analysisDataTemp << 'knowsLinks='+knowsLinks+ "\n"

def challenged = g.V('type','agent').inE('challenged').count()
analysisDataTemp << 'challenged='+challenged+ "\n"

def interpreted = g.V('type','agent').outE('interpreted').count()
analysisDataTemp << 'interpreted='+interpreted+ "\n"

def buffered = g.V('type','agent').outE('buffered').count()
analysisDataTemp << 'buffered='+buffered+ "\n"

def processed = g.V('type','agent').outE('processed').count()
analysisDataTemp << 'processed='+processed+ "\n"

def reinterpreted = g.V('type','agent').outE('reinterpreted').count()
analysisDataTemp << 'reinterpreted='+reinterpreted+ "\n"

def forgot = g.V('type','agent').outE('forgot').count()
analysisDataTemp << 'forgot='+forgot+ "\n"
analysisDataTemp << "}\n" 

analysisDataTemp << "time { \n"	
def startTimeSeconds = g.V('type','god').next().simulationStart * 1e-9
analysisDataTemp << 'startTimeSeconds='+startTimeSeconds+ "\n"

def finishTimeSeconds = g.V('type','god').next().simulationFinish * 1e-9
analysisDataTemp << 'finishTimeSeconds='+finishTimeSeconds+ "\n"

analysisDataTemp << "}\n" 
println "...Done"

analysisDataTemp << "// basicStatistics.groovy FINISH\n\n"

analysisDataTemp << 'averageBenefit='+g.V('type','agent').benefit.sum()/g.V('type','agent').count()+ "\n"
analysisDataTemp << 'averageBenefitC1='+g.V('type','agent').has('cluster','c1').benefit.sum()/g.V('type','agent').has('cluster','c1').count()+ "\n"
analysisDataTemp << 'averageBenefitC2='+g.V('type','agent').has('cluster','c2').benefit.sum()/g.V('type','agent').has('cluster','c2').count()+ "\n"


analysisDataTemp <<"// "
 
//benefit analysis

penalties = new Table();g.V('type','agent').as('agent').cluster.as('cluster').back(1).outE('penalized').label.as('event').back(1).timestamp.as('timestamp').back(1).inV.value.as('penalty').table(penalties).iterate()
benefits = new Table();g.V('type','agent').as('agent').cluster.as('cluster').back(1).outE('benefitted').label.as('event').back(1).timestamp.as('timestamp').back(1).inV.value.as('benefit').table(benefits).iterate()

bfile = new FileWriter("/media/ramdisk/benefitsPenalties.dat")
bfile << "agent,cluster,timestamp,event,value\n"
penalties.each{row->
	bfile << row.getColumn('agent').toString() +","+
	row.getColumn('cluster').toString()+","+
	row.getColumn('timestamp').toString()+","+
	row.getColumn('event').toString()+","+
	row.getColumn('penalty').toString() + "\n"
}
benefits.each{row->
	bfile << row.getColumn('agent').toString() +","+
	row.getColumn('cluster').toString()+","+
	row.getColumn('timestamp').toString()+","+
	row.getColumn('event').toString()+","+
	row.getColumn('benefit').toString() + "\n"
}
bfile.close()

//link weights analysis

weights = new Table();g.V('type','agent').outE('knows').as('knowsLink').weight.as('weight').back(1).inV().as('fromAgent').cluster.as('fromCluster').back(2).outV().as('toAgent').cluster.as('toCluster').table(weights).iterate()
wfile = new FileWriter("/media/ramdisk/linkWeights.dat")
wfile<<"knowsLink,weight,fromAgent,fromCluster,toAgent,toCluster\n"
weights.each{row->
	wfile<<row.getColumn('knowsLink').toString() +","+
			row.getColumn('weight').toString() +","+
			row.getColumn('fromAgent').toString() +","+
			row.getColumn('fromCluster').toString() +","+
			row.getColumn('toAgent').toString() +","+
			row.getColumn('toCluster').toString()+ "\n"
}
wfile.close()

print "Copying analysis of results to experiment's directory on the hard disk...."
new File("analysis").mkdirs()

def benefitsPenaltiesFile =  new File("analysis/benefitsPenalties.dat")
if (benefitsPenaltiesFile.exists()) { benefitsPenaltiesFile.delete() }
Files.copy(new File("/media/ramdisk/benefitsPenalties.dat").toPath(),benefitsPenaltiesFile.toPath())

def analysisDataPerm =  new File("analysis/analysisDataTemp.dat")
if (analysisDataPerm.exists()) { analysisDataPerm.delete() }
Files.copy(analysisDataTemp.toPath(),analysisDataPerm.toPath())

def linkWeightsPerm =  new File("analysis/linkWeights.dat")
if (linkWeightsPerm.exists()) { linkWeightsPerm.delete() }
Files.copy(new File("/media/ramdisk/linkWeights.dat").toPath(),linkWeightsPerm.toPath())


println "...Done"

