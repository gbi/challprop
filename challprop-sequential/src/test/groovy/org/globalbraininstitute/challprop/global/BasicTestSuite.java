package org.globalbraininstitute.challprop.global;

import org.globalbraininstitute.challprop.frames.FrameInitializationTest;
import org.globalbraininstitute.challprop.frames.FrameMethodsTest;
import org.globalbraininstitute.challprop.functions.*;
import org.globalbraininstitute.challprop.utils.EnvironmentTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)
@SuiteClasses(value = {
		EnvironmentTest.class,
		
		BufferingTest.class ,
		AgentsWithChallengesTest.class,
		ChallengeGenerationTest.class,
		IntensityTest.class,
		InterpretationTest.class,
		PenaltyTest.class,
		PreferenceUpdateTest.class,
		PreprocessingTest.class,
		ProcessingByMultiplicationTest.class,
		PropagationTest.class,
		ReinterpretationTest.class,
		SelectionAgentBasedTest.class,
		SituationUpdateTest.class,
		FilterOutTest.class,
		ChallengeUpdateTest.class,

		FrameInitializationTest.class,
		ParametersTest.class,
		FrameMethodsTest.class

		})

public class BasicTestSuite {
	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}

    @AfterClass 
    public static void tearDownClass() {
    	Globals.network.reinit();
        Globals.network.commit();
    }
}
