package org.globalbraininstitute.challprop.global;

import org.globalbraininstitute.challprop.utils.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(value = Suite.class)
@SuiteClasses(value = {
		UtilsTest.class,
		AlgorithmsTest.class,
		EnvironmentTest.class,
		ParametersTest.class,
		FunctionsTest.class,
		StepsTest.class,
		AnalysisTest.class

		})

public class UtilsTestSuite {
	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}

    @AfterClass 
    public static void tearDownClass() {
        Globals.network.commit();
    }
}
