package org.globalbraininstitute.challprop.global

import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ParametersTest {
	private static Logger logger = LoggerFactory.getLogger(ParametersTest.class)

	@Test
	public void parametersTest() {
		assert Parameters.parameters.decayFactor == 1
	}
	
}
