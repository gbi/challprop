package org.globalbraininstitute.challprop.global;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UtilsTestSuite.class,
				BasicTestSuite.class,
				AnalyticsTestSuite.class,
				ScenarioTestSuite.class
	})
    public class CompleteTestSuite {
	
    @AfterClass 
    public static void tearDownClass() {
    	Globals.network.clear();
        Globals.network.commit();
    }

}
