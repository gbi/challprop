package org.globalbraininstitute.challprop.global;

import org.globalbraininstitute.challprop.scenarios.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(value = Suite.class)
@SuiteClasses({
		EquivalentAgentsTestSeparateSituationGeneration.class,
		EquivalentAgentsTestBulkSituationGeneration.class,
		ControlledPropagationTestScenario1TestMethods.class,
		ControlledPropagationTestScenario2TestMethods.class,
		ControlledPropagationTestScenario31TestMethods.class,
		ControlledPropagationTestScenario32TestMethods.class
		})

public class ScenarioTestSuite {
	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}

    @AfterClass 
    public static void tearDownClass() {
        Globals.network.commit();
    }
    
}
