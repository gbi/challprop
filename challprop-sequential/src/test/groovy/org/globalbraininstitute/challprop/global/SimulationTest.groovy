package org.globalbraininstitute.challprop.global

import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class SimulationTest {
	
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(SimulationTest.class)
	static Integer processingStepsAllowed
	static RealVector needVector

	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}
	
	@Test
	public void run() {
		def agentList = NetworkMocks.createNetworkMock6(1000)
		
		for (int i = 0; i <=100; i++) {
			ControlledSimulation.generation(10)
			ControlledSimulation.propagation(agentList,1)
		}

	} 
	
    @AfterClass 
    public static void tearDownClass() {
        network.commit();
    }
}
