package org.globalbraininstitute.challprop.global;

import org.globalbraininstitute.challprop.analytics.RstatTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(value = Suite.class)
@SuiteClasses({
		RstatTest.class
		})

public class AnalyticsTestSuite {
	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}

    @AfterClass 
    public static void tearDownClass() {
        Globals.network.commit();
    }
    
}
