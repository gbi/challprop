package org.globalbraininstitute.challprop.utils

import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.annotations.AfterClass

class FunctionsTest {
	static Logger logger = LoggerFactory.getLogger(FunctionsTest.class)
	static Network network = Globals.network
	
	@BeforeClass
	public static void init() {
		def agentList = NetworkMocks.createNetworkMock6(5)
		(new ControlledSimulation(agentList)).basicRun(10,3)
	}
	
	@Test
	public void testChallengesFunction() {
		println Functions.challenges(network.g.baseGraph)
	}
	
	@Test
	public void testFurtherAllFunction() {
		def challenges = network.god.asVertex().outE.has('label','created').inV.has('type','situation').outE.has('source',network.god.getIdentity()).toList()
		def prop = Functions.furtherAll(challenges[0])
		println prop
	}
	
	@Test
	public void testNextStep() {
		def challenges = network.god.asVertex().outE.has('label','created').inV.has('type','situation').outE.has('source',network.god.getIdentity()).toList()
		def start = challenges[0]
		println start
		def s = Functions.nextStep(start)
		println s
		while (true) {
			s = Functions.nextStep(s)
			println s
			if ( s == null) break
		}
	}
	
	@Test
	public void testPreviousStep() {
		def challenges = network.god.asVertex().outE.has('label','created').inV.has('type','situation').outE.has('source',network.god.getIdentity()).toList()
		def further = Functions.furtherAll(challenges[0])
		def start = further.reverse()[0]
		println start
		def s = Functions.previousStep(start)
		println s
		while (true) {
			s = Functions.previousStep(s)
			println s
			if ( s == null) break
		}
	}

	@AfterClass
	public static void commit() {
		network.reinit()
		network.commit()
	}

}
