package org.globalbraininstitute.challprop.utils

import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.utils.Algorithms as G
import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

class AlgorithmsTest {
	static Logger logger = LoggerFactory.getLogger(AlgorithmsTest.class)

	@Ignore
	@Test
	public void testNewPWVector() {
		def vector = Algorithms.newPWVector(10, 2.0, 0.05, 0.5, 0.2)
		assertNotNull(vector)
		logger.debug("Vector with components from powerlaw distribution: {}", vector)
	}
	
	@Test
	public void testTryAlgorithms() {
		(1..3).each{ 
			def matrix = G.processMatrixSparseGeneration(0.5,0.5)
			logger.debug("Processing matrix with components from powerlaw distribution: {}", matrix)
			def matrixString = Utils.serializeMatrix(matrix)
			logger.debug("Deserialized processing matrix: {}", matrixString)
		}
	}
	
	@Test
	public void testNewPresetVector() {
		def dimensions = 10
		def preset = 2.0
		RealVector vector= G.newPresetVector(preset)
		
		for (int i=0;i<dimensions;i++) {
			assert vector.getEntry(i) == preset
		}
	}
	
	@Test
	public void testNewPresetMatrix() {
		def dimensions = 10
		def preset = 0
		RealMatrix matrix= G.newPresetMatrix(preset)
		
		for (int c=0;c<dimensions;c++) {
			RealVector vector = matrix.getColumnVector(c)
			for (int i=0;i<dimensions;i++) {
				assert vector.getEntry(i) == preset
			}
		}
	}

}
