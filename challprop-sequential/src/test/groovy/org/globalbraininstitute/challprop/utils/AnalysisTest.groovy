package org.globalbraininstitute.challprop.utils

import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

class AnalysisTest {
	static Network network = Globals.network
	static Logger logger = LoggerFactory.getLogger(AnalysisTest.class)
	static {
		Gremlin.load()
	}
	
	@BeforeClass
	public static void init() {
		def agentList = NetworkMocks.createNetworkMock6(5)
		(new ControlledSimulation(agentList)).basicRun(10,3)
	}
	
	@Test
	public void testSituationsProcessedByEachAgent() {
		def map = Analysis.situationsProcessedByEachAgent()
		assertNotNull(map)
		println map
	}
	
	@Test
	public void testAgentsWhichProcessedEachSituation() {
		def map = Analysis.agentsWhichProcessedEachSituation()
		assertNotNull(map)
		println map
	}

	
	@AfterClass
	public static void clean() {
		network.reinit()
		network.commit()
	}
}
