package org.globalbraininstitute.challprop.utils

import org.globalbraininstitute.challprop.global.Parameters
import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class UtilsTest {
	static Logger logger = LoggerFactory.getLogger(UtilsTest.class)
	
	@Ignore
	@Test
	public void iterableSizeTest() {
		List list = [1,2,3,4,5,6,"whatever",15]
		int size = Utils.iterableSize(list)
		assertEquals(size, list.size())
		logger.info("Size of {} is {}.", list.toString(), size)
	}
	
	@Ignore
	@Test
	public void getFirstElement() {
		List list = [1,2,3,4,5,6,"whatever",15]
		Object element = Utils.getFirstElement(list)
		assertEquals(element, list[0])
		logger.info("The first element of the iterable {} is {}.", list, element)
	}
	
	@Ignore
	@Test
	public void testGraphMLSnapshot() {
		Utils.graphMLSnapshot(network.g.baseGraph)
	}
	
	@Ignore
	@Test
	public void matrixComponentTest() {
		def dimensions = 10;
		def totalPropagations = 4;
		for (int i = 1; i <=totalPropagations; i++) {
			println "propagation: " + i + "; matrixComponent: " +Utils.matrixComponent(totalPropagations, dimensions, i)
		}
	}
	
	@Ignore
	@Test
	public void initializeDatabaseTablesTest() {
		Utils.initializeDatabaseTables();
	}
	
	@Ignore
	@Test
	public void generateSituationVectorTableTest() {
		def t = System.currentTimeMillis()
		def numberOfItems = 15000
		Utils.generateSituationVectorTable(numberOfItems);
		println "Generation of one vector took: " + (System.currentTimeMillis() - t)/numberOfItems + " milliseconds"
	}

	@Ignore
	@Test
	public void generateNeedVectorTableTest() {
		def t = System.currentTimeMillis()
		def numberOfItems = 15000
		Utils.generateNeedVectorTable(numberOfItems);
		println "Generation of one vector took: " + (System.currentTimeMillis() - t)/numberOfItems + " milliseconds"
	}


	@Ignore
	@Test
	public void generateMatrixTableTest() {
		def t = System.currentTimeMillis()
		def numberOfItems = 6000
		Utils.generateMatrixTable(numberOfItems);
		println "Generation of one matrix took: " + (System.currentTimeMillis() - t)/numberOfItems + " milliseconds"
	}
	
	@Test
	public void getSituationVectorFromDatabase() {
		def paramOld = Parameters.parameters.useDatabaseOfVectors
		Parameters.parameters.useDatabaseOfVectors = true
		def t = System.currentTimeMillis()
		def numberOfItems = 10
		for (int x = 0; x < numberOfItems; x++) {
			Utils.getVectorString("situation")
		}
		println "Time to get vector from database: " + (System.currentTimeMillis() - t) / numberOfItems + " milliseconds"
		Parameters.parameters.useDatabaseOfVectors = paramOld
	}

	@Test
	public void getNeedVectorFromDatabase() {
		def paramOld = Parameters.parameters.useDatabaseOfVectors
		Parameters.parameters.useDatabaseOfVectors = true
		def t = System.currentTimeMillis()
		def numberOfItems = 10
		for (int x = 0; x < numberOfItems; x++) {
			Utils.getVectorString("need")
		}
		println "Time to get vector from database: " + (System.currentTimeMillis() -t)/numberOfItems +" milliseconds"
		Parameters.parameters.useDatabaseOfVectors = paramOld
	}

	@Test
	public void getMatrixFomDatabase() {
		def paramOld = Parameters.parameters.useDatabaseOfMatrixes
		Parameters.parameters.useDatabaseOfMatrixes = true
		def t = System.currentTimeMillis()
		def numberOfItems = 10
		for (int x = 0; x < numberOfItems; x++) {
			Utils.getMatrixString()
		}
		println "Time to get matrix from database: " + (System.currentTimeMillis() -t)/numberOfItems + " milliseconds"
		Parameters.parameters.useDatabaseOfMatrixes = paramOld
	}

	@Ignore
	@Test
	public void getVectorFromMatlab() {
		def paramOld = Parameters.parameters.useDatabaseOfVectors
		Parameters.parameters.useDatabaseOfVectors = false
		def t = System.currentTimeMillis()
		def numberOfItems = 10
		for (int x = 0; x < numberOfItems; x++) {
			Utils.getVectorString()
		}
		println "Time to get vector from Matlab: " + (System.currentTimeMillis() -t)/numberOfItems + " milliseconds"
		Parameters.parameters.useDatabaseOfVectors = paramOld
		
	}

	@Ignore
	@Test
	public void getMatrixFomMatlab() {
		def paramOld = Parameters.parameters.useDatabaseOfMatrixes
		Parameters.parameters.useDatabaseOfMatrixes = false
		def t = System.currentTimeMillis()
		def numberOfItems = 10
		for (int x = 0; x < numberOfItems; x++) {
			Utils.getMatrixString()
		}
		println "Time to get matrix from Matlab: " + (System.currentTimeMillis() -t)/numberOfItems + " milliseconds"
		Parameters.parameters.useDatabaseOfMatrixes = paramOld
	}

	
		
	/*
	@After
	public void commit() {
		network.g.baseGraph.commit()
	}
	*/
}
