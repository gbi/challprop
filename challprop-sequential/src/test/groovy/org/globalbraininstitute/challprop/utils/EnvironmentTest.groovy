package org.globalbraininstitute.challprop.utils

import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class EnvironmentTest {
	static Logger logger = LoggerFactory.getLogger(EnvironmentTest.class)
	
	@Test
	public void javaVersionTest() {
		String[] pro = ["java.version", "java.vm.version", "java.runtime.version"];
		Properties properties = System.getProperties();
		for (int i = 0; i < pro.length; i++) {
			System.out.print(pro[i]+"\t\t: ");
			System.out.println(properties.getProperty(pro[i]));
		}	
	}
	
	@Test
	public void getLocalHostTest() {
		try {
			String computerName = InetAddress.getLocalHost().getHostName();
			if (computerName.indexOf(".") > -1)
				computerName = computerName.substring(0, computerName.indexOf(".")).toUpperCase();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			}
		}

}	
