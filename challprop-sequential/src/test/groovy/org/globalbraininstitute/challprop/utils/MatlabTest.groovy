package org.globalbraininstitute.challprop.utils

import org.globalbraininstitute.challprop.utils.Matlab as G
import org.junit.Ignore
import org.junit.Test
import org.rosuda.JRI.Rengine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull
import static org.junit.Assert.fail

class MatlabTest {
	static Logger logger = LoggerFactory.getLogger(MatlabTest.class)

	@Ignore
	@Test
	public void testNewPWVector() {
		def vector = Matlab.newPWVector(10, 2.0, 0.05, 0.5, 0.2)
		assertNotNull(vector)
		logger.debug("Vector with components from powerlaw distribution: {}", vector)
	}
	
	@Ignore
	@Test
	/**
	 * uses R to draw a histogram of values
	 */
	public void testPWDistribution() {
		def vector = Matlab.newPWVector(10, 2.0, 0.05, 0.5, 0.2)
		// new R-engine
		Rengine re=new Rengine ((String[])["--vanilla"], false, null);
		if (!re.waitForR())
		{
			System.out.println ("Cannot load R");
			return;
		}

		re.assign("vector", vector.toArray())
		logger.debug("Vector assigned to R variable: {}", re.eval("vector").asDoubleArray())

		re.eval("hist(vector)")
		
		System.in.withReader {
			print  'Is this reminds a powerlaw distribution (y/n)?'
			if (it.readLine() == 'y') {
				logger.debug("Histogram looks like powerlaw, says tester")
			} else {
				fail("Histogram does not look like powerlaw, as estimated by tester")
			}
		}
		
		// done...
		re.end();

	}
	
	@Test
	public void testTryMatlab() {
		(1..3).each{ 
			def matrix = G.processMatrixSparseGeneration(10,0.5,0.5)
			logger.debug("Processing matrix with components from powerlaw distribution: {}", matrix)
			def matrixString = Utils.serializeMatrix(matrix)
			logger.debug("Deserialized processing matrix: {}", matrixString)
		}
	}
}
