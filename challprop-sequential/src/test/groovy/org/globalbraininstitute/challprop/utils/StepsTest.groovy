package org.globalbraininstitute.challprop.utils

import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class StepsTest {
	static Network network = Globals.network
	static Logger logger = LoggerFactory.getLogger(StepsTest.class)
	static {
		Gremlin.load()
	}
	
	@BeforeClass
	public static void init() {
		def agentList = NetworkMocks.createNetworkMock6(10)
		(new ControlledSimulation(agentList)).basicRun(50,3)
		
	}
	
	@Test
	public void testDefineNextPropagationStep() {
		
		List originalChallenges = Functions.getAllOriginalChallenges(network.g.baseGraph)
		
	}
	
	@Test 
	public void testDefineMaxBranchingFactors() {
		new Steps().load()
		List originalChallenges = Functions.getAllOriginalChallenges(network.g.baseGraph)
		def maxBranchingFactor = originalChallenges[0].maxBranchingFactor
		println maxBranchingFactor
		
	}
	
	@AfterClass
	public static void clear() {
		network.reinit()
		network.commit()
	}
}
