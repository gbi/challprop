package org.globalbraininstitute.challprop.analytics

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.util.structures.Tree
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.rosuda.JRI.Rengine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BasicStatistics {
	static {
		Gremlin.load()
	}
	
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(BasicStatistics.class)
	
	@BeforeClass
	public static void setUpClass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.finalize();
			}
		});
	   }
	
	@Ignore
	@Test
	public void agentsProcessedNumberOfSituations() {
		Tree processingPaths = new Tree()
		
		def m = [:]
		network.g.baseGraph.V('type','agent').groupBy(m){it}{it.outE('processed')}{it.size()}.iterate()
//		network.g.baseGraph.V('type','situation').each{m.put(it,it.inE('processed').outV.has('type','agent').count())}
		
		def l = []
		m.each{l.add((int) it.value)}
		
		Rengine re=new Rengine ((String[])["--vanilla"], false, null);
		if (!re.waitForR())
		{
			System.out.println ("Cannot load R");
			return;
		}

		re.assign("Agents", ((int[]) l))
		//logger.debug("Vector assigned to R variable: {}", re.eval("vector").asDoubleArray())

		re.eval("hist(Agents,"+\
			 "main=paste('Situations processed by agents'),"+\
			 "xlab = 'Number of situations processed',"+\
			 "ylab = 'Frequency of agents',"+\
			 "labels=TRUE,"+\
			 "col='gray')")
		System.in.withReader {
			print  'Further?'
			it.readLine()
		}
		re.end();
	}

	@Ignore
	@Test
	public void situationsProcessedNumberOfTimes() {
		Tree processingPaths = new Tree()
		
		def furtherAll = {Edge chall ->
			def id=chall.identity;
			def tm = chall.timestamp;
			chall.outV.bothE.has('identity',id).has('timestamp',T.gte,tm).order{it.a.timestamp <=> it.b.timestamp}
		}
		
		def originalChallenges = {Graph g ->
			g.V('type','situation').outE('challenged').has('source',g.V('type','god').next().getProperty('identity'))
		}
		
		
		def m = new TreeMap()
		def l = []
		network.g.baseGraph.V('type','situation').each{m.put(it,it.inE('processed').outV.has('type','agent').count())}
		m.each{l.add((int) it.value)}
		l.add(0)
		
		/*
		 * analyze the result with R and produce a histogram of processings per agent;
		 * think of other statistics to visualize
		 * with the following code snippet
		*/
		
		Rengine re=new Rengine ((String[])["--vanilla"], false, null);
		if (!re.waitForR())
		{
			System.out.println ("Cannot load R");
			return;
		}

		re.assign("Agents", ((int[]) l))
		//logger.debug("Vector assigned to R variable: {}", re.eval("vector").asDoubleArray())

		re.eval("hist(Agents, main=paste('Situations processed by agents'), xlab = 'Number Of Agents', ylab = 'Number of Situations')")
		System.in.withReader {
			print  'Further?'
			it.readLine()
		}
		re.end();
	}

	@Test
	public void linkWeightDistribution() {
		Tree processingPaths = new Tree()

		def l = []
		def m = [:].withDefault{0};
		network.g.baseGraph.V.has('type','agent').outE('knows').sideEffect{l.add((int) it.weight.round(4))}.iterate()
		
		Rengine re=new Rengine ((String[])["--vanilla"], false, null);
		if (!re.waitForR())
		{
			System.out.println ("Cannot load R");
			return;
		}

		re.assign("LinkWeights", ((int[]) l))
		//logger.debug("Vector assigned to R variable: {}", re.eval("vector").asDoubleArray())

		re.eval("hs <- hist(LinkWeights,"+\
			 "main=paste('Distribution of link weights'),"+\
			 "xlab = 'Link weight',"+\
			 "ylab = 'Frequency',"+\
			 "labels=TRUE,"+\
			 "col='gray',"+\
			 "prob = TRUE,"+\
			 "axes=FALSE)")
		re.eval("axis(side=1,at=hs\$mids)")
		re.eval("axis(2)")
		re.eval("lines(density(LinkWeights, adjust=2), lty='dotted')")
		System.in.withReader {
			print  'Further?'
			it.readLine()
		}
		re.end();
	}
	

}
