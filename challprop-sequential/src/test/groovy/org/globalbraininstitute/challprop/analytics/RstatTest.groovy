package org.globalbraininstitute.challprop.analytics

import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.BeforeClass
import org.junit.Test
import org.junit.Ignore
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class RstatTest {
	static {
		Gremlin.load()
	}
	
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(RstatTest.class)
	
	@BeforeClass
	public static void setUpClass() {
		
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			Utils.initializeDataDirectoryOnDisk()
		}
  	
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.finalize();
			}
		});
	   }

	@Test
	public void histogramRTest() {
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			// this test runs only if generation on disk is allowed...
			def data = [1,2,3,5,3,6,7,2,3,534,7,48,23,4,4,43,2,3,20]
			Map pars = new TreeMap()
			Rstat.histogram(data, "testHistogram", pars)

			def histogram = new File(Parameters.parameters.simulationHome + "analysis/testHistogram.png")
			assert histogram.exists()
			assert histogram.canRead()

			histogram.delete()
		}
	}
	
	@Test
	public void chiSquareTest() {
		def data = [1,2,3,5,3,6,7,2,3,534,7,48,23,4,4,43,2,3,20]
		Map pars = new TreeMap()
		Rstat.chiSquareGoodnessOfFit(data, "testChiSquareGoodnessOfFit", pars)
	}

}
