package org.globalbraininstitute.challprop.mocks

import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class NetworkMocksTest {
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(NetworkMocksTest.class)

	/**
	 * 
	 */
	
	@BeforeClass
	public static void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
	
	@Test
	public void createNetworkMock7Test() {
		network.reinit();
		def networkBranchingFactor = 1;
		def totalPropagations = 4;
		NetworkMocks.createNetworkMock7(totalPropagations, networkBranchingFactor);				
	}
			
	@AfterClass
	public static void commit() {
		//network.reinit()
		network.commit()
	}
	

}
