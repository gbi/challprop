package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull

public class EquivalentAgentsTestSeparateSituationGeneration {
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(EquivalentAgentsTestSeparateSituationGeneration.class)
	static ignoreNonRivalCorrectionOrig = Parameters.parameters.ignoreNonRivalCorrection
	static rivalComponentsOrig
	static reciprocityRateOrig

	/**
	 * in the network of where all agents have need vectors as unit vectors to one and processing
	 * matrixes able to zero out unit vectors (extract all benefit). all situations are designed to be
	 * interpreted as unit challenge vectors, so  each agent should take it and reduce to zero. The result
	 * should be no topology in the network in terms of link weights, as challenges get processed only once.
	 * The network is not connected = agents do not know each other
	 */

	@BeforeClass
	public static void init() {
	  // test fails with ignoreNonRivalCorrection set to 0..
	  // Parameters.parameters.ignoreNonRivalCorrection = 1

	  def preset =1.0
	  def matrix = A.newPresetMatrix(0)
	  // created zero matrix
	  def needVector = A.newPresetVector(preset)
	  // all need Vectors will be unit vectors
	  def skillVector = A.newPresetVector(0)
	  //  let all skill vectors be zero, as I have no idea so far how they will influence the thing...
		rivalComponentsOrig = Parameters.parameters.rivalComponents
		Parameters.parameters.rivalComponents = [0,1,2,3,4,5,6,7,8,9]
		reciprocityRateOrig = Parameters.parameters.reciprocityRate
		Parameters.parameters.reciprocityRate = 0


		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock4(20, needVector, skillVector, matrix)
	  (new ControlledSimulation(framedAgentsList)).oneByOneSituationGenerationWithPreset(100, preset*2)
	  
	}
	
	@Test
	public void noTopologyInTheNetwork() {

		def no_knows_edges_created =  network.g.baseGraph.V.has('type','agent').inE('knows').outV().has('type','agent').toList() == []
		if (!no_knows_edges_created) {
			def knows_weight_sum = network.g.V.has('type','agent').inE('knows').outV().has('type','agent').back(2).weight.sum()
			assertEquals("there is weights with non zero links in the network",knows_weight_sum,0,1.0E-4d)
			//assertNull(knows_weight_sum)
		} else {
			assert no_knows_edges_created == true
		}
	}
	
	/*		
	@Test
	public void noDuplicateChallenges() {
		//this query should not return challenges (duplicated) -> it should fail with java.lang.NullPointerException (run in Gremlin console)
		//list = g.E.has('type','challenge').order{it.a.timestamp <=> it.b.timestamp}.filter{it.bothV.has('type','agent') == nextStep(it).bothV.has('type','agent')}.filter{it.label == nextStep(it).label}.filter{it.identity == nextStep(it).identity}.[0..20].toList()
		//sorry, donno how to do it in here...
	}
	*/
	
	@Test
	public void allChallengesProcessedOnce() {	

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.getIdentity()).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			assertEquals("Challenge with identity "+it.identity+" is processed more than once",1, processingSteps)
			}
	}
	
	@Test
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()
		def intensitySum = 0
		network.god.asVertex().outE('created').inV().each{
			intensitySum+=it.inE('interpreted').last().intensity
		}
		assert sumOfBenefits == intensitySum
		
	}
	
	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0' // the difference between values is due to rivalCompontents
			// this test is not completely correct as when dimensions will change in configuration it will fail...
			}
		assert bool == true 
	}

	
		
	@AfterClass
	public static void commit() {
		// Parameters.parameters.ignoreNonRivalCorrection = ignoreNonRivalCorrectionOrig
		Parameters.parameters.rivalComponents = rivalComponentsOrig
		Parameters.parameters.reciprocityRate = reciprocityRateOrig
		network.reinit()
		network.commit()
	}
	

}
