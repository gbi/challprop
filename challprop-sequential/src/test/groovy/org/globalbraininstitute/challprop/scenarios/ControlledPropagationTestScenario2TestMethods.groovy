package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import static org.testng.Assert.assertEquals

public class  ControlledPropagationTestScenario2TestMethods {
	static {
		Gremlin.load();
	}
	static Network network = Globals.network;
	private Logger logger = LoggerFactory.getLogger(ControlledPropagationTestScenario2TestMethods.class);
	static Integer propagationStepsAllowed;
	static Integer networkBranchingFactor;
	static Integer numberOfSituations;
	static needVector;
	static skillVector;
	static reciprocityRateOrig
	static rivalComponentsOrig

	/**
	 * Creating a specific and exact network structure in order to be able to predict exactly the results of the 
	 * simulation
	 */


	@BeforeClass
	public static void init() {
		propagationStepsAllowed = 5
		networkBranchingFactor = 1
		numberOfSituations = 50
		def defaultRandomJump = Parameters.parameters.disableRandomJump
		Parameters.parameters.disableRandomJump = true

		needVector = A.newPresetVector(1.0)
		skillVector = A.newPresetVector(0)


		rivalComponentsOrig = Parameters.parameters.rivalComponents
		Parameters.parameters.rivalComponents = [0,1,2,3,4,5,6,7,8,9]
		reciprocityRateOrig = Parameters.parameters.reciprocityRate
		Parameters.parameters.reciprocityRate = 0

		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock7(propagationStepsAllowed,networkBranchingFactor, needVector, skillVector)

		def cycles = propagationStepsAllowed
		def situationPreset = propagationStepsAllowed +2.0

		(new ControlledSimulation(framedAgentsList))
				.centralAgent(numberOfSituations, situationPreset ,cycles)
		Parameters.parameters.disableRandomJump = defaultRandomJump

	}

	@Test
	public void thereIsACertaintTopologyInTheNetwork() {

		def m = [:].withDefault{0};
		network.g.baseGraph.V.has('type','agent').inE('knows').outV().has('type','agent').back(2).sideEffect{m[(int) it.weight.round(0)]+=1}.iterate()
		m.sort{a,b->a.key<=>b.key};

		assertEquals(m[numberOfSituations], propagationStepsAllowed)

	}

	@Test
	public void allSituationsProcessedCertainNumberOfTimes() {

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.getIdentity()).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			def processingStepsExpected = this.propagationStepsAllowed  +1
			assertEquals(processingSteps, processingStepsExpected, "Challenge with identity "+it.identity+" is processed more than once")
		}

	}

	@Test
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()
		def intensitySum = 0
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().toList().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		initialSituationVectors.each{
			intensitySum += Utils.deserializeVector(it).subtract(needVector).getL1Norm()
		}
		assert sumOfBenefits == intensitySum

	}

	@Test
	public void sumOfBenefitsEqualsToExtractedIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()

		def intensitySum = 0
		def finalSituationVectors = []
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		network.god.asVertex().outE('created').inV().each{
			finalSituationVectors.add(it.outE('logged')[0].inV().next().vector)}

		assert initialSituationVectors.size() == finalSituationVectors.size()
		for (int c = 0; c <initialSituationVectors.size();c++) {
			intensitySum += Utils.deserializeVector(initialSituationVectors[c]).subtract(Utils.deserializeVector(finalSituationVectors[c])).getL1Norm()
		}

		assert sumOfBenefits == intensitySum

	}

	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0'
		}
		assert bool == true
	}

	@AfterClass
	public static void commit() {
		Parameters.parameters.rivalComponents = rivalComponentsOrig
		Parameters.parameters.reciprocityRate = reciprocityRateOrig
		network.reinit()
		network.commit()

	}


}
