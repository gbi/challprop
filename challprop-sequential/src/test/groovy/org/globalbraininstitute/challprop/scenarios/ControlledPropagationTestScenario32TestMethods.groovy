package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.util.structures.Tree
import org.globalbraininstitute.challprop.analytics.Rstat
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory


import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.Ignore

import javax.script.Bindings
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

import static org.testng.Assert.assertEquals

public class ControlledPropagationTestScenario32TestMethods {
	static {
		Gremlin.load();
	}
	static Network network = Globals.network;
	private Logger logger = LoggerFactory.getLogger(ControlledPropagationTestScenario32TestMethods.class);
	static Integer propagationStepsAllowed;
	static Integer networkBranchingFactor;
	static Integer numberOfSituations;
	static needVector;
	static skillVector;
	static Map orig = new HashMap();
	static linkWeights

	/**
	 * Creating a specific and exact network structure in order to be able to predict exactly the results of the 
	 * simulation
	 */

	@BeforeClass
	public static void init() {
		
	  if (Parameters.parameters.generateTestDataOnDisk == true) {
		  Utils.initializeDataDirectoryOnDisk()
	  }
		
      propagationStepsAllowed = 3
	  networkBranchingFactor = 2
	  numberOfSituations = 30
	  
	  orig.put("disableRandomJump", Parameters.parameters.disableRandomJump)
	  Parameters.parameters.disableRandomJump = true
	  orig.put("respectKnowsLinksWeights", Parameters.parameters.respectKnowsLinksWeights)
	  Parameters.parameters.respectKnowsLinksWeights = false
		orig.put("rivalComponents", Parameters.parameters.rivalComponents)
		Parameters.parameters.rivalComponents = [0,1,2,3,4,5,6,7,8,9]
		orig.put("reciprocityRate", Parameters.parameters.reciprocityRate)
		Parameters.parameters.reciprocityRate = 0
	  
	  needVector = A.newPresetVector(1.0)
	  skillVector = A.newPresetVector(0)
	  
	  def dimensions = network.god.asVertex().dimensions
	  List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock7(propagationStepsAllowed,networkBranchingFactor, needVector, skillVector)
		network.g.baseGraph.saveGraphML('/home/vveitas/tmp/networkScenario32.graphml')
	  def cycles = propagationStepsAllowed+2
	  def situationPreset = propagationStepsAllowed +2.0
	  
	  (new ControlledSimulation(framedAgentsList))
	  		.centralAgent(numberOfSituations, situationPreset ,cycles)
		network.g.baseGraph.saveGraphML('/home/vveitas/tmp/networkScenario32AfterSimulation.graphml')
			  
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("gremlin-groovy");
		Tree t = new Tree();
		Bindings bindings = engine.createBindings();
		bindings.put("g", network.g.baseGraph);
		bindings.put("l", propagationStepsAllowed+2)
		bindings.put("t", t);
		
		engine.eval("def originalChallenges(g){g.V('type','situation').outE('challenged').has('source',g.V('type','god').identity.next())}")
		engine.eval("centralAgent = originalChallenges(g)[0].inV.next()", bindings)
		engine.eval("centralAgent.out.has('type','agent').loop(2){it.loops<l}.tree(t).iterate()", bindings);
		
		linkWeights = []
		def depth = 1
		for (;;) {
			def linkWeightsAtDepth =[]
			if (t.getObjectsAtDepth(depth)==[]) break
			t.getObjectsAtDepth(depth).each{it.outE('knows').each{linkWeightsAtDepth.add(it.weight)}}
			this.linkWeights.add(linkWeightsAtDepth)
			depth++
		}
	  			  
	}

	
	@Test
	public void thereIsMoreThanOnePropagationPath() {
		def linksWithWeights = network.g.baseGraph.V('type','agent').outE('knows').has('weight',T.gte,numberOfSituations -1).toList()
		
		// number of links through which challenges were propagated equals to the number of propagation steps
		// this means that there is only one path of propagation in the network  
		assert linksWithWeights.size() != propagationStepsAllowed
		
	}
	
	@Test
	public void notAllKnowsLinkWeightIsEqualToNumberOfSituations() {
		def linksWithWeights = network.g.baseGraph.V('type','agent').outE('knows').has('weight',T.gte,1.0E-4).inV().has('type','agent').back(2).toList()
		
		// there is more than one path of propagation in the network
		linksWithWeights.each {
			assert it.weight.round(3) != numberOfSituations
		}
				
	}

	@Ignore
	@Test
	public void distributionOfWeightsInEveryRingIsUniform() {
		File file
		// I will be testing whether every ring has equally distributed values with the help of simple chi-square test
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			def analysisPath = Parameters.parameters.simulationHome + "analysis/"
			(new File(analysisPath)).mkdirs()
			def pathToFile = analysisPath + "distributionOfWeightsInEveryRingRandom".replaceAll("\\s","") + ".txt"
			file = new File(pathToFile)
		} 
		for (int i = 0;i < this.linkWeights.size()-2;i++) {
			def chiTest = Rstat.chiSquareGoodnessOfFit(this.linkWeights[i], "Ring"+i+"LinkWeightChiSquareTest", new TreeMap())
			def significance = 0.05d
			if (Parameters.parameters.generateTestDataOnDisk == true) {
				file.append(chiTest.toString()+"\n")
			}
			assert chiTest.get("p.value") > significance
		}
			
	}

	@Ignore
	@Test
	public void histogramOfLinkWeights() {
		def flatListOfWeights = []
		this.linkWeights.each{
			it.collect{flatListOfWeights.add(it)}
			}
		
		Rstat.histogram(flatListOfWeights, "Distribution Of Link Weights", new TreeMap())
		
	}


	@Test
	public void thereIsACertainTopologyInTheNetwork() {

		def edges = network.g.baseGraph.V.has('type','agent').outE('knows').inV().has('type','agent').back(2).toList()
		assert (edges == []) == false

		def weights = 0
		edges.each { edge ->
			weights = weights + edge.weight
		}
		assert weights != edges.size()*1E-5d
	}

	@Test
	public void allSituationsProcessedCertainNumberOfTimes() {

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.getIdentity()).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			def processingStepsExpected = this.propagationStepsAllowed  +1
			assertEquals(processingSteps, processingStepsExpected, "Challenge with identity "+it.identity+" is processed more than once")
		}

	}

	@Test
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()
		def intensitySum = 0
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().toList().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		initialSituationVectors.each{
			intensitySum += Utils.deserializeVector(it).subtract(needVector).getL1Norm()
		}
		assert sumOfBenefits == intensitySum

	}

	@Test
	public void sumOfBenefitsEqualsToExtractedIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()

		def intensitySum = 0
		def finalSituationVectors = []
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		network.god.asVertex().outE('created').inV().each{
			finalSituationVectors.add(it.outE('logged')[0].inV().next().vector)}

		assert initialSituationVectors.size() == finalSituationVectors.size()
		for (int c = 0; c <initialSituationVectors.size();c++) {
			intensitySum += Utils.deserializeVector(initialSituationVectors[c]).subtract(Utils.deserializeVector(finalSituationVectors[c])).getL1Norm()
		}

		assert sumOfBenefits == intensitySum

	}

	
	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0'
			}
		assert bool == true 
	}
	
	@AfterClass
	public static void commit() {

		Parameters.parameters.disableRandomJump = orig.get("disableRandomJump")
		Parameters.parameters.respectKnowsLinksWeights = orig.get("respectKnowsLinksWeights")
		Parameters.parameters.rivalComponents = orig.get("rivalComponents")
		Parameters.parameters.reciprocityRate = orig.get("reciprocityRate")
		network.reinit()
		network.commit()
	}
	

}
