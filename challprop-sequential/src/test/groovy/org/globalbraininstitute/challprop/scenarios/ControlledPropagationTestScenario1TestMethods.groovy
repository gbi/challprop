package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.globalbraininstitute.challprop.global.Parameters

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test


import static org.testng.Assert.assertEquals
import static org.testng.Assert.assertNotEquals

public class ControlledPropagationTestScenario1TestMethods {
	static {
		Gremlin.load();
	}
	static Network network = Globals.network;
	private Logger logger = LoggerFactory.getLogger(ControlledPropagationTestScenario1TestMethods.class);
	static RealVector needVector;
	static Integer processingStepsAllowed;
	static List<FramedAgent> framedAgentsList
	static rivalComponentsOrig
	static reciprocityRateOrig
	static randomJumpParameterOrig

	/**
	 * in the network of where all agents have need vectors as unit vectors to one and processing
	 * matrixes able to zero out unit vectors (extract all benefit). all situations are designed to be
	 * interpreted as unit challenge vectors, so  each agent should take it and reduce to zero. The result
	 * should be no topology in the network in terms of link weights, as challenges get processed only once.
	 * The network is not connected = agents do not know each other
	 */

	@BeforeClass
	public static void init() {
		processingStepsAllowed = 2
		def preset = 1
		def numberOfAgents = 40
		def numberOfSituations =  60
		def challengesGenerationStep = 5

		Parameters.simulationParameters.put('preset',preset)
		Parameters.simulationParameters.put('numberOfAgents',numberOfAgents)
		Parameters.simulationParameters.put('numberOfSituations',numberOfSituations)
		Parameters.simulationParameters.put('challengesGenerationStep',challengesGenerationStep)

		rivalComponentsOrig = Parameters.parameters.rivalComponents
		Parameters.parameters.rivalComponents = [0,1,2,3,4,5,6,7,8,9]
		reciprocityRateOrig = Parameters.parameters.reciprocityRate
		//Parameters.parameters.reciprocityRate = 0
		randomJumpParameterOrig = Parameters.parameters.randomJump
		Parameters.parameters.randomJump = 0

		def dimensions = network.god.asVertex().dimensions
		def matrix = A.newPresetMatrix(0)
		// created zero matrix
		this.needVector = A.newPresetVector(preset)
		// all need Vectors will be unit vectors
		def skillVector = A.newPresetVector(0)
		//  let all skill vectors be zero, as I have no idea so far how they will influence the thing...
		def processingVector = A.newPresetVector(preset)

		def startTime = System.currentTimeMillis()

		framedAgentsList = NetworkMocks.createNetworkMock5(numberOfAgents,
				Utils.serializeVector(needVector),
				Utils.serializeVector(skillVector),
				Utils.serializeVector(processingVector))
		def networkCreationFinish = System.currentTimeMillis()
		println "Network creation: " + (networkCreationFinish - startTime)/1000 + " seconds"

		(new ControlledSimulation(framedAgentsList))
				.randomDistributionProcessingByVectorSubraction(numberOfSituations,processingStepsAllowed*4,preset + processingStepsAllowed)

		def finishTime = System.currentTimeMillis()
		println "Controlled simulation: " + (finishTime - networkCreationFinish)/1000 + " seconds"
		println "Simulation run time: " +  (finishTime - startTime)/1000 + " seconds"
	}

	@Test
	public void thereIsSomeTopologyInTheNetwork() {

		def no_knows_edges_created =  network.g.baseGraph.V.has('type','agent').outE('knows').toList() == []
		if (!no_knows_edges_created) {
			def knows_weight_sum = network.g.V.has('type','agent').outE('knows').weight.sum()
			def knows_link_count = network.g.V.has('type','agent').outE('knows').count()
			assert no_knows_edges_created == false
			assertNotEquals(0.01d,knows_weight_sum/knows_link_count,0.1E-13d)
		} else {
			assert no_knows_edges_created == true
		}
	}

	@Test
	public void allSituationsProcessedCertainNumberOfTimes() {

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.getIdentity()).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			def processingStepsExpected = this.processingStepsAllowed
			assertEquals(processingSteps, processingStepsExpected, "Challenge with identity "+it.identity+" is processed more than once")
		}

	}

	@Test
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()
		def intensitySum = 0
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().toList().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		initialSituationVectors.each{
			intensitySum += Utils.deserializeVector(it).subtract(needVector).getL1Norm()
		}
		assert sumOfBenefits == intensitySum

	}

	@Test
	public void sumOfBenefitsEqualsToExtractedIntensities() {
		def sumOfBenefits = network.god.asVertex().inE('knows').outV().has('type','agent').benefit.sum()

		def intensitySum = 0
		def finalSituationVectors = []
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		network.god.asVertex().outE('created').inV().each{
			finalSituationVectors.add(it.outE('logged')[0].inV().next().vector)}

		assert initialSituationVectors.size() == finalSituationVectors.size()
		for (int c = 0; c <initialSituationVectors.size();c++) {
			intensitySum += Utils.deserializeVector(initialSituationVectors[c]).subtract(Utils.deserializeVector(finalSituationVectors[c])).getL1Norm()
		}

		assert sumOfBenefits == intensitySum

	}

	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0'
		}
		assert bool == true
	}

	@Test
	public void reciprocalLinksTest() {
		def t = true;
		network.g.baseGraph.V('type','agent').each{t & it.inE('knows').outV.has('type','agent').count() == it.outE('knows').inV.has('type','agent').count()}.iterate()
		assert t ==true

		framedAgentsList.each{FramedAgent agent ->

			def knowsLinks = agent.getKnowsLinksToAgents()
			knowsLinks.each{Edge link ->
				logger.debug('the link {}', link.toString())
				def fromAgentId = link.inV().next().identity
				List<Edge> reciprocalLink = agent.asVertex().inE('knows').outV.filter{it.identity == fromAgentId}.back(2).toList()
				logger.debug('has reciprocal link', reciprocalLink[0])
				assert reciprocalLink != []
			}
		}
	}


	@AfterClass
	public static void commit() {
		Parameters.parameters.rivalComponents = rivalComponentsOrig
		Parameters.parameters.reciprocityRate = reciprocityRateOrig
		Parameters.parameters.randomJump = randomJumpParameterOrig
		network.reinit()
		network.commit()

	}
}
