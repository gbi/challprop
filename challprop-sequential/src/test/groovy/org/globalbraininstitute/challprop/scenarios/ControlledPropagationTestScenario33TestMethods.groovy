package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.groovy.Gremlin
import org.globalbraininstitute.challprop.analytics.Rstat
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Before
import org.junit.Test
import org.junit.Ignore

import javax.script.Bindings
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

import static org.testng.Assert.assertEquals

public class ControlledPropagationTestScenario33TestMethods {
	static {
		Gremlin.load();
	}
	static Network network = Globals.network;
	private Logger logger = LoggerFactory.getLogger(ControlledPropagationTestScenario33TestMethods.class);
	static Integer propagationStepsAllowed;
	static Integer networkBranchingFactor;
	static Integer numberOfSituations;
	static Integer numberOfAgents;
	static needVector;
	static skillVector;
	static Map orig;
	private List flatListOfWeights
	private List listOfBenefits
	
	private String testName

	/**
	 * Creating a specific and exact network structure in order to be able to predict exactly the results of the 
	 * simulation
	 */

	@BeforeClass
	public void init() {
      propagationStepsAllowed = 1
	  networkBranchingFactor = 2
	  numberOfSituations = 200
	  numberOfAgents = 50

	  
	  orig = Parameters.parameters // make a copy of all default parameters
	  Parameters.parameters.disableRandomJump = false
	  Parameters.parameters.respectKnowsLinksWeights = false
	  // after random propagation. Otherwise if more than buffer size is challenged, they may be just forgot
	  Parameters.parameters.bufferSize = 30
	  Parameters.parameters.randomJump = 1 //I want the probability of random jump to be as high as possible

	  
	  needVector = Utils.serializeVector(A.newPresetVector(1.0))
	  skillVector = Utils.serializeVector(A.newPresetVector(0))
	  def processingVector = Utils.serializeVector(A.newPresetVector(1.0))
	  
	  def dimensions = network.god.asVertex().dimensions
	  List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock5(numberOfAgents,
																			needVector, 
																			skillVector,
																			processingVector)
	  
	  flatListOfWeights = []
  
	  def cycles = ((int) propagationStepsAllowed+numberOfSituations)
	  def situationPreset = propagationStepsAllowed +2.0
	  
	  (new ControlledSimulation(framedAgentsList))
	  		.randomDistributionProcessingByVectorSubraction(numberOfSituations, cycles, situationPreset)
			  
		// get the link weights for analysis in tests
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("gremlin-groovy");
		List links = new ArrayList();
		List benefits = new ArrayList();
		Bindings bindings = engine.createBindings();
		bindings.put("g", network.g.baseGraph);
		bindings.put("l", links);
		bindings.put("b", benefits)
		
		engine.eval("g.V('type','agent').outE('knows').weight.each{l.add(it)}.iterate()", bindings);
		engine.eval("g.V('type','agent').each{b.add(it.benefit)}.iterate()",bindings)
		
		this.flatListOfWeights = links
		this.listOfBenefits = benefits
	  			  
	}

	@Before
	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
	
	@Test
	public void thereIsMoreThanOnePropagationPath() {
		def linksWithWeights = network.g.baseGraph.V('type','agent').outE('knows').has('weight',T.gte,numberOfSituations -1).toList()
		
		// number of links through which challenges were propagated equals to the number of propagation steps
		// this means that there is only one path of propagation in the network  
		assert linksWithWeights.size() != propagationStepsAllowed
		
	}
	
	@Test
	public void notAllKnowsLinkWeightIsEqualToNumberOfSituations() {
		def linksWithWeights = network.g.baseGraph.V('type','agent').outE('knows').has('weight',T.gte,numberOfSituations -1).toList()
		
		// number of links through which challenges were propagated equals to the number of propagation steps
		// this means that there is only one path of propagation in the network
		linksWithWeights.each {
			assert it.weight.round(3) != numberOfSituations
		}
				
	}
	
	@Test
	public void distributionOfWeightsIsGloballyUniform() {
		File file
		// Her
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			def analysisPath = Parameters.parameters.simulationHome + "analysis/"
			(new File(analysisPath)).mkdirs()
			def pathToFile = analysisPath + "distributionOfWeightsIsRandom".replaceAll("\\s","") + testName+".txt"
			file = new File(pathToFile)
		} 
		def chiTest = Rstat.chiSquareGoodnessOfFit(flatListOfWeights, "FlatLinkWeightChiSquareTest", new TreeMap())
		def significance = 0.05d
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			file.append(chiTest.toString()+"\n")
		}
		assert chiTest.get("p.value") > significance
			
	}
		
	@Test
	public void distributionOfBenefitsIsGloballyUniform() {
		File file
		// Her
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			def analysisPath = Parameters.parameters.simulationHome + "analysis/"
			(new File(analysisPath)).mkdirs()
			def pathToFile = analysisPath + "distributionOfBenefitsIsRandom".replaceAll("\\s","") + testName+".txt"
			file = new File(pathToFile)
		}
		def chiTest = Rstat.chiSquareGoodnessOfFit(listOfBenefits, "FlatLinkWeightChiSquareTest", new TreeMap())
		def significance = 0.05d
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			file.append(chiTest.toString()+"\n")
		}
		assert chiTest.get("p.value") > significance
			
	}

	
			
	@Test
	@Ignore
	public void histogramOfLinkWeights() {
		Rstat.histogram(flatListOfWeights, "Distribution Of Link Weights"+testName, new TreeMap())
	}
	
	@Test
	@Ignore
	public void histogramOfBenefits() {
		Rstat.histogram(listOfBenefits, "Distribution Of Benefits"+testName, new TreeMap())
	}


	
	@Test
	public void thereIsACertaintTopologyInTheNetwork() {
		
		def edges = network.g.baseGraph.V.has('type','agent').outE('knows').toList()
		assert (edges == []) == false
		
		edges.each{edge ->
			if (edge.inV().outE('knows').toList() != []) {
				assertEquals(edge.weight, Parameters.parameters.dimensions * Parameters.parameters.learningRate*numberOfSituations,1E-4d)
			}
		}
	}
			
	@Test
	public void allSituationsProcessedCertainNumberOfTimes() {	

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.getIdentity()).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			def processingStepsExpected = this.propagationStepsAllowed  +1 
			assertEquals(processingSteps, processingStepsExpected, "Challenge with identity "+it.identity+" is processed more than once")
		}
		
	}
	
	@Test	
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().outE('knows').inV().has('type','agent').benefit.sum()
		def intensitySum = 0
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().toList().each{
			initialSituationVectors.add(it.outE('logged').last().inV().next().vector)}
		initialSituationVectors.each{
			intensitySum += Utils.deserializeVector(it).subtract(Utils.deserializeVector(needVector)).getL1Norm()
		}
		assertEquals(sumOfBenefits,intensitySum,1E-5*numberOfSituations)
		
	}
	
	@Test
	public void sumOfBenefitsEqualsToExtractedIntensities() {
		def sumOfBenefits = network.god.asVertex().outE('knows').inV().has('type','agent').benefit.sum()

		def intensitySum = 0
		def finalSituationVectors = []
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			initialSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		network.god.asVertex().outE('created').inV().each{
			finalSituationVectors.add(it.outE('logged')[0].inV().next().vector)}
		
		assert initialSituationVectors.size() == finalSituationVectors.size()
		for (int c = 0; c <initialSituationVectors.size();c++) {
			intensitySum += Utils.deserializeVector(initialSituationVectors[c]).subtract(Utils.deserializeVector(finalSituationVectors[c])).getL1Norm()
		}
			
		assertEquals(sumOfBenefits,intensitySum,1E-5*numberOfSituations)
		
	}

	
	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0'
			}
		assert bool == true 
	}
	
	@AfterClass
	public void commit() {
		network.commit()
		if (Parameters.parameters.generateTestDataOnDisk == true) {
			/*
			* Write graph to file
			*/
			def analysisPath = Parameters.parameters.simulationHome + "analysis/"
			(new File(analysisPath)).mkdirs()
			def pathToFile = analysisPath + testName.replaceAll("\\s","") + ".graphml"
			def file = new File(pathToFile)

			network.g.baseGraph.saveGraphML(file)
			Parameters.parameters.disableRandomJump = orig.get("disableRandomJump")
			Parameters.parameters.respectKnowsLinksWeights = orig.get("respectKnowsLinksWeights")
			Parameters.parameters.bufferSize = orig.get("bufferSize")
		}
		network.reinit()
		network.commit()
		
		Utils.playAudio('/media/data/workspaces/kepler/challprop/resources/sonar.wav')

	}
	

}
