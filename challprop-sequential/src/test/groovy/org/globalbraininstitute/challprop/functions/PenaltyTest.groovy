package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PenaltyTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(PenaltyTest.class)

	@Ignore
	@Test
	public void testPenalty() {
		
		network.populate()
		
		ArrayList newSituations = new ArrayList()
		def situation =  network.god.addNewSituation()
		situation.addNewInstance().setVector(situation.getVector())
		newSituations.add(situation)
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe interpretationPipe = new TransformFunctionPipe(new Interpretation("subtraction"))
		Pipe intensityPipe = new SideEffectFunctionPipe(new Intensity("absoluteSum"))
		Pipe penaltyPipe = new SideEffectFunctionPipe(new Penalty())
		Pipeline pipeline = new Pipeline(generationPipe, interpretationPipe, intensityPipe, penaltyPipe)
		pipeline.setStarts(newSituations)
		
		while(pipeline.hasNext()) {
			FramedChallenge interpretedChallenge = pipeline.next() 
			assert (interpretedChallenge.asEdge().label == "interpreted")
			RealVector needVector = Utils.deserializeVector(interpretedChallenge.getDomain().asVertex().needVector)
			RealVector situationVector = Utils.deserializeVector(interpretedChallenge.getRange().asVertex().vector)
			RealVector challengeVector = situationVector.subtract(needVector)
			assert(interpretedChallenge.getVector() == Utils.serializeVector(challengeVector))
			assert(interpretedChallenge.getIntensity() == challengeVector.getL1Norm())
			def benlocal = challengeVector.toArray().findAll{it < 0 }.sum() ?:0
			// this works only on fresh graph, because after several iterations benefit vectors of agents become non-zero...
			assert(interpretedChallenge.getDomain().asVertex().getProperty("benefit") == benlocal)
		  }
	}

	@Test
	public void testPenaltyMultiple() {
		
		network.populate()
		
		ArrayList newSituations = new ArrayList()
		(0..20).each{ 
			def situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			newSituations.add(situation) 
			}
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe interpretationPipe = new TransformFunctionPipe(new Interpretation("subtraction"))
		Pipe intensityPipe = new SideEffectFunctionPipe(new Intensity("absoluteSum"))
		Pipe penaltyPipe = new SideEffectFunctionPipe(new Penalty())
		Pipeline pipeline = new Pipeline(generationPipe, interpretationPipe, penaltyPipe, intensityPipe)
		pipeline.setStarts(newSituations)
		
		while(pipeline.hasNext()) {
			FramedChallenge interpretedChallenge = pipeline.next()
			RealVector needVector = Utils.deserializeVector(interpretedChallenge.getDomain().asVertex().needVector)
			RealVector situationVector = Utils.deserializeVector(interpretedChallenge.getRange().asVertex().vector)
			RealVector challengeVector = situationVector.subtract(needVector)
		  }
	}

	
	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
			
}
