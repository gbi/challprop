package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class FilterOutTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(FilterOutTest.class)

	@Test
	public void testFilterOutNull() {
		
		List listOfObjects = []
		listOfObjects.add(null)
		for (int c = 0; c <4; c++) {
			listOfObjects.add(c)
		}
		listOfObjects.add(null)
		for (int c = 5; c <9; c++) {
			listOfObjects.add(c)
		}
		listOfObjects.add(null)
		
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		filterOutNullPipe.setStarts(listOfObjects)
		def filteredObjects = filterOutNullPipe.toList()
		
		assert filteredObjects != listOfObjects
		assert filteredObjects == listOfObjects.findAll{it !=null}

	}

}
