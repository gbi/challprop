package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SelectionAgentBasedTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(SelectionAgentBasedTest.class)

	@Ignore
	@Test
	public void testSelectionWithoutConcurrentlyModifiedChallenges() {
		
		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock1()
		FramedAgent framedAgent1 = framedAgentsList[0]
		
		for (int x = 1; x<=framedAgent1.getBufferSize(); x++) {
			ArrayList newSituations = new ArrayList()
			FramedSituation situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			newSituations.add(situation)
			FramedChallenge challenge = situation.addChallengeToAgent(framedAgent1)
			challenge.setIdentity(situation.identity)
			challenge.setSource(framedAgentsList[x].asVertex().getProperty('identity'))
		}
		
		//generated some challenges to agents
		
		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
			
		Pipeline prePipeline = new Pipeline(agentsWithChallengesPipe, preprocessingPipe, bufferingPipe)
		prePipeline.setStarts(framedAgentsList)
		while (prePipeline.hasNext()) {
				prePipeline.next()
		}
		// by now, the buffer of the agent1 should be full of challenges from different agents
		
		assert framedAgent1.getBuffer().size() == framedAgent1.getBufferSize()
		List<FramedChallenge> buffer = network.g.frameEdges(framedAgent1.getBuffer(), Direction.BOTH, FramedChallenge.class).toList()
		
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipeline selPipeline = new Pipeline(selectionPipe)
		selPipeline.setStarts(Arrays.asList(framedAgent1))
		def selectedChallenge
		while (selPipeline.hasNext()) {
			selectedChallenge = selPipeline.next() 
		}
		def skillVector = Utils.deserializeVector(framedAgent1.getSkillVector()) 
		
		buffer.each{FramedChallenge challenge -> 
			def challengeVector = Utils.deserializeVector(challenge.getVector())
			def capability = challengeVector.cosine(skillVector)
			def trust = framedAgent1.asVertex().inE().outV().filter{it.getProperty('identity') == challenge.getSource()}.back(2).next().weight
			def bexp = challenge.getIntensity() * capability * trust
			challenge.asEdge().setProperty("bexp", bexp)
		} 
		
		assert buffer.sort{it.asEdge().bexp}.reverse()[0] == selectedChallenge
	}
	
	@Test
	public void testSelectionWithConcurrentlyModifiedChallenges() {
		List<FramedAgent> agentList = NetworkMocks.createNetworkMock6(2)
		FramedAgent agent1 = agentList[0]
		FramedAgent agent2 = agentList[1]
		
		(new ControlledSimulation(agentList)).distributeChallengesForAllAgents(2)
		
	}

	
	@After
	public void commit() {
		network.commit()
		network.reinit()
	}

}
