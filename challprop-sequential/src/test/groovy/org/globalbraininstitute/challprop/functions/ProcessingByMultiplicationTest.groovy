package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.analysis.function.Abs
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.AfterClass
import org.junit.BeforeClass

import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class ProcessingByMultiplicationTest {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(ProcessingByMultiplicationTest.class)
	private static ignoreNonRivalCorrectionOrig
	static private benefitBefore
	static private benefitAfter
	static private skillVectorBefore
	static private RealVector vectorBefore
	static private RealVector vectorAfter
	static private FramedAgent framedAgent1
	static private negativeComponentsBefore


	@BeforeClass
	public static void run() {
		//this test does not work if ignoreNonRivalCorrection is set to 0
		ignoreNonRivalCorrectionOrig = Parameters.parameters.ignoreNonRivalCorrection
		Parameters.parameters.ignoreNonRivalCorrection = 1

		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock2()
		framedAgent1 = framedAgentsList[0]

		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipeline godsPipeline = new Pipeline(generationPipe)
		godsPipeline.setStarts(Arrays.asList(network.god.addNewSituation()))
		while (godsPipeline.hasNext()) {
			godsPipeline.next()
		}
		//generated a challenge to an agents

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipeline prePipeline = new Pipeline(agentsWithChallengesPipe, preprocessingPipe, bufferingPipe, selectionPipe, filterOutNullPipe)
		prePipeline.setStarts(framedAgentsList)
		FramedChallenge selectedChallenge = null
		while (prePipeline.hasNext()) {
			selectedChallenge = prePipeline.next()
		}

		benefitBefore = framedAgent1.getBenefit()
		vectorBefore = Utils.deserializeVector(selectedChallenge.getVector())
		negativeComponentsBefore = vectorBefore.toArray().findAll{it < 0 }.sum() ?:0
		skillVectorBefore = framedAgent1.getSkillVector()

		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipeline proPipeline = new Pipeline(processingPipe)
		proPipeline.setStarts(selectedChallenge?Arrays.asList(selectedChallenge):[])
		FramedChallenge processedChallenge
		while (proPipeline.hasNext()) {
			processedChallenge = proPipeline.next()
		}

		vectorAfter = Utils.deserializeVector(processedChallenge.getVector())
		benefitAfter = framedAgent1.getBenefit()

	}

	@Test
	public void testBenefitCalculationOne() {

		def benefit = 0
		for (int i = 0; i < vectorBefore.getDimension(); i++) {
			benefit = benefit + Math.abs(vectorBefore.getEntry(i)) - Math.abs(vectorAfter.getEntry(i))
		}

		assertEquals(benefit,benefitAfter - benefitBefore,1.0E-12d)
		logger.debug("Benefit = {} correctly extracted", benefit)
	}

	@Test
	public void testBenefitCalculationTwo() {
		def function = new Abs()
		def benefit = 0
		(vectorBefore.map(function).subtract(vectorAfter.map(function))).each{
			benefit = benefit + it.getValue()
		}
		

		assertEquals(benefit,benefitAfter - benefitBefore,1.0E-12d)
		logger.debug("Benefit = {} correctly extracted", benefit)


	}

	@Test
	public void testSkillVectorUpdate() {
		def benefit = benefitAfter - benefitBefore
		def skillVector = Utils.deserializeVector(skillVectorBefore).add(vectorBefore.mapMultiply(benefit))
		assertEquals(Utils.serializeVector(skillVector), framedAgent1.getSkillVector())
		logger.debug("Skill vector updated from {} to {}", Utils.deserializeVector(skillVectorBefore), Utils.deserializeVector(framedAgent1.getSkillVector()))

		RealMatrix processingMatrix = Utils.deserializeMatrix(framedAgent1.getProcessingMatrix())

		assertEquals(vectorAfter,processingMatrix.operate(vectorBefore))
		logger.debug("Processed challenge vector from {}, to {}", vectorBefore, vectorAfter)

	}

	@Test
	public void testLinkWeightCalculation() {
		def shouldBeWeight = framedAgent1.getLearningRate() * (benefitAfter - 1.0E-5) + 1.0
		logger.debug("Calculated shouldBeWeight {} = learningRate {} * (benefitAfter {} - benefitBefore {}) + 1.0E-5",shouldBeWeight,framedAgent1.getLearningRate(), benefitAfter,benefitBefore)
		def isWeight = framedAgent1.getKnowsLinks().toList()[0].weight
		logger.debug("Calculated isWeight {}",isWeight)
		assertEquals(isWeight,shouldBeWeight,1.0E-5)


	}

	@AfterClass
	public static void clean() {
		// putting the changed parameters to where it was (not sure this is needed, but anyway)
		Parameters.parameters.ignoreNonRivalCorrection = ignoreNonRivalCorrectionOrig

		network.reinit()
		network.commit()
	}
}
