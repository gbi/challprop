package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AgentsWithChallengesTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(AgentsWithChallengesTest.class)

	@Test
	public void testAgentsWithChallenges() {
		
		FramedAgent framedAgent1 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		FramedAgent framedAgent2 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent2).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent2).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		FramedAgent framedAgent3 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent3).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent3).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}



		def newSituation1 = network.god.addNewSituation()
		def newSituation2 = network.god.addNewSituation()
		
		def challenge1 =  newSituation1.addChallengeToAgent(framedAgent1)
		challenge1.setIdentity(newSituation1.identity)
		def challenge2 =  newSituation2.addChallengeToAgent(framedAgent2)
		challenge2.setIdentity(newSituation2.identity)

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipeline pipeline = new Pipeline(agentsWithChallengesPipe)
		
		ArrayList agents = new ArrayList()
		agents.add(framedAgent1)
		agents.add(framedAgent2)
		agents.add(framedAgent3)
		
		ArrayList challengedAgents = new ArrayList()
		pipeline.setStarts(agents)
		
		while(pipeline.hasNext()) {
			challengedAgents.add(pipeline.next())
		}
		
		assert challengedAgents.contains(framedAgent1)
		assert challengedAgents.contains(framedAgent2)
		assert !challengedAgents.contains(framedAgent3)
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
			
}
