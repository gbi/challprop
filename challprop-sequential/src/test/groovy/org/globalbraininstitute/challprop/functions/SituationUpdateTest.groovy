package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SituationUpdateTest {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(SituationUpdateTest.class)

	@Test
	public void testSituationUpdate() {
		
		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock3(2)
		// create a network of two agents
		FramedAgent framedAgent1 = framedAgentsList[0]
		FramedAgent framedAgent2 = framedAgentsList[1]
		
		FramedSituation situation = network.god.addNewSituation()
		situation.addNewInstance().setVector(situation.getVector())
		// create a new situation
		
		FramedChallenge challenge =  situation.addChallengeToAgent(framedAgent2)
		challenge.setIdentity(situation.identity)
		challenge.setSource(framedAgent1.getIdentity())
		// challenge the second agent with that situation as if it comes form the first agent

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipeline upPipeline = new Pipeline(agentsWithChallengesPipe,preprocessingPipe, bufferingPipe,\
			selectionPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe)
		upPipeline.setStarts(framedAgentsList)
		FramedChallenge updatedChallenge
		while (upPipeline.hasNext()) {
			updatedChallenge = upPipeline.next()
		}

		//check that challenge has been processed
		List instanceList =situation.getInstances().toList() 
		assert instanceList.size() == 1
		assert instanceList.sort{it.timestamp}.pop().getVector() == situation.getVector()
		assert situation.getVector() != updatedChallenge.getVector()
		
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipeline  sitPipeline = new Pipeline(situationUpdatePipe)
		sitPipeline.setStarts(Arrays.asList(updatedChallenge))
		while (sitPipeline.hasNext()) {
			sitPipeline.next()
		}

		//check that new situation vector is equal to processed challenge's vector
		//and also the new instance of the situation is recorded
		instanceList =situation.getInstances().toList()
		assert situation.getInstances().toList().size() == 2
		assert instanceList[0].getVector() == updatedChallenge.getVector()
		assert situation.getVector() == updatedChallenge.getVector()


		//check if the non-rival elements of the situation has not changed
		RealVector updatedVectorReal = Utils.deserializeVector(instanceList[0].getVector())
		RealVector oldVectorReal = Utils.deserializeVector(instanceList[1].getVector())
		def rivalComponents = situation.getRivalComponents()
		rivalComponents.each{index ->
			if (! index in  rivalComponents) {
				assert updatedVectorReal.getEntry(index) == oldVectorReal.getEntry(index)
			}
		}

	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
	
}
