package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.junit.AfterClass
import org.junit.After
import org.junit.BeforeClass
import org.junit.Test

class PropagationTest {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(PropagationTest.class)

	@BeforeClass
	static public void disableRandomJump(){
		Parameters.parameters.disableRandomJump = false
	}
	
	@Test
	public void testPropagationArbitraryNumberOfAgents() {
		
		def numberOfAgents = 1
		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock3(numberOfAgents)
		
		// create a network of some agents
		
		FramedSituation situation = network.god.addNewSituation()
		situation.addNewInstance().setVector(situation.getVector())
		// create a new situation
	
		FramedChallenge challenge =  situation.addChallengeToAgent(framedAgentsList[0])
		challenge.setIdentity(situation.identity)
		challenge.setSource(network.god.getIdentity())
		// 	challenge the first agent to get it rolling
		
		for (int count=0;count<framedAgentsList.size()-1;count++) {
			FramedAgent framedAgent1 = framedAgentsList[count]
			FramedAgent framedAgent2 = framedAgentsList[count+1]

			Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
			Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
			Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
			Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
			Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
			Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
			Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
			Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
			Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
			Pipeline upPipeline = new Pipeline(agentsWithChallengesPipe,preprocessingPipe, bufferingPipe,\
				selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, situationUpdatePipe)
			
			upPipeline.setStarts(framedAgentsList)
			List updatedChallenges
			while (upPipeline.hasNext()) {
				updatedChallenges = upPipeline.toList()
			}
				
			updatedChallenges.each{
				assert network.g.frame(it.getDomain().asVertex(), FramedAgent.class) == framedAgent1
			}
		
			Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
			Pipeline proPipeline = new Pipeline(propagationPipe)
			proPipeline.setStarts(updatedChallenges)
			FramedChallenge propagatedChallenge
			while (proPipeline.hasNext()) {
				propagatedChallenge = proPipeline.next()
			}
			
			assert network.g.frame(propagatedChallenge.getRange().asVertex(), FramedAgent.class) != framedAgent1
		}
	}

	
	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
	
	@AfterClass
	static public void enableRandomJump(){
		Parameters.parameters.disableRandomJump = false
	}

}
