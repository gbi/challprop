package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BufferingTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(BufferingTest.class)

	@Test
	public void testBuffering() {
		
		FramedAgent framedAgent1
		
		if (!network.g.getVertices('type','agent').toList().isEmpty()) {
			framedAgent1 = network.g.frame(network.g.getVertices('type','agent').iterator().next(), FramedAgent.class)
		} else {
			framedAgent1  = network.g.addVertex(null,FramedAgent.class)
			network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}

		}
		
		for (int x = 0; x<10; x++) {
			ArrayList newSituations = new ArrayList()
			FramedSituation situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			newSituations.add(situation)
		
			Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
			Pipeline godsPipeline = new Pipeline(generationPipe)
			godsPipeline.setStarts(newSituations)
			godsPipeline.iterate()
			//generated some challenges to agents
		
			Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
			Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
			Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
			
			Pipeline prePipeline = new Pipeline(agentsWithChallengesPipe, preprocessingPipe)
			prePipeline.setStarts(Arrays.asList(framedAgent1))
			prePipeline.iterate()
			
			def bufferBefore = network.g.frameEdges(framedAgent1.getBuffer(), Direction.BOTH, FramedChallenge.class).toList()

			Pipeline bufferingPipeline = new Pipeline(bufferingPipe)
			bufferingPipeline.setStarts(Arrays.asList(framedAgent1))
			bufferingPipeline.iterate()
			
			def bufferAfter = framedAgent1.getBuffer().toList()
			assert bufferAfter.size() <= framedAgent1.getBufferSize()
			
			def challenge = situation.getInterpretedLinks().toList()[0].asEdge()
			def worstChallenge
			if (bufferBefore.size() != 0) {
				def worstChallengeID = bufferBefore.sort{it.intensity}[0].asEdge().getId()
				worstChallenge = network.g.baseGraph.getEdge(worstChallengeID)
			}
			
			if (bufferBefore.size()<framedAgent1.getBufferSize()) {
				assert bufferAfter.identity.contains(challenge.identity)
				logger.debug("Emtpy buffer: buffered with intensity {}", challenge.intensity)
			} else if (challenge.intensity > worstChallenge.intensity) {
				assert bufferAfter.identity.contains(challenge.identity)
				assert worstChallenge.archived == true
				logger.debug("Better challenge: buffered with intensity {}; forgot with intensity {}", challenge.intensity, worstChallenge.intensity)
			} else {
				assert !bufferAfter.identity.contains(challenge.identity)
				assert worstChallenge.archived == false
				assert challenge.archived == true
				logger.debug("Worse challenge: forget with intenisty {}; left with intenisty {}", challenge.intensity, worstChallenge.intensity)
			}
		}
		
		def interpretedChallenges = framedAgent1.getAllInterpretedChallengesAsEdges()
		def bestInterpretedChallenges = framedAgent1.getBufferSize() >= interpretedChallenges.size() ? \
			interpretedChallenges.sort{it.intensity}.reverse() : \
			interpretedChallenges.sort{it.intensity}.reverse()[0..framedAgent1.getBufferSize()-1]
		
		def bufferAfter = network.g.frameEdges(framedAgent1.getBuffer(), Direction.BOTH, FramedChallenge.class).toList()
			
		bufferAfter.each{challenge ->
			bestInterpretedChallenges.identity.contains(challenge.getIdentity())
		}
		logger.debug("Buffer contains best {} challenges from all interpreted", bufferAfter.size())
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
			
}
