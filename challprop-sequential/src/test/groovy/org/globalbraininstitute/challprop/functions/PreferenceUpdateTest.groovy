package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class PreferenceUpdateTest {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(PreferenceUpdateTest.class)

	@Test
	public void testPreferenceUpdateWithGod() {
		
		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock3(2)
		// create a network of two agents
		FramedAgent framedAgent1 = framedAgentsList[0]
		
		FramedSituation situation = network.god.addNewSituation()
		situation.addNewInstance().setVector(situation.getVector())
		// create a new situation
		
		FramedChallenge challenge =  situation.addChallengeToAgent(framedAgent1)
		challenge.setIdentity(situation.identity)
		challenge.setSource(network.god.getIdentity())
		// challenge the first agent with that situation

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null)) 
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipeline rePipeline = new Pipeline(agentsWithChallengesPipe,preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe)
		rePipeline.setStarts(framedAgentsList)
		FramedChallenge reinterpretedChallenge
		while (rePipeline.hasNext()) {
				reinterpretedChallenge = rePipeline.next()
		}	
		network.commit()
		RealVector situationVector = Utils.deserializeVector(situation.getVector())

		Knows link = network.g.frame(network.god.getLinkFromAgent(framedAgent1.asVertex().getId()), Direction.BOTH, Knows.class)
		RealVector preferenceVectorBefore = Utils.deserializeVector(link.getPreferenceVector())
		
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipeline upPipeline = new Pipeline(preferenceUpdatePipe)
		upPipeline.setStarts(Arrays.asList(reinterpretedChallenge))
		if (upPipeline.hasNext()) {
			while (upPipeline.hasNext()) {
				upPipeline.next()
			}
		}
		network.commit()
		// pull the challenge through all processes up to preference update
		// it should update the preferenceVector of a link between the god and and agent
		link = network.g.frame(network.god.getLinkFromAgent(framedAgent1.asVertex().getId()), Direction.BOTH, Knows.class)
		RealVector preferenceVectorAfter = Utils.deserializeVector(link.getPreferenceVector())
		
		RealVector difference = preferenceVectorAfter.subtract(preferenceVectorBefore)
		
		
		for (int i = 0;i <difference.getDimension();i++) {
			assertEquals(difference.getEntry(i), situationVector.getEntry(i),1.0E-12d)
		}
		
	}

	@Test
	public void testPreferenceUpdateWithAnotherAgent() {
		
		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock3(2)
		// create a network of two agents
		FramedAgent framedAgent1 = framedAgentsList[0]
		FramedAgent framedAgent2 = framedAgentsList[1]
		
		FramedSituation situation = network.god.addNewSituation()
		situation.addNewInstance().setVector(situation.getVector())
		// create a new situation
		
		FramedChallenge challenge =  situation.addChallengeToAgent(framedAgent2)
		challenge.setIdentity(situation.identity)
		challenge.setSource(framedAgent1.getIdentity())
		// challenge the second agent with that situation as if it comes form the first agent

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipeline rePipeline = new Pipeline(agentsWithChallengesPipe,preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe)
		rePipeline.setStarts(framedAgentsList)
		FramedChallenge reinterpretedChallenge
		while (rePipeline.hasNext()) {
			reinterpretedChallenge = rePipeline.next()
		}
		RealVector situationVector = Utils.deserializeVector(situation.getVector())

		Knows link = network.g.frame(framedAgent1.getLinkFromAgent(framedAgent2.getIdentity()), Direction.BOTH, Knows.class)
		RealVector preferenceVectorBefore = Utils.deserializeVector(link.getPreferenceVector())
		
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipeline upPipeline = new Pipeline(preferenceUpdatePipe)
		upPipeline.setStarts(Arrays.asList(reinterpretedChallenge))
		while (upPipeline.hasNext()) {
			upPipeline.next()
		}
		// pull the challenge through all processes up to preference update
		// it should update the preferenceVector of a link between the god and and agent
		link = network.g.frame(framedAgent1.getLinkFromAgent(framedAgent2.getIdentity()), Direction.BOTH, Knows.class)
		RealVector preferenceVectorAfter = Utils.deserializeVector(link.getPreferenceVector())
		
		RealVector difference = preferenceVectorAfter.subtract(preferenceVectorBefore)
		
		for (int i = 0;i <difference.getDimension();i++) {
			assertEquals(difference.getEntry(i), situationVector.getEntry(i),1.0E-12d)
		}
		
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}

}
