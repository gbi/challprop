package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ChallengeUpdateTest {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(ChallengeUpdateTest.class)

	@Before
	public void addshutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	  
	}
	
	@Test
	public void concurrentlyModifiedChallengeCorrectlyUpdated() {
		
		def preset =1.0
		def dimensions = network.god.asVertex().dimensions
		def needVector = A.newPresetVector(preset)
		// all need Vectors will be unit vectors
		def skillVector = A.newPresetVector(0)
		//  let all skill vectors be zero, as I have no idea so far how they will influence the thing...
		def processingVector = A.newPresetVector(preset)
		
		ArrayList<FramedAgent> agentsList = NetworkMocks.createNetworkMock5(2, 
													Utils.serializeVector(needVector), 
													Utils.serializeVector(skillVector), 
													Utils.serializeVector(processingVector))
		FramedAgent agent1 = agentsList[0]
		FramedAgent agent2 = agentsList[1]
		
		FramedSituation situation = network.god.addNewSituation()
		situation.setVector(Utils.serializeVector(A.newPresetVector(preset+2.0)))
		situation.addNewInstance().setVector(situation.getVector())
		for (int a = 0; a < agentsList.size(); a++) {
			def challenge =  situation.addChallengeToAgent(agentsList[a])
			challenge.setIdentity(situation.identity)
			challenge.setSource(network.god.getIdentity())
		}

		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		//Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingBySubtraction())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline bufPipeline = new Pipeline(preprocessingPipe, bufferingPipe)
		Pipeline procPipeline = new Pipeline(selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe)

		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe)

		bufPipeline.setStarts(agentsList)
		bufPipeline.toList()
		network.commit()
		
		procPipeline.setStarts(Arrays.asList(agent1))
			def challengeAgent1 = procPipeline.next()
		network.commit()
		
		procPipeline.reset()
		procPipeline.setStarts(Arrays.asList(agent2))
		procPipeline.next()
		network.commit()
		
		def list=[]
		network.g.baseGraph.V('type', 'agent').bothE().has('type','challenge').each{if (it.updatedFromChallengeWithId !=null) {list.add(it)}}
		assert list.size() == 1
		
		//get the last challenge buffered by the agent 2
		def lastBufferedAgent2 = agent2.asVertex().bothE('buffered').has('type','challenge').toList()[0]
		//def lastBufferedAgent2 = network.g.baseGraph.V('type', 'agent').bothE().has('type','challenge').has('label','buffered').toList()[0]
		assert lastBufferedAgent2 !=null
		assert lastBufferedAgent2.updatedFromChallengeWithId != null
		
		def challenge1 = Functions.previousStep(challengeAgent1.asEdge()) // need to get previous because the onecomming from pipe is reinterpreted...
		assert challenge1.vector == lastBufferedAgent2.vector
		
	}
	
	@After
	public void commit() {
		network.reinit()
		network.commit()

	}

}
