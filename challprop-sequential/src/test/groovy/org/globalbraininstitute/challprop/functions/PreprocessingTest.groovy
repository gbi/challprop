package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class PreprocessingTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(PreprocessingTest.class)

	@Test
	public void testPreprocessing() {
		
		FramedAgent framedAgent1 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}

		RealVector needVector = Utils.deserializeVector(framedAgent1.getNeedVector())

		def newSituation1 = network.god.addNewSituation()
		RealVector situationVector = Utils.deserializeVector(newSituation1.getVector())
		
		def challenge1 =  newSituation1.addChallengeToAgent(framedAgent1)
		challenge1.setIdentity(newSituation1.identity)
		challenge1.setSource(network.god.getIdentity())

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipeline pipeline = new Pipeline(agentsWithChallengesPipe, preprocessingPipe)
		
		ArrayList agents = new ArrayList()
		agents.add(framedAgent1)
		
		ArrayList challengedAgents = new ArrayList()
		pipeline.setStarts(agents)
		
		while(pipeline.hasNext()) {
			challengedAgents.add(pipeline.next())
		}
		
		assert challengedAgents.contains(framedAgent1)
		FramedChallenge challenge = network.g.frame(network.g.baseGraph.E.filter{it.label == 'interpreted'}.next(), Direction.OUT, FramedChallenge.class)
		RealVector challengeVector = Utils.deserializeVector(challenge.getVector())
		assertEquals('Interpretation not correct',challengeVector,situationVector.subtract(needVector))
		
		def intensity = challenge.getIntensity()
		def intensityTest = 0
		challengeVector.toArray().each{
			intensityTest = intensityTest + Math.abs(it)
		}
		assertEquals('Intensity calculation not correct',intensity,intensityTest,0)
		
		def penalty = framedAgent1.getBenefit()
		def penaltyTest = 0
		challengeVector.toArray().each{
			if (it<0) {
				penaltyTest = penaltyTest + it
			}
		}
		assertEquals('Penalty calculation not correct',penalty,penaltyTest,0)
		
		FramedChallenge originalChallenge = network.g.frame(network.g.baseGraph.E.filter{it.label == 'challenged'}.next(), Direction.IN, FramedChallenge.class)
		assertEquals('Original challenge archived', originalChallenge.getArchived(), true)
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
			
}
