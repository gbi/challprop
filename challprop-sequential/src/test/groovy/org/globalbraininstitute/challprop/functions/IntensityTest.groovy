package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class IntensityTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(IntensityTest.class)

	@Test
	public void testIntensity() {
		
		network.populate()
		
		ArrayList newSituations = new ArrayList()
		newSituations.add(network.god.addNewSituation())
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe interpretationPipe = new TransformFunctionPipe(new Interpretation("subtraction"))
		Pipe intensityPipe = new SideEffectFunctionPipe(new Intensity("absoluteSum"))
		Pipeline pipeline = new Pipeline(generationPipe, interpretationPipe, intensityPipe)
		pipeline.setStarts(newSituations)
		
		while(pipeline.hasNext()) {
			FramedChallenge interpretedChallenge = pipeline.next() 
			assert (interpretedChallenge.asEdge().label == "interpreted")
			RealVector needVector = Utils.deserializeVector(interpretedChallenge.getDomain().asVertex().needVector)
			RealVector situationVector = Utils.deserializeVector(interpretedChallenge.getRange().asVertex().vector)
			RealVector challengeVector = situationVector.subtract(needVector)
			assert(interpretedChallenge.getVector() == Utils.serializeVector(challengeVector))
			assert(interpretedChallenge.getIntensity() == challengeVector.getL1Norm())
		  }
		  
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
			
}
