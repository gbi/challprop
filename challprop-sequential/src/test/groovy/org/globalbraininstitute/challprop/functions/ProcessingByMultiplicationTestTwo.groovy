package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.analysis.function.Abs
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class ProcessingByMultiplicationTestTwo {
	
	static {
		Gremlin.load()
	}
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(ProcessingByMultiplicationTestTwo.class)
	static private FramedAgent framedAgent1
	static private situationVectorBefore
	static private situationVectorAfter


	@BeforeClass
	public static void run() {

		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock2()
		framedAgent1 = framedAgentsList[0]

		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipeline godsPipeline = new Pipeline(generationPipe)
		godsPipeline.setStarts(Arrays.asList(network.god.addNewSituation()))
		while (godsPipeline.hasNext()) {
			godsPipeline.next()
		}
		//generated a challenge to an agent


		// first pass of a pipeline with the situation:

		Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipeline fullPipeline = new Pipeline(agentsWithChallengesPipe, preprocessingPipe, bufferingPipe, selectionPipe, filterOutNullPipe, processingPipe)
		fullPipeline.setStarts(framedAgentsList)
		while (fullPipeline.hasNext()) {
			fullPipeline.next()
		}

		// getting the value of the situations vector after the first pass:
		FramedSituation situation = network.g.frame(network.g.baseGraph.V('type','situation').next(),FramedSituation.class)
		situationVectorBefore = situation.getVector().tokenize(",")

		// second pass of the same situation:
		fullPipeline.setStarts(framedAgentsList)
		while (fullPipeline.hasNext()) {
			fullPipeline.next()
		}

		// getting the value of the situation vector after the second pass:
		situationVectorAfter = situation.getVector().tokenize(",")


	}

	@Test
	public void testNonRivalComponentsNotAccountedWhenProcessingRepeatedly() {

		// non-rival components of the situation vector before the second pass and after the second pass should be equa
		// (non-rival component of the same challenge are not processed more than once by the same agent)
		List rivalComponents = Parameters.parameters.rivalComponents
		situationVectorBefore.eachWithIndex{entry, index ->
			if (! index in rivalComponents) { assertEquals(entry,situationVectorAfter[index]) }
		}

	}


	@AfterClass
	public static void clean() {

		network.reinit()
		network.commit()
	}
}
