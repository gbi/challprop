package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ChallengeGenerationTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(ChallengeGenerationTest.class)

	@Test
	public void testOneChallengeGeneration() {
		
		FramedAgent framedAgent1 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		FramedAgent framedAgent2 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent2).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent2).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		FramedAgent framedAgent3 = network.g.addVertex(null,FramedAgent.class)
		network.god.addAgentKnows(framedAgent3).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent3).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}


		for (int x = 0; x <=10; x++) {
			ArrayList newSituations = new ArrayList()
			newSituations.add(network.god.addNewSituation())
			Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
			Pipeline pipeline = new Pipeline(generationPipe)
			pipeline.setStarts(newSituations)
		
			for (FramedChallenge framedChallenge: pipeline) {
				FramedSituation situation = framedChallenge.getDomain()
				assert (situation == newSituations[0])
				FramedAgent agent = framedChallenge.getRange()
			}
		}
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()
		
	}
			
}
