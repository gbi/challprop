package org.globalbraininstitute.challprop.functions

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

class ReinterpretationTest {
	
	static {
		Gremlin.load()
	}
	Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(ReinterpretationTest.class)

	@Test
	public void testReinterpretation() {
		network.populate()
		
		ArrayList newSituations = new ArrayList()
		newSituations.add(network.god.addNewSituation())
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe interpretationPipe = new TransformFunctionPipe(new Interpretation("subtraction"))
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipeline pipeline = new Pipeline(generationPipe, interpretationPipe, reinterpretationPipe)
		pipeline.setStarts(newSituations)
		
		while(pipeline.hasNext()) {
			FramedChallenge reinterpretedChallenge = pipeline.next() 
			assert (reinterpretedChallenge.asEdge().label == "reinterpreted")
			RealVector situationVector = Utils.deserializeVector(reinterpretedChallenge.getRange().asVertex().vector)
			RealVector reinterpretedVector = Utils.deserializeVector(reinterpretedChallenge.getVector())
			for (int i = 0;i <reinterpretedVector.getDimension();i++) {
				assertEquals(reinterpretedVector.getEntry(i), situationVector.getEntry(i),1.0E-12d)
			}
		  }
		  
	}

	@After
	public void commit() {
		network.reinit()
		network.commit()

	}
			
}
