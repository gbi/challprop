package org.globalbraininstitute.challprop.frames

import com.tinkerpop.frames.FramedGraph
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

public class FrameInitializationTest {

	Network network = Globals.network
	FramedGraph framedGraph = network.g
	private static Logger logger = LoggerFactory.getLogger(FrameInitializationTest.class)

	@Before
	public void setup() {
		framedGraph.registerFrameInitializer(FrameInitializers.framedAgent)
		framedGraph.registerFrameInitializer(FrameInitializers.framedSituation)
		framedGraph.registerFrameInitializer(FrameInitializers.framedSituationInstance)
		framedGraph.registerFrameInitializer(FrameInitializers.framedGod)
		framedGraph.registerFrameInitializer(FrameInitializers.framedEdge)
	}
	
	@Test
	public void testGodInitialization() {
		FramedGod framedGod = framedGraph.addVertex(null, FramedGod.class)
		assertEquals(framedGod.getType(), "god")
		assertEquals(framedGod.asVertex().getProperty("rivalComponents"), Parameters.parameters.rivalComponents)
		assertEquals(framedGod.asVertex().getProperty("dimensions"), Parameters.parameters.dimensions)
		assertEquals(framedGod.asVertex().getProperty("numberOfCycles"), Parameters.parameters.numberOfCycles)
		assertEquals(framedGod.asVertex().getProperty("newSituationsPerCycle"), Parameters.parameters.newSituationsPerCycle)
	}
	
	@Test
	public void testAgentInitialization() {
		FramedAgent framedAgent= framedGraph.addVertex(null, FramedAgent.class)
		assertEquals(framedAgent.getType(), "agent")
		logger.debug("needVector is {}", framedAgent.getNeedVector())
		logger.debug("skillVector is {}", framedAgent.getSkillVector())
		logger.debug("processingMatrix is {}", framedAgent.getProcessingMatrix())
		logger.debug("bufferSize is {}", framedAgent.getBufferSize())
		logger.debug("benefit is {}", framedAgent.getBenefit())
	}

	@Test
	public void testSituationInitialization() {
		FramedSituation framedSituation= framedGraph.addVertex(null, FramedSituation.class)
		assertEquals(framedSituation.getType(), "situation")
		assertNotNull(framedSituation.identity)
		logger.debug("rivalComponents is {}", framedSituation.getRivalComponents()).toString()
	}
	
	@Test
	public void testSituationInstanceInitialization() {
		FramedSituationInstance framedSituationInst= framedGraph.addVertex(null, FramedSituationInstance.class)
		assertEquals(framedSituationInst.getType(), "situationInst")
		logger.debug("vector is {}", framedSituationInst.getVector())
	}
	
	@Test
	public void testChallengeInitialization() {
		FramedAgent agent1= framedGraph.addVertex(null, FramedAgent.class)
		FramedAgent agent2= framedGraph.addVertex(null, FramedAgent.class)
		FramedSituation instance1= framedGraph.addVertex(null, FramedSituation.class)
		FramedChallenge framedChallenge = instance1.addChallengeToAgent(agent1)
		framedChallenge.setIdentity(instance1.identity)
		framedChallenge.setSource(agent2.getIdentity()) 
		assertEquals(framedChallenge.getSource(), agent2.getIdentity())
		assertEquals(instance1.getChallengedAgents().iterator().next(), agent1)
	}
	
	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
}
