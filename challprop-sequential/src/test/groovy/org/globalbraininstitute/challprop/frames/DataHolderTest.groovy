package org.globalbraininstitute.challprop.frames

import com.tinkerpop.gremlin.groovy.Gremlin
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class DataHolderTest {
	static {
		Gremlin.load()
	}
	
	static Network network = Globals.network
	private static Logger logger = LoggerFactory.getLogger(DataHolderTest.class)
	static Integer processingStepsAllowed
	static RealVector needVector

    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}
	
	@Test
	public void run() {
		def agentList = NetworkMocks.createNetworkMock6(10)
		
		for (int i = 0; i <=10; i++) {
			ControlledSimulation.generation(5)
			ControlledSimulation.propagation(agentList,1)
		}
	
		def bool = true
		network.g.baseGraph.V('type','agent').outE('penalized').timestamp.each{bool = bool & it !=null};
		assert bool == true
		
		bool = true
		network.g.baseGraph.V('type','agent').outE('penalized').inV().value.each{bool = bool & it !=null};
		assert bool == true

		bool = true
		network.g.baseGraph.V('type','agent').outE('benefitted').timestamp.each{bool = bool & it !=null};
		assert bool == true
		
		bool = true
		network.g.baseGraph.V('type','agent').outE('benefited').inV().value.each{bool = bool & it !=null};
		assert bool == true

	} 
	
    @AfterClass 
    public static void tearDownClass() {
		network.reinit()
        network.commit();
    }
}
