package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.frames.FramedGraph
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.junit.After
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class FrameMethodsTest {

	Network network = Globals.network
	FramedGraph framedGraph = network.g
	private static Logger logger = LoggerFactory.getLogger(FrameMethodsTest.class)

	@Test
	public void testGetVector() {
		ArrayList<FramedAgent> agentsList = NetworkMocks.createNetworkMock6(2)
		(new ControlledSimulation(agentsList)).runWithBulkSituationGeneration(10)
		
		FramedSituation anySituation = network.god.getSituations().iterator().next()
		FramedChallenge anyChallenge = network.g.frame(anySituation.asVertex().bothE().has('type','challenge').next(), Direction.BOTH, FramedChallenge.class)
		FramedAgent agent = network.g.frame(anyChallenge.asEdge().bothV().has('type','agent').next(),FramedAgent.class)
		
		assert FrameMethods.getChallengeVectorUpdate(anyChallenge) == Utils.deserializeVector(anySituation.getVector()).subtract(Utils.deserializeVector(agent.getNeedVector())) 
	}
	

	@After
	public void commit() {
		network.reinit()
		network.commit()
	}
}
