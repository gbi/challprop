/**
 * Create a two clusters of agents:
 * First cluster has needVector with large values - these agents will on average get less benefit from processing challenges
 * Second cluster has needVector with small values - these agents will process anything that has intensity a little higher 
 * than zero .The average benefit of first cluster should be considerably smaller than second
 * Inter and intra cluster link numbers and link strengths should also be different  
 * @author vveitas 
 * 
 */

package org.globalbraininstitute.challprop.experiments

import com.thinkaurelius.titan.core.TitanFactory
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.ControlledSimulation
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.utils.Algorithms
import org.globalbraininstitute.challprop.utils.Utils

Globals.network.godVertex.setProperty('simulationStart',System.nanoTime())

def parameters = new HashMap()
parameters.put("c1", new HashMap())
parameters.c1.put("agents",20)
parameters.c1.put("needVector",Utils.serializeVector(Algorithms.newPresetVector(100.0)))

parameters.put("c2", new HashMap())
parameters.c2.put("agents",20)
parameters.c2.put("needVector",Utils.serializeVector(Algorithms.newPresetVector(1.0)))

def agentList = NetworkMocks.createNetworkMockWithClusters(parameters)
(0..10).each{
	ControlledSimulation.generation(10)
	ControlledSimulation.propagation(agentList,10)
}

Globals.network.godVertex.setProperty('simulationFinish',System.nanoTime())

println "Experiment ID: " + Globals.network.global.simulationID

print "Archiving the graph..."
Globals.network.g.saveGraphSON("graph.graphson")
g = TitanFactory.open("../graph")
g.loadGraphSON("graph.graphson")
g.shutdown()
println "Done"

print "Flushing Cassandra database on disk..."
Globals.network.clear()
Globals.network.gracefulShutdown()
println "Done"

