/**
 * Create a folder on the disk for a specific experiment and copy the configuration file there
 * @author vveitas
 */

package org.globalbraininstitute.challprop.experiments

import java.nio.file.Files
import java.nio.file.StandardCopyOption

def configurationsPath = "configs/"
def configFileName = "challprop.conf"
Map parameters = new ConfigSlurper('config').parse(new File(configurationsPath + configFileName).toURI().toURL())
String pathOnDisk = parameters.pathOnDisk
Date d = new Date()
String simulationID = d.getDateString().replaceAll("/","-") +"-"+d.getTimeString().replaceAll("/","-").replaceAll("\\s+","")
String simulationHome = pathOnDisk + "data/" + simulationID + "/"
String graphPath = simulationHome + "graph/"
String experimentPath = simulationHome+ "experiment/"

//put everything into parameters
parameters.put("simulationID", simulationID)
parameters.put("simulationHome", simulationHome)
parameters.put("graphPath", graphPath)
parameters.put("experimentPath", experimentPath)

//create simulationHome directory on disk
if (parameters.generateTestDataOnDisk) {
	File f = new File(simulationHome)
	f.mkdirs()
	f = new File(graphPath)
	f.mkdirs()
	f = new File(experimentPath+"/configs")
	f.mkdirs()
	f = new File(experimentPath+"/log")
	f.mkdirs()
	f = new File(experimentPath+"/analytics")
	f.mkdirs()

}

def experimentConfig = new ConfigObject()
experimentConfig.putAll(parameters)
def configFile = new File(experimentPath + "configs/challprop.conf")
configFile.createNewFile()
experimentConfig.writeTo(new PrintWriter(configFile))

def scriptDir = new File(getClass().protectionDomain.codeSource.location.path).parent
File tempConfigFile = new File(parameters.tempConfigFile)
//copy files from template directory to the experiment directory
Files.copy(configFile.toPath(),tempConfigFile.toPath(),StandardCopyOption.REPLACE_EXISTING)
Files.copy(new File(scriptDir + "/twoClustersOfAgents.groovy").toPath(),new File(experimentPath+"/twoClustersOfAgents.groovy").toPath())
analysisDir = scriptDir.toString().replace('experiments','analytics').replace('challprop-sequential','challprop-analytics')
Files.copy(new File(analysisDir + "/knitr_analysis_template.Rmd").toPath(),new File(experimentPath+"/analytics/knitr_analysis_template.Rmd").toPath())
Files.copy(new File(analysisDir + "/basicStatistics.groovy").toPath(),new File(experimentPath+"/analytics/basicStatistics.groovy").toPath())
Files.copy(new File(analysisDir + "/avarage_benefit.R").toPath(),new File(experimentPath+"/analytics/avarage_benefit.R").toPath())
