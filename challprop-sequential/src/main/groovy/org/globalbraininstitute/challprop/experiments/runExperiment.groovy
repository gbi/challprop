/**
 * Run an experiment from the filesystem's folder created with createExperiment.groovy
 * @author vveitas
 */

package org.globalbraininstitute.challprop.experiments

def shell = new GroovyShell()
shell.run(new File("twoClustersOfAgents.groovy"), null)