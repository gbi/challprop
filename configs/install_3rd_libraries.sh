#!/bin/sh

cd ~/tmp
wget https://bitbucket.org/gbi/challprop/downloads/matlabcontrol-4.1.0.jar
mvn install:install-file \
	-Dfile=matlabcontrol-4.1.0.jar \
	-DgroupId=matlabcontrol \
	-DartifactId=matlabcontrol \
	-Dversion=4.1.0 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/RserveEngine.jar
mvn install:install-file \
	-Dfile=RserveEngine.jar \
	-DgroupId=net.rforge \
	-DartifactId=rosuda-rserve \
	-Dversion=1.7.0_45 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/JRI.jar
mvn install:install-file \
	-Dfile=JRI.jar \
	-DgroupId=net.rforge \
	-DartifactId=rjava-jri \
	-Dversion=0.9.5 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/JRIEngine.jar
mvn install:install-file \
	-Dfile=JRIEngine.jar \
	-DgroupId=net.rforge \
	-DartifactId=rjava-jriengine \
	-Dversion=0.9.5 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/REngine.jar
mvn install:install-file \
	-Dfile=REngine.jar \
	-DgroupId=net.rforge \
	-DartifactId=rjava-jrengine \
	-Dversion=0.9.5 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/jmathplot.jar
mvn install:install-file \
	-Dfile=jmathplot.jar \
	-DgroupId=jmathplot \
	-DartifactId=jmathplot \
	-Dversion=2009.09 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/gephi-toolkit.jar
mvn install:install-file \
	-Dfile=gephi-toolkit.jar \
	-DgroupId=gephi-toolkit \
	-DartifactId=gephi-toolkit \
	-Dversion=0.8.7 \
	-Dpackaging=jar

wget https://bitbucket.org/gbi/challprop/downloads/gephi_streaming_server.jar
mvn install:install-file \
	-Dfile=gephi_streaming_server.jar \
	-DgroupId=gephi-plugins \
	-DartifactId=gephi-streaming-server \
	-Dversion=0.8.7 \
	-Dpackaging=jar

