/**
 * Elementary test to see whether the dynamic visualization with Gephi is working
 * Gephi must be running with Master server enabled for this test to pass
 * @author vveitas
 */

package org.globalbraininstitute.challprop.visual

import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class GephiStreamingTest {
	static Logger logger = LoggerFactory.getLogger(GephiStreamingTest.class)
	
	@Ignore
	@Test
	public void gephiStreamingServerTest() {
		GephiStreamingServer gephi = new GephiStreamingServer()
		assert gephi.post("{\"an\":{\"A\":{\"label\":\"Streaming Node A\"}}}") == 200;
		assert gephi.post("{\"an\":{\"B\":{\"label\":\"Streaming Node B\"}}}") == 200;
		assert gephi.post("{\"an\":{\"C\":{\"label\":\"Streaming Node C\"}}}") == 200;
		assert gephi.post("{\"ae\":{\"AB\":{\"source\":\"A\",\"target\":\"B\",\"directed\":false}}}") == 200;
		assert gephi.post("{\"ae\":{\"BC\":{\"source\":\"B\",\"target\":\"C\",\"directed\":false}}}") == 200;
		assert gephi.post("{\"ae\":{\"CA\":{\"source\":\"C\",\"target\":\"A\",\"directed\":false}}}") == 200;
	}
	
}	
