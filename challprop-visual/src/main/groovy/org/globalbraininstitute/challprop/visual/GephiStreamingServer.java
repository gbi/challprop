package org.globalbraininstitute.challprop.visual;

//========================================================================
//Copyright 2006 Mort Bay Consulting Pty. Ltd.
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================


import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GephiStreamingServer {
	
	/*
	 * http://wiki.gephi.org/index.php/Graph_Streaming
	 */
	
  public static Integer post(String out) {
	  	Integer result = null;
		try {
			HttpURLConnection httpcon = (HttpURLConnection) ((new URL("http://localhost:8080/workspace0?operation=updateGraph").openConnection()));
			httpcon.setDoOutput(true);
			httpcon.setRequestProperty("Content-Type", "application/json");
			httpcon.setRequestProperty("Accept", "application/json");
			httpcon.setRequestMethod("POST");
			httpcon.connect();

			OutputStreamWriter osw = new OutputStreamWriter(httpcon.getOutputStream());

			osw.write(out);
			osw.flush();
			osw.close();
		
			result =  httpcon.getResponseCode();
		
			} catch (MalformedURLException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		
		return result;
  	}
  
  	public static Integer postNewAgent(Vertex element) {
  		JsonObject JsonObject = Json.createObjectBuilder()
  					.add("an", Json.createObjectBuilder()
  							.add(element.getId().toString(), Json.createObjectBuilder()
  									.add("type", "agent")
  									.add("benefit", (Double) element.getProperty("benefit"))
  									.add("needVector", (String) element.getProperty("needVector"))
  								)
  						)
  				.build();
		return post(JsonObject.toString());
  	}
  	
  	public static Integer postChangeAgent(Vertex element) {
  		JsonObject JsonObject = Json.createObjectBuilder()
  					.add("cn", Json.createObjectBuilder()
  							.add(element.getId().toString(), Json.createObjectBuilder()
  									.add("benefit", (Double) element.getProperty("benefit"))
  								)
  						)
  				.build();
		return post(JsonObject.toString());
  	}


  	public static Integer postNewKnowsLink(Edge element) {
  		JsonObject JsonObject = Json.createObjectBuilder()
					.add("ae", Json.createObjectBuilder()
							.add(element.getId().toString(), Json.createObjectBuilder()
									.add("label", "knows")
									.add("source", element.getVertex(Direction.IN).getProperty("identity").toString())
									.add("target", element.getVertex(Direction.OUT).getProperty("identity").toString())
									.add("directed", true)
									.add("weight", (Double) element.getProperty("weight"))
								)
						)
				.build();
  		return post(JsonObject.toString());
  	}
  	
  	public static Integer postChangeKnowsLink(Edge element) {
  		JsonObject JsonObject = Json.createObjectBuilder()
					.add("ce", Json.createObjectBuilder()
							.add(element.getId().toString(), Json.createObjectBuilder()
									.add("weight", (Double) element.getProperty("weight"))
								)
						)
				.build();
  		return post(JsonObject.toString());
  	}
  	
  
}  
 
