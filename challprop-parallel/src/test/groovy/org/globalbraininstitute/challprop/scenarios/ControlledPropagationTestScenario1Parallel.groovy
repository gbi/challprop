package org.globalbraininstitute.challprop.scenarios

import org.junit.Test
import org.testng.TestNG

public class ControlledPropagationTestScenario1Parallel {
	@Test
	public void testTestNGProgramatically() {
		TestNG testng = new TestNG();
		List<String> suites = new ArrayList<String>();
		suites.add("./testNGSuites/ControlledPropagationTestScenario1Parallel.xml");
		testng.setTestSuites(suites);
		testng.run()	
	}
}
