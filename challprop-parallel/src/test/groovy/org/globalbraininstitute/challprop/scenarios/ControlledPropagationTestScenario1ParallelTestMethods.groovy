package org.globalbraininstitute.challprop.scenarios

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulationParallel
import org.globalbraininstitute.challprop.mocks.NetworkMocks
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.ITestContext
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeMethod
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test

import static org.testng.Assert.assertEquals
import static org.testng.Assert.assertNotEquals

public class ControlledPropagationTestScenario1ParallelTestMethods {
	static {
		Gremlin.load();
	}
	static Network network = Globals.network;
	private Logger logger = LoggerFactory.getLogger(ControlledPropagationTestScenario1ParallelTestMethods.class);
	static RealVector needVector;
	static Integer processingStepsAllowed;

	/**
	 * in the network of where all agents have need vectors as unit vectors to one and processing
	 * matrixes able to zero out unit vectors (extract all benefit). all situations are designed to be
	 * interpreted as unit challenge vectors, so  each agent should take it and reduce to zero. The result
	 * should be no topology in the network in terms of link weights, as challenges get processed only once.
	 * The network is not connected = agents do not know each other
	 */

	@BeforeTest
	public void init(ITestContext context) {
	  // getting parameters from textNg xml file
	  def simulationParameters = context.getCurrentXmlTest().getParameters()
	  Parameters.simulationParameters = simulationParameters
	  //creating a network with predefined agents
	  def preset = simulationParameters.preset.toDouble()
	  def dimensions = network.god.asVertex().dimensions
	  def matrix = A.newPresetMatrix(0)
	  this.needVector = A.newPresetVector(preset)
	  def skillVector = A.newPresetVector(0)
	  def processingVector = A.newPresetVector(preset)
	  
	  def startTime = System.currentTimeMillis()
	  
	  List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock5(simulationParameters.NUMBER_OF_AGENTS.toInteger(), 
		  												Utils.serializeVector(needVector), 
														Utils.serializeVector(skillVector), 
														Utils.serializeVector(processingVector))
	  
	  def networkCreatedFinish = System.currentTimeMillis()
	  println "Network creation: " + (networkCreatedFinish - startTime)/1000 + " seconds"
	  
	  NetworkMocks.createParallelAgents(framedAgentsList, "ProcessingBySubtraction")
	  
	  def agentsCreatedFinish = System.currentTimeMillis()
	  println "Parallel agents creation (with individual pipelines): " + (agentsCreatedFinish - networkCreatedFinish)/1000 + " seconds"
	  (new ControlledSimulationParallel(simulationParameters))
	  		.equalDistributionProcessingByVectorSubtractionParallel()
			  
	  def finishTime = System.currentTimeMillis()
	  println "Controlled simulation: " + (finishTime - agentsCreatedFinish)/1000 + " seconds."
	  println "Simulation run time: " + (finishTime - startTime)/1000 + " seconds."
	}
	
	@BeforeMethod
	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
	
	@Test
	public void thereIsSomeTopologyInTheNetwork() {
				
		def no_knows_edges_created =  network.g.baseGraph.V.has('type','agent').outE('knows').toList() == []
		if (!no_knows_edges_created) {
			def knows_weight_sum = network.g.V.has('type','agent').outE('knows').weight.sum()
			def knows_link_count = network.g.V.has('type','agent').outE('knows').count()
			assert no_knows_edges_created == false
			assertNotEquals(0.01d,knows_weight_sum/knows_link_count,0.1E-13d)
		} else {
			assert no_knows_edges_created == true
		}
	}
			
	@Test
	public void allSituationsProcessedCertainNumberOfTimes() {	

		List challs = network.god.asVertex().outE().inV().has('type','situation').outE().has('source',network.god.asVertex().id).toList()

		challs.each{
			List allSteps = Functions.furtherAll(it)
			def processingSteps = 0
			for (Edge challenge : allSteps) {
				if (challenge.getLabel() == "processed") {
					processingSteps = processingSteps + 1
				}
			}
			def processingStepsExpected = this.processingStepsAllowed 
			assertEquals(processingSteps, processingStepsExpected, "Challenge with identity "+it.identity+" is processed more than once")
		}
		
	}
	
	@Test
	public void sumOfBenefitsEqualsToSumOfInitialIntensities() {
		def sumOfBenefits = network.god.asVertex().outE('knows').inV().has('type','agent').benefit.sum()
		def intensitySum = 0
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().toList().each{
			initialSituationVectors.add(it.outE('logged')[0].inV().next().vector)}
		initialSituationVectors.each{
			intensitySum += Utils.deserializeVector(it).subtract(needVector).getL1Norm()
		}
		assert sumOfBenefits == intensitySum
		
	}
	
	@Test
	public void sumOfBenefitsEqualsToExtractedIntensities() {
		def sumOfBenefits = network.god.asVertex().outE('knows').inV().has('type','agent').benefit.sum()

		def intensitySum = 0
		def finalSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			finalSituationVectors.add(it.outE('logged').toList().reverse()[0].inV().next().vector)}
		def initialSituationVectors = []
		network.god.asVertex().outE('created').inV().each{
			initialSituationVectors.add(it.outE('logged')[0].inV().next().vector)}
		
		assert initialSituationVectors.size() == finalSituationVectors.size()
		for (int c = 0; c <initialSituationVectors.size();c++) {
			intensitySum += Utils.deserializeVector(initialSituationVectors[c]).subtract(Utils.deserializeVector(finalSituationVectors[c])).getL1Norm()
		}
			
		assert sumOfBenefits == intensitySum
		
	}

	
	@Test
	public void allSituationVectorsAfterSimulationAreFilledWithOnes() {
		boolean bool = true
		network.god.asVertex().outE('created').inV.each{
			bool = bool & it.vector == '1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0'
			}
		assert bool == true 
	}
	
	@AfterClass
	public static void commit() {
		network.commit()
		//network.reinit()
	}
	

}
