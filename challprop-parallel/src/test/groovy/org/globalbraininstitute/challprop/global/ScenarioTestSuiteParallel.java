package org.globalbraininstitute.challprop.global;

import org.globalbraininstitute.challprop.scenarios.ControlledPropagationTestScenario1Parallel;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(value = Suite.class)
@SuiteClasses({
		ControlledPropagationTestScenario1Parallel.class
		})

public class ScenarioTestSuiteParallel {
	
    @BeforeClass 
    public static void setUpClass() {      
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		public void run() {
    		  Globals.network.finalize();
    		}
    	});
   	}

    @AfterClass 
    public static void tearDownClass() {
        Globals.network.commit();
    }
    
}
