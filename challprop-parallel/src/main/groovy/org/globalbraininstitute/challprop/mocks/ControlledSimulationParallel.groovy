package org.globalbraininstitute.challprop.mocks

import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import groovyx.gpars.actor.Actors
import org.globalbraininstitute.challprop.actors.ChallengeGenerator
import org.globalbraininstitute.challprop.actors.ParallelAgent
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.functions.*
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ControlledSimulationParallel {
	static Network network = Globals.network
	ArrayList<FramedAgent> agentsList = network.agentsMap.keySet().toArray()
	ArrayList<ParallelAgent> parallelAgentsList = network.agentsMap.values().toArray()
	Map simulationParameters = Parameters.simulationParameters
	private Logger logger = LoggerFactory.getLogger(ControlledSimulation.class);

	public ControlledSimulationParallel(ArrayList<FramedAgent> agentsList) {
		
		this.agentsList = agentsList
	
	}
	
	public ControlledSimulationParallel(Map simulationParameters) {
		this.simulationParameters = simulationParameters
	}
	
	public void runWithBulkSituationGeneration(Integer numberOfSituations) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())

		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		def situationList = []
			for (int no = 0; no < numberOfSituations; no++ ){
				FramedSituation situation = network.god.addNewSituation()
				situation.addNewInstance().setVector(situation.getVector())
				// these situations will be interpreted by all agents as unit vectors which is the same as their need vectors
				situationList.add(situation)
			}
			
			generationPipe.setStarts(situationList)
			def generationPipeResults = generationPipe.toList()
			network.commit()
			
			pipeline.setStarts(agentsList)
			pipeline.toList()
			network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
	}

	public void oneByOneSituationGenerationWithPreset(Integer numberOfCycles, Double preset) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())

		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		for (int cycle = 0; cycle < numberOfCycles; cycle++ ){
			FramedSituation situation = network.god.addNewSituation()
			def situationVector = Utils.serializeVector(A.newPresetVector(preset))
			situation.setVector(situationVector)
			situation.addNewInstance().setVector(situation.getVector())
			// these situations will be interpreted by all agents as unit vectors which is the same as their need vectors
			
			generationPipe.setStarts(Arrays.asList(situation))
			def generationPipeResults = generationPipe.toList()
			network.commit()
			
			pipeline.setStarts(agentsList)
			pipeline.toList()
			network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
		}
		
	}

	public void bulkSituationGenerationWithPreset(Integer numberOfSituations, Double preset) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())

		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
	  def situationList = [] 
	  for (int no = 0; no < numberOfSituations; no++ ){
		  FramedSituation situation = network.god.addNewSituation()
		  def situationVector = Utils.serializeVector(A.newPresetVector(preset*2))
		  situation.setVector(situationVector)
		  situation.addNewInstance().setVector(situation.getVector())
		  // these situations will be interpreted by all agents as unit vectors which is the same as their need vectors
		  situationList.add(situation)   
	  }
		  
		  generationPipe.setStarts(situationList)
		  def generationPipeResults = generationPipe.toList()
		  network.commit()
		  
		  for (int x = 0; x<Parameters.parameters.bufferSize; x++) {
			  pipeline.setStarts(agentsList)
			  pipeline.toList()
			  network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
		  }		
	}

	
	public void distributeChallengesForAllAgents(Integer numberOfSituations) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)

		def situationList = []
			for (int no = 0; no < numberOfSituations; no++ ){
				FramedSituation situation = network.god.addNewSituation()
				situation.addNewInstance().setVector(situation.getVector())
				for (int a = 0; a < agentsList.size(); a++) {
					def challenge =  situation.addChallengeToAgent(agentsList[a])
					challenge.setIdentity(situation.identity)
					challenge.setSource(network.god.asVertex().getId())
				}
				situationList.add(situation)
			}
			
			pipeline.setStarts(agentsList)
			pipeline.toList()
			network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
	}

	public void basicRun(Integer numberOfSituations, Integer cycles) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		def situationList = []
		for (int no = 0; no < numberOfSituations; no++ ){
			FramedSituation situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			situationList.add(situation)
		}
		
		generationPipe.setStarts(situationList)
		generationPipe.toList()
		network.commit()
		
		for (int cycle = 0; cycle<cycles;cycle++) {
			pipeline.setStarts(agentsList)
			pipeline.toList()
			network.commit()
		} 
	}
		
	public void randomDistributionProcessingByVectorSubraction(Integer numberOfSituations, Integer cycle, Double preset) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingBySubtraction())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		int generations = numberOfSituations/agentsList.size()/Parameters.parameters.bufferSize*2+1
		int situationsPerGeneration = numberOfSituations/generations
		for (int g = 1; g <= generations;g++) {
			// the above automatically determines the number of situations inputed into simulation each generation
			def situationList = SituationMocks.situationsWithPreset(situationsPerGeneration, preset)
			generationPipe.setStarts(situationList)
			generationPipe.toList()
			network.commit()
		
			for (int i = 0; i<=cycle;i++) {
				pipeline.reset()
				pipeline.setStarts(agentsList)
				pipeline.toList()
				network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
			}
		}
	}

	public void equalDistributionProcessingByVectorSubraction(Integer numberOfSituations, Integer cycle, Double preset) {
		
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingBySubtraction())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		
		def situationList = SituationMocks.situationsWithPreset(numberOfSituations, preset)
		def maxAgentIndex = agentsList.size() -1
		def index =0
		situationList.each{situation ->
			def challenge =  situation.addChallengeToAgent(agentsList[index])
			challenge.setIdentity(situation.identity)
			challenge.setSource(network.god.asVertex().getId())
			index = index + 1
			if (index == maxAgentIndex) index =0
		}

		for (int i = 0; i<=cycle;i++) {
			pipeline.reset()
			pipeline.setStarts(agentsList)
			pipeline.toList()
			network.commit() // this commit is needed not to have duplicate challenges (hell knows why...)
		}
	}
	
	/**
	 * Parallel implementation of the similar method (above)
	 */
	public void equalDistributionProcessingByVectorSubtractionParallel() {
		
		Actors.defaultActorPGroup.resize(simulationParameters.POOL_SIZE.toInteger())

		def startTime = System.currentTimeMillis()

		parallelAgentsList.each {it.start()}
		def situationPreset = simulationParameters.preset.toDouble() + simulationParameters.processingStepsAllowed.toInteger()
		ChallengeGenerator chalengeGenerator = new ChallengeGenerator(simulationParameters.challengesGenerationStep.toInteger(), 
																simulationParameters.SLEEP_TIME.toInteger(),
																situationPreset).start()
		//this forces main thread to live until both actors stop
		[parallelAgentsList, chalengeGenerator]*.join()
	}
	

	public static void generation(Integer numberOfSituations) {
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		
		def situationList = SituationMocks.situationsWithoutPreset(numberOfSituations)
		generationPipe.setStarts(situationList)
		generationPipe.toList()
		network.commit()

	}
	
	public static void propagation(List<FramedAgent> agents, Integer cycles) {
		
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		
		for (int cycle = 0; cycle<cycles;cycle++) {
			pipeline.setStarts(agents)
			pipeline.toList()
			network.commit()
		}
	}

	public void propagationParallel() {
		
		Actors.defaultActorPGroup.resize(simulationParameters.POOL_SIZE.toInteger())

		parallelAgentsList.each {it.start()}
		ChallengeGenerator chalengeGenerator = new ChallengeGenerator().start()
		//this forces main thread to live until both actors stop
		[parallelAgentsList, chalengeGenerator]*.join()
	}

	
	public void centralAgent(Integer numberOfSituations, Double situationPreset, Integer cycles, String... processingStyle) {
		Parameters.parameters.maxBranchingFactor = 1
		
		def start = System.nanoTime()
		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe
		if (processingStyle.length > 0 && processingStyle[0] == "processingVector") {
			processingPipe = new TransformFunctionPipe(new ProcessingBySubtraction())
		} else {
			processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
		}
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())
		
		Pipeline pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		logger.warn("=Pipeline construction={}", System.nanoTime() - start)
		
		start = System.nanoTime()
		def situationList = SituationMocks.situationsWithPreset(numberOfSituations,situationPreset)
		logger.warn("Constructing {} situations with preset={}", numberOfSituations, System.nanoTime() - start)
		for (int no = 0; no < numberOfSituations; no++ ){
			def situation = situationList[no]
			// all situations are submitted to centralAgent first for the start of propagation
			start = System.nanoTime()
			def challenge = situation.addChallengeToAgent(agentsList[0])
			logger.warn("=God challenges an agent with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
			challenge.setIdentity(situation.identity)
			challenge.setSource(network.god.asVertex().getId())
			
			//every situation is processed individually
			
			for (int cycle = 0; cycle<cycles;cycle++) {
				start = System.nanoTime()
				pipeline.setStarts(agentsList)
				pipeline.toList()
				network.commit()
				logger.warn("=Performing a cycle={}", System.nanoTime()- start)
			}
	
		}
		
	}

}
