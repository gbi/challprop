/**
 * Create a two clusters of agents:
 * First cluster has needVector with large values - these agents will on average get less benefit from processing challenges
 * Second cluster has needVector with small values - these agents will process anything that has intensity a little higher 
 * than zero .The average benefit of first cluster should be considerably smaller than second
 * Inter and intra cluster link numbers and link strengths should also be different  
 * @author vveitas 
 * 
 */

package org.globalbraininstitute.challprop.experiments

import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters

import java.nio.file.Files

Globals.network.gracefulShutdown()
Files.copy(new File("log/challprop.log").toPath(),new File(Parameters.parameters.experimentPath+"/challprop.log").toPath())