package org.globalbraininstitute.challprop.experiments

import groovyx.gpars.actor.Actors
import groovyx.gpars.actor.DefaultActor
import org.globalbraininstitute.challprop.actors.ParallelAgent
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.ControlledSimulationParallel
import org.globalbraininstitute.challprop.mocks.NetworkMocks

class ExecuteAgentsInParalelExpr extends DefaultActor {

	static List<ParallelAgent> parallelAgentsList;
	static long sleepTime = 100
	
	public static void main(String[] args) {

		// Populating a global Map of parameters
		Parameters.simulationParameters.put("NUMBER_OF_AGENTS",40);
		Parameters.simulationParameters.put("NUMBER_OF_CHALLENGES_NEED_TO_BE_CREATED",600);
		/**		Actually from the current design pool size = number of generated challenges * Number
		 * 		of processed challenges when the actor receives a challenge in his message queue.  */
		Parameters.simulationParameters.put("POOL_SIZE",10);
		Parameters.simulationParameters.put("ACTIVENESS_FACTOR",10);
		Parameters.simulationParameters.put("SLEEP_TIME",50);
		Parameters.simulationParameters.put("challengesGenerationStep",5);
		Parameters.simulationParameters.put("maxNumberOfActiveChallenges",30);
		Parameters.simulationParameters.put("minNumberOfActiveChallenges",10);
		Parameters.simulationParameters.put("NUMBER_OF_SITUATIONS",600);
	
		Actors.defaultActorPGroup.resize(Parameters.simulationParameters.POOL_SIZE)

		
		def startTime = System.currentTimeMillis()

		List<FramedAgent> framedAgentsList = NetworkMocks.createNetworkMock6(Parameters.simulationParameters.NUMBER_OF_AGENTS)

		parallelAgentsList = NetworkMocks.createParallelAgents(framedAgentsList, 'ProcessingByMultiplication')
		
		(new ControlledSimulationParallel(Parameters.simulationParameters))
			.propagationParallel()

	}
}
