package org.globalbraininstitute.challprop.mocks

import com.tinkerpop.frames.FramedGraph
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class SituationMocks {

	static Network network = Globals.network
	static FramedGraph framedGraph = network.g
	private Logger logger = LoggerFactory.getLogger(SituationMocks.class)

	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
	
	public static List<FramedSituation> situationsWithPreset(Integer numberOfSituations, Double preset) {
		def situationList = []
		for (int no = 0; no < numberOfSituations; no++ ){
			FramedSituation situation = network.god.addNewSituation()
			def situationVector = Utils.serializeVector(A.newPresetVector(preset))
			situation.setVector(situationVector)
			situation.addNewInstance().setVector(situation.getVector())
			// these situations will be interpreted by all agents as unit vectors which is the same as their need vectors
			situationList.add(situation)
		}
		return situationList
	}
	
	public static List<FramedSituation> situationsWithoutPreset(Integer numberOfSituations) {
		def situationList = []
		for (int no = 0; no < numberOfSituations; no++ ){
			FramedSituation situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			situationList.add(situation)
		}
		return situationList
	}	
														
}
