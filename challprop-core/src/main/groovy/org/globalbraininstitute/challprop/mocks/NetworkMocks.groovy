package org.globalbraininstitute.challprop.mocks

import com.tinkerpop.frames.FramedGraph
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.actors.ParallelAgent
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Algorithms as A
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class NetworkMocks {

	static Network network = Globals.network
	static FramedGraph framedGraph = network.g
	static private Logger logger = LoggerFactory.getLogger(NetworkMocks.class)

	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
	
	/**
	 * NetowrkMock1 is a network of agents whith number of agents = buffer size + 1; 
	 * The first agent is connected to all the other agents with increasing weight (trust)
	 * @return
	 */
	public static List<FramedAgent> createNetworkMock1() {
		FramedAgent framedAgent1 = network.g.addVertex(null,FramedAgent.class)
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		list.add(framedAgent1)
		network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		
		def bufferSize = framedAgent1.getBufferSize()
		for (int x = 0; x <bufferSize; x++) {
			FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
			framedAgent.setNeedVector(framedAgent1.getNeedVector())
			framedAgent.setSkillVector(framedAgent1.getSkillVector())
			framedAgent.setProcessingMatrix(framedAgent1.getProcessingMatrix())
			list.add(framedAgent)
			network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}
			Knows link = framedAgent.addKnowsAgent(framedAgent1)
			link.weight = x+1.0d
		}
		network.commit()
		return list
	}
	
	/**
	 * NetworkMock2 is a network of one agent (connected to God of course)...
	 * @return
	 */
	public static List<FramedAgent> createNetworkMock2() {
		FramedAgent framedAgent1 = network.g.addVertex(null,FramedAgent.class)
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		list.add(framedAgent1)
		network.god.addAgentKnows(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(framedAgent1).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		
		network.commit()
		return list
	}
	
	/**
	 * NetworkMock3 is a network of agents connected in a row..
	 * Takes an argument - a number of agents
	 * @return
	 */
	public static List<FramedAgent> createNetworkMock3(Integer numberOfAgents) {
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		for (int i = 0; i<numberOfAgents;i++) {
			FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
			list.add(framedAgent)
			network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}
		}
		for (int i = 0; i<numberOfAgents-1;i++) {
			list[i].addAgentKnows(list[i+1])
			if (Parameters.parameters.reciprocityRate != 0) {
				list[i].addKnowsAgent(list[i+1])
			}
		}
		network.commit()
		return list
	}
	
	/**
	 * NetworkMock4 is a network of arbitrary number of agents which are absolutely identical in terms of 
	 * their processing matrix, need vector and all the other stuff. All parameters are submitted externally
	 * (i.e. not generated randomly) 
	 * @param numberOfAgents
	 * @return
	 */
	public static List<FramedAgent> createNetworkMock4(Integer numberOfAgents, \
														RealVector needVector,\
														RealVector skillVector,\
														RealMatrix processingMatrix) {
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		for (int i = 0; i<numberOfAgents;i++) {
			FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
			framedAgent.setNeedVector(Utils.serializeVector(needVector))
			framedAgent.setSkillVector(Utils.serializeVector(skillVector))
			framedAgent.setProcessingMatrix(Utils.serializeMatrix(processingMatrix))
			list.add(framedAgent)
			network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}
		}
		network.commit()
		return list
	}
	
	/**
	* NetworkMOck5 is a network of arbitrary number of identical agents with the processing Vector instead of processing 
	* matrix. This network mock is designed for the ControlledPropagationTest.scenario1												
	 */
	public static List<FramedAgent> createNetworkMock5(Integer numberOfAgents, \
														String needVector,\
														String skillVector,\
														String processingVector) {
			List<FramedAgent> list = new ArrayList<FramedAgent>()
			for (int i = 0; i<numberOfAgents;i++) {
				FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
				framedAgent.setNeedVector(needVector)
				framedAgent.setSkillVector(skillVector)
				framedAgent.setProcessingVector(processingVector)
				list.add(framedAgent)
				network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
				if (Parameters.parameters.reciprocityRate != 0) {
					network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
				}
			}
			network.commit()
			return list
		}
	
	/**
	 * Creates simple network with the given number of agents 
	 */
														
	public static List<FramedAgent> createNetworkMock6(Integer numberOfAgents) {
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		for (int i = 0; i<numberOfAgents;i++) {
			FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
			list.add(framedAgent)
			network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}
		}
		network.commit()
		return list
	}
	
	public static List<FramedAgent> createNetworkMock7(Integer totalPropagations, Integer networkBranchingFactor, RealVector needVector, RealVector skillVector) {
		def start = System.nanoTime()
		def dimensions = network.god.asVertex().dimensions
		
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		FramedAgent centralAgent = network.g.addVertex(null,FramedAgent.class)
		list.add(centralAgent)
		centralAgent.setNeedVector(Utils.serializeVector(needVector))
		centralAgent.setSkillVector(Utils.serializeVector(skillVector))
		centralAgent.setProcessingMatrix(Utils.serializeMatrix(A.newPresetMatrix(
			Utils.matrixComponent(totalPropagations,dimensions,1))))
		network.god.addAgentKnows(centralAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(centralAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		def ringsForMock7 = {FramedAgent agent, Integer propagations -> 
			for (int branch = 1; branch <=networkBranchingFactor; branch++) {
				FramedAgent child = network.g.addVertex(null,FramedAgent.class);
				agent.addAgentKnows(child)
				if (Parameters.parameters.reciprocityRate != 0) {
					child.addAgentKnows(agent)
				}
				child.setNeedVector(Utils.serializeVector(needVector))
				child.setSkillVector(Utils.serializeVector(skillVector))
				if (propagations > 0) {
					child.setProcessingMatrix(Utils.serializeMatrix(A.newPresetMatrix(
					Utils.matrixComponent(totalPropagations,dimensions,totalPropagations-propagations+2))))
				} 
				network.god.addAgentKnows(child).setWeight(Parameters.parameters.linkToGodWeight as Double)
				if (Parameters.parameters.reciprocityRate != 0) {
					network.god.addKnowsAgent(child).setWeight(Parameters.parameters.linkToGodWeight as Double)
				}
				list.add(child)
				if (propagations > 0) {
					call(child,propagations-1)
				} 
			}
		}
		ringsForMock7(centralAgent,totalPropagations)
		logger.warn("=NetworkMock7 with noOfAgents={};={}", list.size().toString(), System.nanoTime()- start)

		return list
	}

	public static List<FramedAgent> createNetworkMock8(Integer totalPropagations, 
														Integer networkBranchingFactor, 
														String needVector, 
														String skillVector,
														String processingVector) {
		def start = System.nanoTime()
		def dimensions = network.god.asVertex().dimensions
		
		List<FramedAgent> list = new ArrayList<FramedAgent>()
		FramedAgent centralAgent = network.g.addVertex(null,FramedAgent.class)
		list.add(centralAgent)
		centralAgent.setNeedVector(needVector)
		centralAgent.setSkillVector(skillVector)
		centralAgent.setProcessingVector(processingVector)
		network.god.addAgentKnows(centralAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
		if (Parameters.parameters.reciprocityRate != 0) {
			network.god.addKnowsAgent(centralAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
		}
		def ringsForMock8 = {FramedAgent agent, Integer propagations ->
			for (int branch = 1; branch <=networkBranchingFactor; branch++) {
				FramedAgent child = network.g.addVertex(null,FramedAgent.class);
				agent.addKnowsAgent(child)
				child.setNeedVector(needVector)
				child.setSkillVector(skillVector)
				child.setProcessingVector(processingVector)
				network.god.addAgentKnows(child).setWeight(Parameters.parameters.linkToGodWeight as Double)
				if (Parameters.parameters.reciprocityRate != 0) {
					network.god.addKnowsAgent(child).setWeight(Parameters.parameters.linkToGodWeight as Double)
				}
				list.add(child)
				if (propagations > 0) {
					call(child,propagations-1)
				}
			}
		}
		ringsForMock8(centralAgent,totalPropagations)
		logger.warn("=NetworkMock8 with noOfAgents={};={}", list.size().toString(), System.nanoTime()- start)

		return list
	}
	
	/**
	 * Creates a network where agents are falling into predefined clusters according to their parameters
	 * Map: parameters is a map with list of parameters for each cluster of agents in a format
	 * parameters of cluster: agent number and needVector
	 * [cluster1:[agentNumber:1,needVector:...],cluster2:[...]] and so on
	 * a method should be able to create as many clusters supplied in a map
	 * cluster names go as properties for agents just to distinguish that during analysis
	 */
	public static List<FramedAgent> createNetworkMockWithClusters(Map parameters) {
				List<FramedAgent> list = new ArrayList<FramedAgent>()
				parameters.each{clusterName,clusterParameters ->
					for (int a = 0;a<clusterParameters.agents; a++) {
						FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
						framedAgent.asVertex().setProperty("cluster", clusterName)
						framedAgent.setNeedVector(clusterParameters.needVector)
						list.add(framedAgent)
						network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
						if (Parameters.parameters.reciprocityRate != 0) {
							network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
						}
					}
				}
				network.commit()
				return list
			}
	/**
	 * Encapsulates each already created framed agent into a parallel agent
	 * This allows to use existing Network Mocks for testing
	 * @param framedAgents
	 * @param parameters
	 * @return List<ParallelAgent>
	 * @author vveitas
	 */
	public static List<ParallelAgent> createParallelAgents(List<FramedAgent> framedAgentsList, String processingPipeName) {
			def parallelAgents = []
			framedAgentsList.each{ framedAgent ->
				ParallelAgent parallelAgent = new ParallelAgent(framedAgent, Parameters.simulationParameters.ACTIVENESS_FACTOR.toInteger(),processingPipeName)
				network.agentsMap.put(framedAgent, parallelAgent)
				assertNotNull(network.agentsMap.get(framedAgent))
			}
			return parallelAgents
		}
	
	public static List<ParallelAgent> createParalleAgents(Map parameters) {
		List<ParallelAgent> list = new ArrayList<FramedAgent>()
		parameters.each{clusterName,clusterParameters ->
			for (int a = 0;a<clusterParameters.agents; a++) {
				FramedAgent framedAgent = network.g.addVertex(null,FramedAgent.class)
				framedAgent.asVertex().setProperty("cluster", clusterName)
				framedAgent.setNeedVector(clusterParameters.needVector)
				ParallelAgent parallelAgent = new ParallelAgent (framedAgent, clusterParameters.activenessFactor)
				list.add(parallelAgent)
				network.god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
				if (Parameters.parameters.reciprocityRate != 0) {
					network.god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
				}
				network.agentsMap.put(framedAgent, parallelAgent)
				assertNotNull(network.agentsMap.get(framedAgent))
			}
		}
		network.commit()
		return list
	}
}
