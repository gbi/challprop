package org.globalbraininstitute.challprop.actors

import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import groovyx.gpars.actor.DefaultActor
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.functions.*
import org.globalbraininstitute.challprop.global.Globals

/**
 * The Basic model for the Parallel agents running, The default 
 * @author safwat
 */
class ParallelAgent extends DefaultActor {

	/**
	 * activenessFactor is the number of challenges that will be detached from the queue (i.e. that 
	 * will be processed by the agent) when new challenge is assigned to the agent.
	 */
	private int activenessFactor
	private FramedAgent framedAgent
	//TODO  This part will be removed as there is no need for this list.
	private List<FramedAgent> framedAgents = new ArrayList<FramedAgent>()
	private Pipeline pipeline


	/**
	 * Temporary method for allowing the pipeline of agents to be differently instantiated. Eg. processing
	 * functions can differ depending on experiment. In the future this should be changed by recording the list
	 * (and order) of pipes to be instantiated into the pipeline of individual agent to the graph (as vertex 
	 * properties) and then instantiating these classes using Groovy reflection.
	 * @param framedAgent
	 * @param activenessFactor
	 * @param processingStyle "ProcessingByMultiplication" or "ProcessingbySubtraction"
	 * @author vveitas
	 */
	public ParallelAgent(FramedAgent framedAgent, int activenessFactor, String processingStyle) {
		super()
		this.framedAgent = framedAgent
		this.activenessFactor = activenessFactor

		Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
		Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
		Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased("updateBest"))
		Pipe filterOutNullPipe = new FilterFunctionPipe(new FilterOut(null))
		Pipe processingPipe = processingStyle == "ProcessingByMultiplication" \
			? new TransformFunctionPipe(new ProcessingByMultiplication()) \
			: new TransformFunctionPipe(new ProcessingBySubtraction())
		Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
		Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
		Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
		Pipe propagationPipe = new TransformFunctionPipe(new Propagation())

		pipeline =  new Pipeline(preprocessingPipe, bufferingPipe,\
			selectionPipe, filterOutNullPipe, processingPipe, reinterpretationPipe, preferenceUpdatePipe, \
			situationUpdatePipe, propagationPipe)
		framedAgents.add(this.framedAgent)
	}
	
	@Override
	protected void act() {
		loop {
			react {
				for(int counter =0; counter<activenessFactor; counter++) {
					pipeline.setStarts(framedAgents)
					pipeline.toList()
					Globals.network.commit()
					// After the commit is finished successfully decrement the active challenges count.
					//ChallengeGenerator.decrementNumberOfChallenges()
				}
			}
		}
	}
	
	@Override
	boolean equals(Object comparedObject) 
	{
		boolean result = false;
		if(comparedObject !=null && comparedObject instanceof ParallelAgent)
		{
			ParallelAgent comparedParallelAgent = (ParallelAgent)comparedObject;
			result = framedAgent.equals(comparedParallelAgent.framedAgent)
		}
		return result;
	}
	
//	@Override
//	public int hashCode() {
//		return framedAgent.hashCode()
//	}
}
