package org.globalbraininstitute.challprop.actors

import groovyx.gpars.actor.DefaultActor
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.mocks.SituationMocks
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * Actor who generates the challenges and push them in the network, also it keeps the number of
 * active challenges within certain operating range. 
 * @author safwat
 */
class ChallengeGenerator extends DefaultActor {

	private static Logger logger = LoggerFactory.getLogger(ChallengeGenerator.class);

	private long minNumberOfActiveChallenges
	private long maxNumberOfActiveChallenges
	public static long currentNumberOfActiveChallenges
	private static long totalGeneratedChallenges
	private List<ParallelAgent> parallelAgentsList
	
	private Integer challengesGenerationStep
	private long sleepTime
	Network network = Globals.network
	private Double preset

	// constructor for challenge generator with 'processingBySubtraction'
	public ChallengeGenerator(Integer challengesGenerationStep, long sleepTime,Double preset) {
		super()
		this.challengesGenerationStep = challengesGenerationStep
		this.sleepTime = sleepTime
		this.parallelAgentsList = network.agentsMap.values().toArray()
		this.preset = preset
	}
	// constructor for challenge generator with 'processingBySubtraction'
	public ChallengeGenerator() {
		this.parallelAgentsList = network.agentsMap.values().toArray()
		this.sleepTime = Parameters.simulationParameters.SLEEP_TIME
	}

	@Override
	protected void act() {
		while (totalGeneratedChallenges<Parameters.simulationParameters.NUMBER_OF_SITUATIONS.toInteger()) {
				def situationList
				if (this.preset == null) {
					situationList = SituationMocks.situationsWithoutPreset(Parameters.simulationParameters.NUMBER_OF_SITUATIONS)
				} else {
					situationList = SituationMocks.situationsWithPreset(challengesGenerationStep, preset)
				}
					
					situationList.each {
			 			ParallelAgent parallelAgent = assignChallengeRandomly(it)
						network.commit()
						incrementNumberOfSituations()
						parallelAgent.send()
			}
			Thread.sleep(sleepTime)
		}
	}

	/**
	 *  Generates challenge and assigns it to the agent and updated the Network model with these changes. 
	 */
	public ParallelAgent assignChallengeRandomly(FramedSituation situation) {
		assertNotNull(situation)
		// get the random agent in the network
		ParallelAgent parallelAgent
		parallelAgent = parallelAgentsList[new Random().nextInt(Parameters.simulationParameters.NUMBER_OF_AGENTS.toInteger())]

		// challenge it with the new instance of a situation
		def challenge =  situation.addChallengeToAgent(parallelAgent.framedAgent)
		challenge.setIdentity(situation.identity)
		challenge.setSource(Globals.network.god.asVertex().getId())
		logger.debug("Generated a challenge {} with identity {}", challenge.toString(), challenge.getIdentity())
		return parallelAgent
	}
	
	public static void incrementNumberOfSituations()
	{
		currentNumberOfActiveChallenges++
		totalGeneratedChallenges++
		logger.debug("incrementNumberOfChallenges")
		logger.debug("Total generated challenges are [" + totalGeneratedChallenges + "], and Current number of active challenges are [" + currentNumberOfActiveChallenges + "]")
	}
	
	public static void decrementNumberOfChallenges()
	{
		currentNumberOfActiveChallenges--
		logger.debug("decrementNumberOfChallenges")
		logger.debug("Total generated challenges are [" + totalGeneratedChallenges + "], and Current number of active challenges are [" + currentNumberOfActiveChallenges + "]")
	}
}
