package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.frames.Incidence
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.VertexFrame
import com.tinkerpop.frames.annotations.gremlin.GremlinGroovy
import com.tinkerpop.frames.annotations.gremlin.GremlinParam
import org.globalbraininstitute.challprop.frames.annotations.TimedAdjacency

public interface FramedAgent extends VertexFrame {
	
	// properties of an agent:

	@Property("identity")
	public void setIdentity(String)
	@Property("identity")
	public String getIdentity()
		
	@Property("type")
	public void setType(String)
	@Property("type")
	public String getType()
	
	@Property("needVector")
	public void setNeedVector(String)
	@Property("needVector")
	public String getNeedVector()
	
	@Property("skillVector")
	public void setSkillVector(String)
	@Property("skillVector")
	public String getSkillVector()
	
	@Property("processingMatrix")
	public void setProcessingMatrix(String)
	@Property("processingMatrix")
	public String getProcessingMatrix()

	// property needed for constructing ControlledPropagationTest.Scenario1
	@Property("processingVector")
	public void setProcessingVector(String)
	@Property("processingVector")
	public String getProcessingVector()
		
	@Property("bufferSize")
	public void setBufferSize(Integer) // number in 
	@Property("bufferSize")
	public int getBufferSize()

	@Property("benefit")
	public void setBenefit(Double)
	@Property("benefit")
	public Double getBenefit()
	
	@Property("learningRate")
	public void setLearningRate(Double)
	@Property("learningRate")
	public Double getLearningRate()

	// Create knows Link
	@Incidence(label = 'knows', direction = Direction.OUT)
	public Knows addKnowsAgent(final FramedAgent agent)

	// Create agent knows Link (
	@Incidence(label = 'knows', direction = Direction.IN)
	public Knows addAgentKnows(final FramedAgent agent)

	// Links to other agents
	@Incidence(label = 'knows', direction = Direction.OUT)
	public Iterable<Knows> getKnowsLinks()

	@TimedAdjacency(label = 'knows', direction = Direction.OUT)
	public Iterable<FramedAgent> getKnowsAgents()

	// Links from other agents
	@Incidence(label = 'knows', direction =Direction.IN)
	public Iterable<Knows> getIsKnownByLinks()

	@TimedAdjacency(label = 'knows', direction = Direction.IN)
	public Iterable<FramedAgent> getIsKnownByAgents()
	
	@GremlinGroovy(value="it.outE('knows').inV.filter{it.id == toAgentId}.toList()", frame=false)
	public List getKnowsLinksToAgent(@GremlinParam("toAgentId") java.lang.Long toAgentId)


	@GremlinGroovy(value="it.outE('knows').inV.has('type','agent').back(2).toList()", frame=false)
	public List getKnowsLinksToAgents()

	@GremlinGroovy(value="it.inE('knows').outV.has('type','agent').back(2).toList()", frame=false)
	public List getKnowsLinksFromAgents()


	/** Get incoming link from an agent**/
	@GremlinGroovy(value="it.inE('knows').outV.filter{it.identity == fromAgentId}.back(2).next()", frame=false)
	public Edge getLinkFromAgent(@GremlinParam("fromAgentId") java.lang.String fromAgentId)

	/** Get ALL incoming links from an agent as list (in case list is empty) **/
	@GremlinGroovy(value="it.inE('knows').outV.filter{it.identity == fromAgentId}.bask(2).toList()", frame=false)
	public List getLinkSFromAgent(@GremlinParam("fromAgentId") java.lang.String fromAgentId)

	// how much an agent trusts you
	@GremlinGroovy(value="it.inE('knows').outV.filter{it.identity == fromAgentId}.back(2).next().weight", frame=false)
	public double getTrustOfAgent(@GremlinParam("fromAgentId") java.lang.String fromAgentId)

	/** Get outgoing link to an agent**/
	@GremlinGroovy(value="it.outE('knows').inV.filter{it.identity == toAgentId}.back(2).next()", frame=false)
	public Edge getLinkToAgent(@GremlinParam("toAgentId") java.lang.String toAgentId)

	/** Get ALL outgoing links to an agent as a list**/
	@GremlinGroovy(value="it.outE('knows').inV.filter{it.identity == toAgentId}.back(2).toList()", frame=false)
	public List getLinkSToAgent(@GremlinParam("toAgentId") java.lang.String toAgentId)

	// how much you trust an agent
	@GremlinGroovy(value="it.outE('knows').inV.filter{it.identity == toAgentId}.back(2).next().weight", frame=false)
	public double getTrustToAgent(@GremlinParam("toAgentId") java.lang.String toAgentId)

	// Get sum of all Knows link weights
	@GremlinGroovy(value="it.outE('knows').weight.sum()", frame=false)
	public Double getKnowsLinksWeightSum()
	
	// Buffered challenges
	@Incidence(label = 'buffered', direction = Direction.OUT)
	public Iterable<FramedChallenge> getBufferedChallenges()

	/** return buffer (isBuffered challenges but not more than size of buffer)
	 *  does not work because GremlinGroovyAnnotationHandler has no handler for Edges
	 *  **/
	@GremlinGroovy(value = "it.outE('buffered').has('archived',false).toList()", frame=false)
	public List<Edge> getBuffer();
	
	// get open challenges (not marked as archived = true) 
	@GremlinGroovy(value = "it.inE('challenged').has('archived',false).toList()", frame=false)
	public List<Edge> getOpenChallengesAsEdges();
	
	/** 
	 * Returns current state of all Situations witch challenge the agent
	 * which are all situations but the ones which are sinked
	 **/
	@GremlinGroovy("it.inE('challenged').hasNot('forgot', T.eq, true).outV.in.outE('instanciated').last()")
	public Iterable<FramedSituation> getChallangingSituationsCurrentState()

	// Forgotten challenges  	
	@Incidence(label = 'forgot', direction = Direction.OUT)
        public Iterable<Edge> getSinkedLinks()
	
	@Incidence(label = 'forgot', direction = Direction.OUT)
	public FramedChallenge addSinkedChallenge(final FramedSituation situation)

	// get sinked situations in their original state
	@TimedAdjacency(label = 'forgot', direction = Direction.OUT)
	public Iterable<FramedSituationInstance> getSinkedSituationsOriginalState()

	// Interpreted challenges
	@Incidence(label = 'interpreted', direction = Direction.OUT)
	public FramedChallenge addInterpretedChallenge(final FramedSituation situation)
	
	// gets interpreted challenges that are not archived
	@GremlinGroovy(value = "it.outE('interpreted').filter{it.archived == false}.toList()", frame=false)
	public List<Edge> getInterpretedChallengesAsEdges()

	// gets interpreted challenges that are not archived
	@GremlinGroovy(value = "it.outE('interpreted').toList()", frame=false)
	public List<Edge> getAllInterpretedChallengesAsEdges()
	
	/*
	 * Gets all interpreted challenges which is not good as it also gets archived ones
	@Incidence(label = 'interpreted', direction = Direction.OUT)
	public Iterable<FramedChallenge> getInterpretedChallenges()
	*/
	
	// Processed challenges
	@Incidence(label = 'processed', direction = Direction.OUT)
	public FramedChallenge addProcessedChallenge(final FramedSituation situation)
	
	// Reinterpreted challenges
	@Incidence(label = 'reinterpreted', direction = Direction.OUT)
	public FramedChallenge addReinterpretedChallenge(final FramedSituation situation)

	@Incidence(label = 'reinterpreted', direction = Direction.OUT)
	public Iterable<FramedChallenge> getReinterpretedLinks()
	
	// Buffered challenges
	@Incidence(label = 'buffered', direction = Direction.OUT)
	public FramedChallenge addBufferedChallenge(final FramedSituation situation)
	
	// get a vertex holding all benefits
	@TimedAdjacency(label = 'benefitted', direction = Direction.OUT)
	public Iterable<DataHolder> getAllBenefits()

	// add a vertex holding benefit
	@TimedAdjacency(label = 'benefitted', direction = Direction.OUT)
	public DataHolder addBenefit()

	// get a vertex holding all benefits
	@TimedAdjacency(label = 'penalized', direction = Direction.OUT)
	public Iterable<DataHolder> getAllPenalties()
	
	// get a vertex holding all benefits
	@TimedAdjacency(label = 'penalized', direction = Direction.OUT)
	public DataHolder addPenalty()
	
}
