package org.globalbraininstitute.challprop.frames

import com.tinkerpop.frames.Domain
import com.tinkerpop.frames.EdgeFrame
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.Range

public interface Knows extends EdgeFrame {
	
	@Property("weight")
	public void setWeight(Double)
	@Property("weight")
	public Double getWeight()

	@Property("weightHistory")
	public void setWeightHistory(Map)
	@Property("weightHistory")
	public Map getWeightHistory()

	@Property("preferenceVector")
	public void setPreferenceVector(String)
	@Property("preferenceVector")
	public String getPreferenceVector()
	
	@Property("numberOfUpdates")
	public void setNumberOfUpdates(Integer)
	@Property("numberOfUpdates")
	public Integer getNumberOfUpdates()
	
	@Domain
	public FramedAgent getDomain()

	@Range
	public FramedAgent getRange()

}
