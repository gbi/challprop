package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

class FrameMethods {
	static Network network = Globals.network
	static FramedGod god = network.god
	private static Logger logger = LoggerFactory.getLogger(FrameMethods.class);
	

	public static RealVector getChallengeVectorUpdate(FramedChallenge challenge) {
		def situationVector = Utils.deserializeVector(challenge.asEdge().bothV.has('type', 'situation').next().getProperty('vector'))
		RealVector needVector = Utils.deserializeVector(challenge.asEdge().bothV.has('type','agent').next().getProperty('needVector'))
		RealVector interpretation = situationVector.subtract(needVector);
		return interpretation
	}
	
	public static Double getIntensityUpdate(RealVector interpretedVector) {
		return interpretedVector.getL1Norm()
	}
	
	public static Double getIntensityUpdate(FramedChallenge challenge) {
		return getChallengeVectorUpdate(challenge).getL1Norm()
	}
	
	public static boolean isConcurrentlyModified(FramedChallenge challenge) {
		def situationProxy = challenge.getRange().getType() == "situation" ? challenge.getRange() : challenge.getDomain()
		FramedSituation situation = this.network.g.frame(situationProxy.asVertex(), FramedSituation.class)
		
		return situation.getLastUpdateTimestamp() > challenge.getTimestamp()
	}

	/**
	 * @author: vveitas
	 *
	 * Takes a buffer of an agent and sorts it according to the formula taking into account intensity, capability and trust;
	 */

	public static Map getSortedBuffer(FramedAgent agent) {
		assertNotNull(agent)
		assertEquals(agent.asVertex().getProperty('type'), 'agent')
		List<Edge> bufferEdges = agent.getBuffer()
		assertNotNull(bufferEdges)
		assert bufferEdges.size() !=  null
		def sortedBuffer = [:]

		logger.debug("sorting a buffer of an agent {} with {} challenges", agent.toString(), bufferEdges.size())

		for (Edge challEdge : bufferEdges) {
			FramedChallenge chall = network.g.frame(challEdge, Direction.OUT, FramedChallenge.class)
			def intensity = chall.getIntensity()
			logger.debug("got challenge {}", chall.toString())

			// challenge and skill vectors can be zero vecors in which case cosine cannot be calculated: increment each entry by small amount
			RealVector skillVector = Utils.deserializeVector(agent.getSkillVector())
			skillVector = skillVector.getL1Norm() ? skillVector : skillVector.mapAdd(0.1d)
			RealVector challengeVector = Utils.deserializeVector(chall.getVector())
			challengeVector = challengeVector.getL1Norm() ? challengeVector : challengeVector.mapAdd(0.1d)
			
			def capability = challengeVector.cosine(skillVector)
			logger.debug("calculated capability {}", capability)
			// prioritizing challenges with *outgoing* link strength (logical)
			logger.debug("calculating trust:")
			logger.debug("source of challenge: {} with type {}", chall.getSource().toString(),network.g.getVertex(chall.getSource()).getProperty('type'))

			def toAgentId = chall.getSource()
			def trust = agent.asVertex().outE('knows').inV.filter{it.identity == toAgentId}.back(2).next().weight

			//def trust = agent.getTrustToAgent(chall.getSource())?: 1.0E-5
			// prioritizing challenges with *incomming* link strength (less logical but what is specified in the paper)
			//def trust = agent.getTrustOfAgent(chall.getSource())?: 1.0E-5
			assertNotNull(trust)
			logger.debug("calculated trust {}",trust)
			def bexp = intensity * capability * trust
			logger.debug('calculated bexp {} = intensity {} * capability {} * trust {}', bexp, intensity,capability,trust)
			
			sortedBuffer.put(bexp, chall)
		}
		logger.debug('sorted buffer size: {}', sortedBuffer.size())
		return sortedBuffer
	}

}
