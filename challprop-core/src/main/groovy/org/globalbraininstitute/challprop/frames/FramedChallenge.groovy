package org.globalbraininstitute.challprop.frames

import com.tinkerpop.frames.Domain
import com.tinkerpop.frames.EdgeFrame
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.Range

public interface FramedChallenge extends EdgeFrame {
	
	@Property("identity")
	public String getIdentity()
	@Property("identity")
	public void setIdentity(String)
	
	@Property("type")
	public String getType()
	@Property("type")
	public void setType(String)
	
	@Property("source")
	public void setSource(String)
	@Property("source")
	public String getSource()
	
	@Property("vector")
	public void setVector(String)
	@Property("vector")
	public String getVector()
	
	@Property("intensity")
	public void setIntensity(Double)
	@Property("intensity")
	public Double getIntensity()
	
	@Property("archived")
	public void setArchived(Boolean)
	@Property("archived")
	public Boolean getArchived(Boolean)
	
	@Property("timestamp")
	public Long getTimestamp()
	
	@Domain
	public FramedSituation getDomain()

	@Range
	public FramedAgent getRange()
	
	
}
