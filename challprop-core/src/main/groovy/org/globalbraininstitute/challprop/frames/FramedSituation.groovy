package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.frames.EdgeFrame
import com.tinkerpop.frames.Incidence
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.VertexFrame
import com.tinkerpop.frames.annotations.gremlin.GremlinGroovy
import org.globalbraininstitute.challprop.frames.annotations.TimedAdjacency

public interface FramedSituation extends VertexFrame {
	
	// properties of a situation
	@Property("identity")
	public String getIdentity()
	@Property("identity")
	public void setIdentity(String)
	
	@Property("type")
	public void setType(String)
	@Property("type")
	public String getType()

	@Property("rivalComponents")
	public void setRivalComponents(List)
	@Property("rivalComponents")
	public List getRivalComponents()
	
	@Property("vector")
	public void setVector(String)
	@Property("vector")
	public String getVector()
	
	// links to the instances of the situation
	// instances are states of the situation at a certain moment in time
	// needed for time machine
	@Incidence(label = 'logged', direction = Direction.OUT)
	public Iterable<EdgeFrame> getLinksToInstances() 
	
	@TimedAdjacency(label = "logged", direction = Direction.OUT)
	public FramedSituationInstance addNewInstance()
	
	@TimedAdjacency(label = 'logged', direction = Direction.OUT)
	public Iterable<FramedSituationInstance> getInstances()
	
	// links to challenges
	// which are actually events
	@Incidence(label = 'challenged', direction = Direction.OUT)
	public Iterable<FramedChallenge>  getChallengedLinks()
	
	@Incidence(label = 'challenged', direction = Direction.OUT)
	public FramedChallenge addChallengeToAgent(final FramedAgent agent)
	
	@TimedAdjacency(label = 'challenged', direction = Direction.OUT)
	public Iterable<FramedAgent> getChallengedAgents()
	
	@Incidence(label = 'interpreted', direction = Direction.IN)
	public Iterable<FramedChallenge> getInterpretedLinks()
	
	@TimedAdjacency(label = 'interpreted', direction = Direction.IN)
	public Iterable<FramedAgent> getAgentsWhichInterpreted()
	
	@Incidence(label = 'reinterpreted', direction = Direction.IN)
	public Iterable<EdgeFrame> getReinterpretedLinks()
	
	@TimedAdjacency(label = 'reinterpreted', direction = Direction.IN)
	public Iterable<FramedAgent> getAgentsWhichReinterpreted()
	
	@Incidence(label = 'forgot', direction = Direction.IN)
	public Iterable<EdgeFrame> getForgotLinks()
	
	@TimedAdjacency(label = 'forgot', direction = Direction.IN)
	public Iterable<FramedAgent> getAgentsWhichForgot()

	@Incidence(label = 'buffered', direction = Direction.IN)
	public Iterable<EdgeFrame> getBufferedLinks()
	
	@TimedAdjacency(label = 'buffered', direction = Direction.IN)
	public Iterable<FramedAgent> getAgentsWhichBuffered()
	
	@GremlinGroovy(value="it.outE('challenged').order{it.a.timeline <=> it.b.timeline}.toList()", frame=false)
	public List getTimeOrderedChallenges()
	
	@GremlinGroovy(value="it.outE('logged').last().timestamp", frame=false)
	public Long getLastUpdateTimestamp()
}
