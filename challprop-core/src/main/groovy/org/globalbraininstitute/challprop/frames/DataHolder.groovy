package org.globalbraininstitute.challprop.frames

import com.tinkerpop.frames.Property
import com.tinkerpop.frames.VertexFrame

public interface DataHolder extends VertexFrame {
	
	@Property("type")
	public void setType(String)
	@Property("type")
	public String getType()

	@Property("value")
	public void setValue(Double)
	@Property("value")
	public Double getValue()
	
}
