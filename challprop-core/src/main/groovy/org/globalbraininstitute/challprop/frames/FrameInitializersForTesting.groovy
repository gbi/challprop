package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Element
import com.tinkerpop.frames.EdgeFrame
import com.tinkerpop.frames.FrameInitializer
import com.tinkerpop.frames.FramedGraph
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.math3.linear.ArrayRealVector
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.json.Json
import javax.json.JsonObject

import static org.junit.Assert.assertNotNull

public class FrameInitializersForTesting {
	private static Logger logger = LoggerFactory.getLogger(FrameInitializersForTesting.class);
	
	public static FrameInitializer framedGod = new FrameInitializer() {

		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
		assertNotNull(framedGraph);
		
		if (kind == FramedGod.class) {
			element.setProperty("type", "god");
			element.setProperty("graphDatabase", Parameters.parameters.graphDatabase);
			Parameters.parameters.each {
				element.setProperty(it.key, it.value)
				}
			}
		}
	};

	public static FrameInitializer framedAgent = new FrameInitializer() {
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			def start = System.nanoTime()
			assertNotNull(framedGraph);
			if (kind == FramedAgent.class) {
				element.setProperty("type", "agent");
				/*
				element.setProperty("needVector", Utils.serializeVector(Matlab.newPWVector(
					Parameters.parameters.dimensions, 
					Parameters.parameters.exponent,
					Parameters.parameters.cst,
					Parameters.parameters.densityVector,
					Parameters.parameters.percneg)));
			
				element.setProperty("skillVector", Utils.serializeVector(Matlab.newPWVector(
					Parameters.parameters.dimensions, 
					Parameters.parameters.exponent,
					Parameters.parameters.cst,
					Parameters.parameters.densityVector,
					Parameters.parameters.percneg)));
			
				element.setProperty("processingMatrix", 
					Utils.serializeMatrix(Matlab.processMatrixSparseGeneration(
						Parameters.parameters.dimensions, 
						Parameters.parameters.densityMatrix, 
						Parameters.parameters.rfactor, 
						1)));
				*/
				element.setProperty("bufferSize", Parameters.parameters.bufferSize);
				element.setProperty("learningRate", Parameters.parameters.learningRate as Double);
				element.setProperty("randomJump", Parameters.parameters.randomJump as Double)
				element.setProperty("benefit", 0d);

				if (Parameters.parameters.gephiVisualization == true) {
					postNewAgent(element)
				}
				logger.warn("=FramedAgentInitialized= {}={}", element.toString(), System.nanoTime()- start)
			}
		}
	};

	public static FrameInitializer framedSituation = new FrameInitializer() {
	
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			def start = System.nanoTime()
			assertNotNull(framedGraph);
			if (kind == FramedSituation.class) {
				element.setProperty("identity", RandomStringUtils.randomAlphanumeric(32))
				element.setProperty("type", "situation");
				element.setProperty("rivalComponents", Parameters.parameters.rivalComponents);
				/*
				element.setProperty("vector", Utils.serializeVector(Matlab.newPWVector(
					Parameters.parameters.dimensions,
					Parameters.parameters.exponent,
					Parameters.parameters.cst,
					Parameters.parameters.densityVector,
					Parameters.parameters.percneg)));
				*/
					logger.warn("=FramedSituationInitialized= {}={}", element.toString(), System.nanoTime()- start)
			}
		}
	};

	public static FrameInitializer framedSituationInstance = new FrameInitializer() {
	
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			assertNotNull(framedGraph);
			if (kind == FramedSituationInstance.class) {
				element.setProperty("type", "situationInst");
			}
		}
	};

	public static FrameInitializer framedChallenge = new FrameInitializer() {
	
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			def start = System.nanoTime()
			assertNotNull(framedGraph);
			if (kind == FramedChallenge.class) {
				element.setProperty("timestamp", System.nanoTime())
				//element.setProperty("identity", RandomStringUtils.randomAlphanumeric(32))
				// identity should be taken from the situation
				element.setProperty("type", "challenge")
				element.setProperty("archived", false)
				
				logger.warn("=FramedChallengeInitialized= {}={}", element.toString(), System.nanoTime()- start)
			}
		}
	};

	public static FrameInitializer knowsLink = new FrameInitializer() {
	
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			def start = System.nanoTime()
			assertNotNull(framedGraph);
			if (kind == Knows.class) {
				element.setProperty("timestamp", System.nanoTime())
				element.setProperty("weight", 0.00001d)
				element.setProperty("preferenceVector",Utils.serializeVector(new ArrayRealVector(Parameters.parameters.dimensions, 0.0d)))
				element.setProperty("numberOfUpdates", 1)
				if (Parameters.parameters.gephiVisualization == true && element.getVertex(Direction.OUT).getProperty('type') != "god") {
					postNewKnowsLink(element)
				}
				logger.warn("=FramedKnowsLinkInitialized= {}={}", element.toString(), System.nanoTime()- start)
			}
		}
	};


	public static FrameInitializer framedEdge = new FrameInitializer() {
	
		@Override
		public void initElement(Class<?> kind, FramedGraph framedGraph, Element element) {
			assertNotNull(framedGraph);
			if (kind == EdgeFrame.class) {
				element.setProperty("timestamp", System.nanoTime())
			}
		}
	};

	public static Integer postToGephi(String out) {
		Integer result = null;
		try {
			HttpURLConnection httpcon = (HttpURLConnection) ((new URL("http://localhost:8080/workspace0?operation=updateGraph").openConnection()));
			httpcon.setDoOutput(true);
			httpcon.setRequestProperty("Content-Type", "application/json");
			httpcon.setRequestProperty("Accept", "application/json");
			httpcon.setRequestMethod("POST");
			httpcon.connect();

			OutputStreamWriter osw = new OutputStreamWriter(httpcon.getOutputStream());

			osw.write(out);
			osw.flush();
			osw.close();
  
			result =  httpcon.getResponseCode();
  
		} catch (MalformedURLException e) {
		  	e.printStackTrace();
		} catch (IOException e) {
		  	e.printStackTrace();
		}
  
		return result;
	}
	
	public static Integer postNewAgent(Element element) {
		JsonObject JsonObject = Json.createObjectBuilder()
					.add("an", Json.createObjectBuilder()
							.add(element.getId().toString(), Json.createObjectBuilder()
									.add("type", "agent")
									.add("benefit", element.getProperty("benefit"))
									.add("needVector", element.getProperty("needVector"))
								)
						)
				.build();
	  return postToGephi(JsonObject.toString());
	}

	public static Integer postNewKnowsLink(Edge element) {
		JsonObject JsonObject = Json.createObjectBuilder()
				  .add("ae", Json.createObjectBuilder()
						  .add(element.getId().toString(), Json.createObjectBuilder()
								  .add("label", "knows")
								  .add("source", element.getVertex(Direction.OUT).getProperty('identity').toString())
								  .add("target", element.getVertex(Direction.IN).getProperty('identity').toString())
								  .add("directed", true)
								  .add("weight", element.getProperty("weight"))
							  )
					  )
			  .build();
		return postToGephi(JsonObject.toString());
	}
}
