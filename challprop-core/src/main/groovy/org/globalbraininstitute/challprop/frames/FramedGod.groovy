package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.frames.Incidence
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.VertexFrame
import com.tinkerpop.frames.annotations.gremlin.GremlinGroovy
import com.tinkerpop.frames.annotations.gremlin.GremlinParam
import org.globalbraininstitute.challprop.frames.annotations.TimedAdjacency

public interface FramedGod extends VertexFrame {
	
	// currently the only property of the generator of diversity is the type
	// In the future putting all the properties of the simulation in this object would make sense

	@Property("identity")
	public void setIdentity(String)
	@Property("identity")
	public String getIdentity()
	
	@Property("type")
	public void setType(String)
	@Property("type")
	public String getType()
	
	@TimedAdjacency(label = "created", direction = Direction.OUT)
	public FramedSituation addNewSituation()

	@TimedAdjacency(label = "created", direction = Direction.OUT)
	public Iterable<FramedSituation> getSituations()
	
	// Create agent knows god Link
	@Incidence(label = 'knows', direction = Direction.OUT)
	public Knows addKnowsAgent(final FramedAgent agent)

	// Create g.o.d. knows agent link
	// not that it creates Direction.IN link - which means that an agent 'knows' god, not vice versa (unless reciprocal edges are enabled).
	@Incidence(label = 'knows', direction = Direction.IN)
	public Knows addAgentKnows(final FramedAgent agent)

	@TimedAdjacency(label = "knows", direction = Direction.OUT)
	public Iterable<FramedAgent> getKnowsAgent()
	
	/** Get outgoing link to an agent**/
	@GremlinGroovy(value="it.outE('knows').inV.filter{it.id == toAgentId}.back(2).next()", frame=false)
	public Edge getLinkToAgent(@GremlinParam("toAgentId") java.lang.Long toAgentId)

	/** Get incoming link from an agent**/
	@GremlinGroovy(value="it.inE('knows').outV.filter{it.id == fromAgentId}.back(2).next()", frame=false)
	public Edge getLinkFromAgent(@GremlinParam("fromAgentId") java.lang.Long fromAgentId)

}
