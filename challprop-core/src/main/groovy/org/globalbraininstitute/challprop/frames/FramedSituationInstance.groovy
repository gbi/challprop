package org.globalbraininstitute.challprop.frames

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.frames.Property
import com.tinkerpop.frames.VertexFrame
import com.tinkerpop.frames.annotations.gremlin.GremlinGroovy
import org.globalbraininstitute.challprop.frames.annotations.TimedAdjacency

public interface FramedSituationInstance extends VertexFrame {
	
	@Property("type")
	public void setType(String)
	@Property("type")
	public String getType()

	@Property("vector")
	public void setVector(String)
	@Property("vector")
	public String getVector()
	
	@TimedAdjacency(label = 'logged', direction = Direction.IN)
	public Iterable<FramedSituation> getSituations()
	
	@GremlinGroovy("it.setProperty('vector', it.inE('logged').outV('type','situation').getProperty('vector'))")
	public void setVector()
	
	
}
