package org.globalbraininstitute.challprop.global

import com.tinkerpop.frames.FramedGraph
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.filter.FilterFunctionPipe
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.functions.*
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class Simulation {

	Network network = Globals.network
	FramedGraph framedGraph = network.g
	static private Logger logger = LoggerFactory.getLogger(Simulation.class)

	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  Globals.network.gracefulShutdown()
			}
		  });
	}
		
	public static void main(String[] args) {
		Simulation simulation = new Simulation() 
		simulation.run(args)
	}
	
	public run(args) {
		network.populate()
		
		def numberOfCycles = network.god.asVertex().getProperty('numberOfCycles')
		
		network.god.asVertex().setProperty("simulationTimeStart", new Date().getTime())

		// God's part of the job
		// insert few situations to the network
		ArrayList newSituations = new ArrayList()
		for (int x=0;x<network.god.asVertex().getProperty('newSituationsPerCycle');x++) {
			FramedSituation situation = network.god.addNewSituation()
			situation.addNewInstance().setVector(situation.getVector())
			newSituations.add(situation)
			logger.debug("Added a situation {}", situation.asVertex())
		}
		// challenge some agents with these situations
		Pipe generationPipe = new TransformFunctionPipe(new ChallengeGeneration())
		Pipeline godsPipeline = new Pipeline(generationPipe)
		godsPipeline.setStarts(newSituations)
		while (godsPipeline.hasNext()) {
			godsPipeline.next()
			logger.debug("Pulled an agent from {}", godsPipeline.toString())
		}
		network.g.baseGraph.commit()

		for (int c=0;c<numberOfCycles;c++) {
			
			// Loop all agents in the network and process challenges
			
			List<FramedAgent> agents = network.god.getKnowsAgent().toList()
			
			Pipe agentsWithChallengesPipe = new FilterFunctionPipe(new AgentsWithChallenges("challenged"))
			Pipe preprocessingPipe = new SideEffectFunctionPipe(new Preprocessing())
			Pipe bufferingPipe = new SideEffectFunctionPipe(new Buffering())
			Pipe selectionPipe = new TransformFunctionPipe(new SelectionAgentBased())
			Pipe processingPipe = new TransformFunctionPipe(new ProcessingByMultiplication())
			Pipe reinterpretationPipe = new TransformFunctionPipe(new Reinterpretation("subtraction"))
			Pipe preferenceUpdatePipe = new SideEffectFunctionPipe(new PreferenceUpdate())
			Pipe situationUpdatePipe = new SideEffectFunctionPipe(new SituationUpdate())
			Pipe propagationPipe = new SideEffectFunctionPipe(new Propagation())
			
			Pipeline pipeline = new Pipeline(agentsWithChallengesPipe,
											preprocessingPipe, 
											bufferingPipe, 
											selectionPipe, 
											processingPipe, 
											reinterpretationPipe, 
											preferenceUpdatePipe,
											situationUpdatePipe,
											propagationPipe)
			
			pipeline.setStarts(agents)
			
			while(pipeline.hasNext()) {
				FramedChallenge challenge = pipeline.next()
				assert (challenge.asEdge().label == "reinterpreted")
				logger.debug("Pulled a challenge from {}", pipeline.toString())
			}
		}
		network.god.asVertex().setProperty("simulationTimeFinish", new Date().getTime())
		network.g.baseGraph.commit()
		
		Utils.graphMLSnapshot(network.g.baseGraph)
		
		network.g.baseGraph.commit()
	}
	
}
