package org.globalbraininstitute.challprop.utils

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.util.structures.Table
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network

public class Analysis {
	static {
		Gremlin.load()
	}
	
	private static Network network = Globals.network
	
	public static Map situationsProcessedByEachAgent() {
		def map = [:]
		network.g.baseGraph.V('type','agent').each{map.put(it,it.outE('processed').inV().toList())}.iterate()
		return map
	}
	
	public static Map agentsWhichProcessedEachSituation() {
		def map = [:]
		network.g.baseGraph.V('type','situation').each{map.put(it,it.inE('processed').outV().toList())}.iterate()
		return map
	}
	
	public static Table maxBranchingFactor() {
		
	}

}
