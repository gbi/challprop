package org.globalbraininstitute.challprop.utils

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Element
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.util.io.graphml.GraphMLWriter
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.global.Parameters
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import sun.audio.AudioPlayer
import sun.audio.AudioStream

import javax.json.Json
import javax.json.JsonObject
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet

import static org.junit.Assert.assertNotNull


public class Utils {
	static Logger logger = LoggerFactory.getLogger(Utils.class)
	
	public static Integer sumArrayList(ArrayList<Integer> arrayList) {
		int sum = 0;
		for (int i=0; i < arrayList.size(); i++) {
			sum+=arrayList.get(i);}
		return sum;
	}
	
	/**
	 * Serializes a RealVector object into string of doubles.
	 * 
	 * @param RealVector
	 * @return String comma delimited list of serialized doubles (in the form -2.3456E-16)
	 * 
	 */
	public static String serializeVector(RealVector realVector) {
		assertNotNull(realVector)
		String serializedForm = new String()
		for (int i = 0; i<realVector.getDimension(); i++) {
			if (i > 0) {serializedForm = serializedForm + ","}
			String entry = realVector.getEntry(i).toString()
			serializedForm = serializedForm + entry 
		}
		assertNotNull(serializedForm)
		logger.trace("Serialized a RealVector object {} into string {}.", realVector.toString(), serializedForm)
		return serializedForm
	}
	
	/**
	 * Deserializes comma separated serialized double values into RealVector object
	 * 
	 * @param serializedVector comma delimited string of serialized doubles (in the form -2.3456E-16) 
	 * @return RealVector
	 *  
	 */
	public static RealVector deserializeVector(String serializedForm) {
		assertNotNull(serializedForm)
		ArrayRealVector realVector = new ArrayRealVector() 
		def serializedDoubles = serializedForm.split(",")
		for (int i=0; i<serializedDoubles.size(); i++) {
			double component = Double.parseDouble(serializedDoubles[i]) 
			realVector = realVector.append(component)
		}
		assertNotNull(realVector)
		logger.trace("Deserialized a String {} into RealVector object {}.", serializedForm, realVector.toString())
		return realVector 
	}

	/**
	 * Serializes a RealMatrix object to a string representation for storing as a com.tinkrepop.Blueprints.Vertex property
	 * 
	 * @param realMatrix
	 * @return string representation of a Matrix: columns are separated by ";" values in columns separated by ","
	 */
	public static String serializeMatrix(RealMatrix realMatrix) {
		String serializedMatrix = new String() 
		int rowNumber = realMatrix.rowDimension
		for (int i = 0; i < rowNumber; i++) {
			RealVector row = realMatrix.getRowVector(i)
			String serializedRow = Utils.serializeVector(row)
			if (i > 0) {serializedMatrix = serializedMatrix + ";"}
			serializedMatrix = serializedMatrix + serializedRow
		}
		assertNotNull(serializedMatrix)
		logger.trace("Serialized a RealMatrix object {} into string {}.", realMatrix.toString(), serializedMatrix)
		return serializedMatrix
	}
	
	public static RealMatrix deserializeMatrix(String serializedMatrix) {
		assertNotNull(serializedMatrix)
		String[] serializedRows = serializedMatrix.split(";")
		int rowDimension = serializedRows.length
		int columnDimension = serializedRows[0].split(",").length  
		Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(rowDimension, columnDimension)
		for (int i=0; i<serializedRows.length; i++) {
			String serializedRow = serializedRows[i]
			RealVector rowVector = Utils.deserializeVector(serializedRow)
			matrix.setRowVector(i, rowVector)
		}
		assertNotNull(matrix)
		logger.trace("Deserialized a String {} into RealMatrix object {}.", serializedMatrix, matrix.toString())
		return matrix
	}

	public static int iterableSize(Iterable iterable) {
		int size = 0;
//		logger.trace("calculating size of iterable is of class {}", iterable.class.name)
		iterable.each{
			size += 1}
		return size
	}
	
	public static Object getFirstElement(Iterable iterable) {
		Object element
		for (Object el : iterable) {
			element = el
			break
		}
		return element
	}
	
	public static wipeFromDisk(String path) {
		assertNotNull(path)
		File dir = new File(path)
		dir.deleteDir()
		logger.trace("Wiped out the {},", path)
	}
	
	public static graphMLSnapshot(Graph graph) {
		Date now = new Date()
		now.format('yyyy-MM-dd:hh:mm:ss')
		def god = graph.V("type","god").next()
		String filePath1 = god.pathOnDisk + god.graphID
		new File(filePath1).mkdir()
		
		String filePath2 = filePath1 + "/snapshots/"
		new File(filePath2).mkdir()
		File file = new File(filePath2 + now.format('yyyy-MM-dd:hh:mm:ss') + ".graphml")
		file.createNewFile()
		OutputStream out = new FileOutputStream(file)
		GraphMLWriter.outputGraph(graph, out)
		out.close()
	}
	
	/**
	 * http://stackoverflow.com/questions/4893684/java-how-to-round-up-float-or-bigdecimal-value-by-0-5
	 * @param d
	 * @param decimalPlace
	 * @return
	 */
	public static double roundDouble(double d, int decimalPlace){
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_DOWN);
		return bd.doubleValue();
	 }
	
	/**
	 * Calculates a value of matrix component for a ControlledPropagationTestSimulation2
	 */									
	public static Double matrixComponent(totalPropagations, dimensions, currentPropagation) {
		Double matrixComponent = 1/dimensions \
								* (totalPropagations - currentPropagation +1) \
								/ (totalPropagations - currentPropagation +2);
		return matrixComponent;
	}
	
	public static Integer postToGephi(String out) {
		Integer result = null;
		try {
			HttpURLConnection httpcon = (HttpURLConnection) ((new URL("http://localhost:8080/workspace0?operation=updateGraph").openConnection()));
			httpcon.setDoOutput(true);
			httpcon.setRequestProperty("Content-Type", "application/json");
			httpcon.setRequestProperty("Accept", "application/json");
			httpcon.setRequestMethod("POST");
			httpcon.connect();

			OutputStreamWriter osw = new OutputStreamWriter(httpcon.getOutputStream());

			osw.write(out);
			osw.flush();
			osw.close();
  
			result =  httpcon.getResponseCode();
  
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
  
		return result;
	}
	
	public static Integer postNewAgent(Element element) {
		JsonObject JsonObject = Json.createObjectBuilder()
					.add("an", Json.createObjectBuilder()
							.add(element.getId().toString(), Json.createObjectBuilder()
									.add("type", "agent")
									.add("benefit", element.getProperty("benefit"))
									.add("needVector", element.getProperty("needVector"))
								)
						)
				.build();
	  return postToGephi(JsonObject.toString());
	}

	public static Integer postNewKnowsLink(Edge element) {
		JsonObject JsonObject = Json.createObjectBuilder()
				  .add("ae", Json.createObjectBuilder()
						  .add(element.getId().toString(), Json.createObjectBuilder()
								  .add("label", "knows")
								  .add("source", element.getVertex(Direction.OUT).getProperty("identity"))
								  .add("target", element.getVertex(Direction.IN).getProperty("identity"))
								  .add("directed", true)
								  .add("weight", element.getProperty("weight"))
							  )
					  )
			  .build();
		return postToGephi(JsonObject.toString());
	}
	
	public static void initializeDatabaseTables() {
		try {
			// connect to database (need to create on local computer in advance)
			Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');

			// drop existing tables
			PreparedStatement preparedStatement = connect.prepareStatement("DROP TABLE IF EXISTS challprop.needVectors")
			preparedStatement.executeUpdate()
			preparedStatement = connect.prepareStatement("DROP TABLE IF EXISTS challprop.situationVectors")
			preparedStatement.executeUpdate()
			preparedStatement = connect.prepareStatement("DROP TABLE IF EXISTS challprop.matrixes")
			preparedStatement.executeUpdate()

			// crate new ones
			preparedStatement = connect.prepareStatement("CREATE TABLE challprop.needVectors ( vector VARCHAR(300) ) ENGINE=myisam")
			preparedStatement.executeUpdate()
			preparedStatement = connect.prepareStatement("CREATE TABLE challprop.situationVectors ( vector VARCHAR(300) ) ENGINE=myisam")
			preparedStatement.executeUpdate()
			preparedStatement = connect.prepareStatement("CREATE TABLE challprop.matrixes ( matrix VARCHAR(10000) ) ENGINE=myisam;")
			preparedStatement.executeUpdate()
			connect.close()
		} catch (Exception e) {
			System.out.println(e.message)
		}

	}
	
	public static void generateNeedVectorTable(Integer numberOfItems) {
		try {
			Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');
			PreparedStatement preparedStatement = null

			for (int x = 0; x < numberOfItems; x++) {
				preparedStatement = connect.prepareStatement("INSERT INTO challprop.needVectors VALUES ( ? ) ")
				preparedStatement.setString(1, Utils.serializeVector(Matlab.newPWVector(
					Parameters.parameters.dimensions, 
					Parameters.parameters.exponentNeed,
					Parameters.parameters.cstNeed,
					Parameters.parameters.densityVectorNeed,
					Parameters.parameters.percnegNeed)))
				preparedStatement.executeUpdate()
			}
			connect.close()
			println "generated vector with Matlab: " + numberOfItems
			
		} catch (Exception e) {
			System.out.println(e.message)
		}
	}

	public static void generateSituationVectorTable(Integer numberOfItems) {
		try {
			Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');
			PreparedStatement preparedStatement = null

			for (int x = 0; x < numberOfItems; x++) {
				preparedStatement = connect.prepareStatement("INSERT INTO challprop.situationVectors VALUES ( ? ) ")
				preparedStatement.setString(1, Utils.serializeVector(Matlab.newPWVector(
						Parameters.parameters.dimensions,
						Parameters.parameters.exponentSituation,
						Parameters.parameters.cstSituation,
						Parameters.parameters.densityVectorSituation,
						Parameters.parameters.percnegSituation)))
				preparedStatement.executeUpdate()
			}
			connect.close()
			println "generated vector with Matlab: " + numberOfItems

		} catch (Exception e) {
			System.out.println(e.message)
		}
	}

	public static void generateMatrixTable(Integer numberOfItems) {
		try {
			
			Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');
			PreparedStatement preparedStatement = null

			for (int x = 0; x < numberOfItems; x++) {
				preparedStatement = connect.prepareStatement("INSERT INTO challprop.matrixes VALUES ( ? ) ")
				preparedStatement.setString(1, Utils.serializeMatrix(Matlab.processMatrixSparseGeneration(
						Parameters.parameters.dimensions, 
						Parameters.parameters.densityMatrix, 
						Parameters.parameters.rfactor, 
						1)))
				preparedStatement.executeUpdate()
			}
			connect.close()
			println "generated matrixes with Matlab: " + numberOfItems
			
		} catch (Exception e) {
			System.out.println(e.message)
		}
	}


	public static String generateZeroVectorString() {
		String serializedForm = new String()
		for (int i = 0; i<Parameters.parameters.dimensions; i++) {
			if (i > 0) {serializedForm = serializedForm + ","}
			String entry ="0"
			serializedForm = serializedForm + entry
		}
		assertNotNull(serializedForm)
		logger.trace("Created an zero vector (probably for a skill vector")
		return serializedForm

	}

	public static String getVectorString(String type) {
		assertNotNull(type)
		String vector
		Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');
		if (Parameters.parameters.useDatabaseOfVectors == true) {
			int noVectorsInDatabase = 0
			if (Parameters.dummyParameters.noVectorsInDatabase == null) {
				PreparedStatement preparedStatement
				if (type == "situation") {
					preparedStatement = connect.prepareStatement("SELECT COUNT(*) FROM challprop.situationVectors")
				} else if (type == "need") {
					preparedStatement = connect.prepareStatement("SELECT COUNT(*) FROM challprop.needVectors")
				}

				ResultSet resultSet = preparedStatement.executeQuery()
				while (resultSet.next()) {
					noVectorsInDatabase = resultSet.getString(1).toInteger();
				}
				Parameters.dummyParameters.put("noVectorsInDatabase",noVectorsInDatabase)
			} else {
			noVectorsInDatabase = Parameters.dummyParameters.noVectorsInDatabase
			}
	
			def rand = new Random().nextInt(noVectorsInDatabase)
			// get vector from the database
			PreparedStatement preparedStatement
			if (type == "situation") {
				preparedStatement = connect.prepareStatement("SELECT * FROM challprop.situationVectors LIMIT ?,1")
			} else if (type == "need") {
				preparedStatement = connect.prepareStatement("SELECT * FROM challprop.needVectors LIMIT ?,1")
			}

			preparedStatement.setInt(1,rand)
			ResultSet resultSet = preparedStatement.executeQuery()
			while (resultSet.next()) {
			vector = resultSet.getString(1);
		}
		logger.debug("Retrieved vector {} from random row {}", vector, rand)
		connect.close()
		} else {
			// generates a new vector
			vector = Utils.serializeVector(Matlab.newPWVector(
					Parameters.parameters.dimensions, 
					Parameters.parameters.exponent,
					Parameters.parameters.cst,
					Parameters.parameters.densityVector,
					Parameters.parameters.percneg))
			logger.debug("Ganerated a new vector {}", vector)
			}
		assertNotNull(vector)
		return vector
	}
	
	public static String getMatrixString() {
		String matrix
		Connection connect = DriverManager.getConnection('jdbc:mysql://localhost/challprop?user=challprop&password=tyu675gfjh');
		if (Parameters.parameters.useDatabaseOfMatrixes == true) {
			int noMatrixesInDatabase = 0
			if (Parameters.dummyParameters.noMatrixesInDatabase == null) {
				PreparedStatement preparedStatement = connect.prepareStatement("SELECT COUNT(*) FROM challprop.matrixes")
				ResultSet resultSet = preparedStatement.executeQuery()
				while (resultSet.next()) {
					noMatrixesInDatabase = resultSet.getString(1).toInteger();
				}
				Parameters.dummyParameters.put("noMatrixesInDatabase",noMatrixesInDatabase)
			} else {
			noMatrixesInDatabase = Parameters.dummyParameters.noMatrixesInDatabase
			}
	
			def rand = new Random().nextInt(noMatrixesInDatabase)
			// get matrix from the database
			PreparedStatement preparedStatement = connect.prepareStatement("SELECT * FROM challprop.matrixes LIMIT ?,1")
			preparedStatement.setInt(1,rand)
			ResultSet resultSet = preparedStatement.executeQuery()
			while (resultSet.next()) {
			matrix = resultSet.getString(1);
		}
		logger.debug("Retrieved matrix {} from random row {}", matrix, rand)
		connect.close()
		} else {
			// generates a new matrix
			matrix = Utils.serializeMatrix(Matlab.processMatrixSparseGeneration(
						Parameters.parameters.dimensions, 
						Parameters.parameters.densityMatrix, 
						Parameters.parameters.rfactor, 
						1))
			logger.debug("Ganerated a new matrix {}", matrix)
			
		}
		return matrix
	}

	public static void deleteDir(String simulationHome) {
		(new File(simulationHome)).deleteDir()
		
	}
	
	public static void playAudio(String filename) {
		
		InputStream instr = new FileInputStream(new File(filename));
		AudioStream audioStream = new AudioStream(instr);
		AudioPlayer.player.start(audioStream);
	}
	
	public static void initializeDataDirectoryOnDisk() {
		
		String pathOnDisk = Parameters.parameters.pathOnDisk
		Date d = new Date()
		String simulationID = d.getDateString().replaceAll("/","-") +"-"+d.getTimeString().replaceAll("/","-").replaceAll("\\s+","")
		String simulationHome = pathOnDisk + "data/" + simulationID + "/"
		String graphPath = simulationHome + "graph/"
		String experimentPath = simulationHome+ "experiment/"
		
		//put everything into parameters
		Parameters.parameters.put("simulationID", simulationID)
		Parameters.parameters.put("simulationHome", simulationHome)
		Parameters.parameters.put("graphPath", graphPath)
		Parameters.parameters.put("experimentPath", experimentPath)
		
		//create simulationHome directory on disk
		File f = new File(Parameters.parameters.simulationHome)
		f.mkdirs()
		f = new File(Parameters.parameters.graphPath)
		f.mkdirs()
		f = new File(Parameters.parameters.experimentPath+"/configs")
		f.mkdirs()
		f = new File(Parameters.parameters.experimentPath+"/log")
		f.mkdirs()
		f = new File(Parameters.parameters.experimentPath+"/analytics")
		f.mkdirs()
	}
}