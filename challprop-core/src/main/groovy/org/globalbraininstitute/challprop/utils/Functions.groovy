package org.globalbraininstitute.challprop.utils

import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.groovy.Gremlin

public class Functions {
	static {
		Gremlin.load()
	}
	
	public static Object nextStep(chall){
		def id=chall.identity;
		def tm = chall.timestamp;
		def nextStep = chall.bothV().has('type','situation').bothE().has('identity',id).has('timestamp',T.gte,tm).toList().sort{it.timestamp}[1]
		return nextStep
	}
	
	public static Object previousStep(chall){
		def id=chall.identity;
		def tm = chall.timestamp;
		def previousStep = chall.bothV().has('type','situation').bothE().has('identity',id).has('timestamp',T.lte,tm).toList().sort{it.timestamp}.reverse()[1]
		return previousStep
	}
	
	public static List previousAll(Object chall){
		def id = chall.identity
		def tm = chall.timestamp
		def prev = chall.bothV().bothE().has('identity',id).has('timestamp',T.lte,tm).toList().sort{it.timestamp}.reverse()
	}
	
	public static List furtherAll(Object chall){
		def id=chall.identity
		def tm = chall.timestamp
		List prop = chall.outV().bothE().has('identity',id).has('timestamp',T.gte,tm).toList()
		return prop
	}
	
	public static Object challenges(Object g){
		def m = [:]
		g.E.has('type','challenge').identity.groupCount(m).cap.iterate()
		return m
	}
	
	public static Object generatedChallenges(Object god){
		return god.asVertex().outE.has('label','created').inV.has('type','situation').outE.has('source',god.asVertex().getProperty("identity").toList())
	}
	
	public static boolean isConcurrentlyModified(Object challenge){
		return challenge.bothV().has('type','situation').outE('logged')[0].next().timestamp > challenge.timestamp
	}
	
	public static String getCurrentSituationVector(Object challenge) {
		return challenge.bothV().has('type','situation').outE('logged')[0].inV.next().vector
	}
	
	public static List getAllOriginalChallenges(Object g) {
		def list=[];
		def god = g.V('type','god').next()
		god.outE('created').inV.each{list.add(it.outE('challenged').has('source',god.getProperty("identity")).next())}
		return list
	}
	
		
}
