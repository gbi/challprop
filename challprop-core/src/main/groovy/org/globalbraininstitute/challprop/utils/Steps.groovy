package org.globalbraininstitute.challprop.utils

import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe

class Steps {
	def load() {
		defineNextPropagationStep()
	}
	static {
		Gremlin.load()
	}
	private def defineNextPropagationStep() {
		Gremlin.defineStep('nextPropagationStep', [Edge,Pipe],{String id, Long tm -> _().bothV.has('type','situation').bothE.has('type','challenge').has('identity',id).has('timestamp',T.gte,tm).order{it.a.timestamp <=> it.b.timestamp}[0]})
	}
	
	private def maxBranchingFactor() {
		Gremlin.defineStep("maxBranchingFactor", [Edge,Pipe], {_().transform{furtherAll(it).as('step').transform{step -> if (step.label == "challenged" && previousStep(step) != null && previousStep(step).label == "challenged") {count += 1} else {count = 0};count}.as('count').toList().max()}})
	}
}
