package org.globalbraininstitute.challprop.utils;

import matlabcontrol.MatlabProxy
import matlabcontrol.MatlabProxyFactory
import matlabcontrol.MatlabProxyFactoryOptions
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector

/**
 * A class for collecting algorithms from Evo's Matlab simulation
 * for generating power law, process matrixes and challenge vectors
 */

public class Matlab {

	/**
	Original MATLAB:
	function c= powerlaw(N,a,b)
	%N=number of iterations/data
	% frequency=power-law by bx^(-a)

	c=zeros(1,N);
	for i=1:N
		x = rand(1);
		sgn=sign(rand(1)-0.5);
		c(i) = sgn*((-a+1)*(x-1)/b)^(1/(-a+1));
	end
	%c=sort(c);
	%hist(c,100);

	 */
	public static ArrayList<Double> powerlawVector(Integer size, Double a, Double b) {
		ArrayList<Double> vector = []
		for (int i=0; i<size; i++) {
			def x = Math.random()
			def list = [-1,1]; Collections.shuffle(list); def sgn = list[0]
			vector.add(sgn*((-a+1)*(x-1)/b)**(1/(-a+1)))
		}
		return vector
	}

	/**
	 *
	 * @param dimensions - number of vector dimensions or compenents
	 * @param exp - exponent of powerlaw distribution
	 * @param cst - value under which components are zero in needvector;
	 * @param density - fraction of non-zero components
	 * @param percneg - number of negative entries in the vector
	 * @return
	 */

	public static RealVector newPWVector(int dimensions, double exp, double cst, double density, double percneg) {

		// MATLAB side matrix generations
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
			.setUsePreviouslyControlledSession(true)
      .setMatlabLocation('/usr/local/MATLAB/R2012a/bin/matlab')
			.setHidden(false)
			.build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		MatlabProxy proxy = factory.getProxy();
		proxy.eval("clear")
		proxy.eval("cd /media/data/challprop-working/chalprop_matlab")
		proxy.setVariable("dimensions", dimensions)
		proxy.setVariable("exp", exp)
		proxy.setVariable("cst", cst) // for some reason challengevector takes this as percentage points
		proxy.setVariable("density", density*100)
		proxy.setVariable("percneg", percneg)

		proxy.eval("V = challengevector(dimensions,exp,cst,density,percneg)");
		def result = proxy.getVariable("V");
		//Disconnect the proxy from MATLAB
		proxy.disconnect();

		// Java side
		ArrayRealVector vector = new ArrayRealVector(result)
	}

	public static RealMatrix processMatrixSparseGeneration(size, density, rfactor, norm=1) {

		// MATLAB side matrix generations
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
			.setUsePreviouslyControlledSession(true)
      .setMatlabLocation('/usr/local/MATLAB/R2012a/bin/matlab')
			.setHidden(false)
			.build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		MatlabProxy proxy = factory.getProxy();
		proxy.eval("clear")
		proxy.eval("cd /media/data/challprop-working/chalprop_matlab")
		proxy.setVariable("size", size)
		proxy.setVariable("density", density)
		proxy.setVariable("rfactor", rfactor)
		proxy.setVariable("norm", norm)
		proxy.eval("M = processmatrixsparse(size,density,rfactor,norm)")
		proxy.eval("F = full(M)")

		def result = proxy.getVariable("F");
		//Disconnect the proxy from MATLAB
		proxy.disconnect();

		// Java side
		Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(size, size)
		for (int c = 0; c<size;c++) {
			def startIndex = c*size
			def endIndex = startIndex + size -1
			double[] column = result[startIndex..endIndex]
			matrix.setColumn(c, column)
		}
		return matrix
	}

}
