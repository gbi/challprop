package org.globalbraininstitute.challprop.utils

import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.apache.commons.math3.random.GaussianRandomGenerator
import org.apache.commons.math3.random.JDKRandomGenerator
import org.apache.commons.math3.random.UncorrelatedRandomVectorGenerator
import org.globalbraininstitute.challprop.global.Parameters

/**
 * @author vveitas
 */
 
public class Algorithms {
	
	public static RealVector newPresetVector(double preset) {
		return new ArrayRealVector(Parameters.parameters.dimensions, preset)
	}
	
	public static RealMatrix newPresetMatrix(double preset) {
		def dimensions = Parameters.parameters.dimensions
		Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(dimensions, dimensions)
		for (int c = 0; c<dimensions;c++) {
			matrix.setColumnVector(c, newPresetVector(preset))
		}
		return matrix
	}

	public static RealVector newPWVector(double exp, double cst, double density, double percneg) {
		
		def vectorGenerator = new UncorrelatedRandomVectorGenerator(Parameters.parameters.dimensions,
			new GaussianRandomGenerator(new JDKRandomGenerator()))
			
		return new ArrayRealVector(vectorGenerator.nextVector())
	}
	
	public static RealMatrix processMatrixSparseGeneration(density, rfactor, norm=1) {
		def dimensions = Parameters.parameters.dimensions
		def vectorGenerator = new UncorrelatedRandomVectorGenerator(dimensions,
			new GaussianRandomGenerator(new JDKRandomGenerator()))

		Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(dimensions, dimensions)
		for (int c = 0; c<dimensions;c++) {
			matrix.setColumn(c, vectorGenerator.nextVector())
		}
		
		return matrix
	}
	
}
