package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.globalbraininstitute.challprop.visual.GephiStreamingServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 *Takes a challenge and processes it with the processing vector
 *This pipe is just for testing equivalent processing
 */

class ProcessingBySubtraction implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(ProcessingBySubtraction.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		assertNotNull(challenge)
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class) // after interpretation so agent is in the domain
		assertNotNull(agent)
		RealVector processingVector = Utils.deserializeVector(agent.getProcessingVector())
		assertNotNull(processingVector)
		RealVector vectorBefore = Utils.deserializeVector(challenge.getVector())
		assertNotNull(vectorBefore)
		
		//calculate benefit
		RealVector vectorAfter = vectorBefore.subtract(processingVector)
		assertNotNull(vectorAfter)
		def benefit = 0
		for (int i = 0; i <vectorBefore.getDimension(); i++) {
			benefit= benefit +  Math.abs(vectorBefore.getEntry(i)) - Math.abs(vectorAfter.getEntry(i))
		}
		logger.debug("Calculated benefit {} for the agent {}", benefit, agent.toString())
		//Modify agent
		// benefit of the agent is a simple sum of all extracted benefits
		agent.setBenefit(agent.getBenefit()+benefit)
		if (Parameters.parameters.gephiVisualization == true ) {
			GephiStreamingServer.postChangeAgent(agent.asVertex())
		}
		logger.debug("Updated benefit for agent {} is {}", agent.toString(), agent.getBenefit())
		// skill vector is a weighted average of all challenges, weighted by benefit extracted
		agent.setSkillVector(Utils.serializeVector(Utils.deserializeVector(agent.getSkillVector()).add(vectorBefore.mapMultiply(benefit))))
		logger.debug("Updated skill vector for agent {} is {}", agent.toString(), agent.getSkillVector())
		
		//Create new instance of a challenge - processed
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		FramedChallenge processedChallenge = agent.addProcessedChallenge(situation)
		processedChallenge.setIdentity(challenge.getIdentity())
		processedChallenge.setSource(challenge.getSource())

		//reverse the non-rival components of the vector
		def vectorAfterCorrection = vectorBefore
		if (!Parameters.parameters.ignoreNonRivalCorrection) {
			def rivalComponents = situation.getRivalComponents()
			rivalComponents.each { vectorAfterCorrection.setEntry(it, vectorAfter.getEntry(it)) }
		} else {
			vectorAfterCorrection = vectorAfter
		}

		processedChallenge.setVector(Utils.serializeVector(vectorAfterCorrection))
		processedChallenge.setType("challenge")
		challenge.setArchived(true)
		logger.debug("Processed a challenge {} and created a processedChallenge {} with identity {}", challenge.toString(), processedChallenge.toString(), challenge.getIdentity())


		// Reinforcing links
		Edge knowsEdge = agent.getLinkToAgent(challenge.getSource())
		assertNotNull(knowsEdge)
		Knows knows = network.g.frame(knowsEdge, Direction.OUT, Knows.class)

		Double learningRate = agent.getLearningRate()
		def newWeight = knows.getWeight() + learningRate * benefit
		logger.debug("Weight calculation: {} = old weight: {} '+', learningRate: {} * benefit {}", newWeight, knows.getWeight(), learningRate, benefit)
		knows.setWeight(newWeight)
		knows.weightHistory.put(System.nanoTime(),newWeight)
		if (Parameters.parameters.gephiVisualization == true ) {
			GephiStreamingServer.postChangeKnowsLink(knows.asEdge())
		}
		logger.debug("Changed 'knows' link weight: ={},{},{},{}=", knows.toString(), System.nanoTime(), "newWeight", newWeight)

		// Reinforcing reciprocal link because
		// on the other hand, I also want to reinforce the link through which the challenge arrived..
		Double reciprocityRate = Parameters.parameters.reciprocityRate
		if (reciprocityRate != 0 && reciprocityRate !=null) {
			Edge reciprocalKnowsEdge = agent.getLinkFromAgent(challenge.getSource())
			Knows reciprocalKnows = network.g.frame(reciprocalKnowsEdge, Direction.OUT, Knows.class)
			def newReciprocalWeight = reciprocalKnows.getWeight() + learningRate * benefit * reciprocityRate
			logger.debug("Reciprocal weight calculation: {} = old weight: {} '+', learningRate: {} * benefit {} * reciprocityRetE: {}", newReciprocalWeight, knows.getWeight(), learningRate, benefit, reciprocityRate)
			reciprocalKnows.setWeight(newReciprocalWeight)
			reciprocalKnows.weightHistory.put(System.nanoTime(),newReciprocalWeight)
			if (Parameters.parameters.gephiVisualization == true ) {
				GephiStreamingServer.postChangeKnowsLink(reciprocalKnows.asEdge())
			}
			logger.debug("Changed 'reciprocalKnows' link weight: ={},{},{},{}=", reciprocalKnows.toString(), System.nanoTime(), "newReciprocalWeight", newReciprocalWeight)

		}

		return processedChallenge
	}
}
