package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 
 * @author vveitas
 */

class SituationUpdate implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(SituationUpdate.class);
	private final String function;
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		assert challenge.asEdge().label == 'reinterpreted'
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		// this assertion whas taking a lot of time (LOT!) when running simulation with concurent challenges...
		//assert situation.asVertex().outE("logged").last().getProperty("timestamp") <= challenge.getTimestamp()
		situation.setVector(challenge.getVector())
		situation.addNewInstance().setVector(situation.getVector())
		challenge.setArchived(true)
		logger.debug("Updated situation {} with a new vector of challenge {} (id {}.)", situation.toString(), challenge.toString(), challenge.getIdentity())
		logger.warn("=SituationUpdatePipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
		
	}
}
