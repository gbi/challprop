package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.DataHolder
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.globalbraininstitute.challprop.visual.GephiStreamingServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 *
 * @author vveitas
 * This is a SideEffectFunctionPipe as it just calculates penalty and applies it
 * automatically to the challenged agent
 *
 */

class Penalty implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(Penalty.class);

	public FramedChallenge compute(FramedChallenge challenge) {
			FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class) // after interpretation so agent is in the domain
			assertNotNull(agent)

			// proceed only if the situatation was already preprocessed at least once (which means that the penalty was calculated)
			def situationProxy = challenge.getRange().getType() == "situation" ? challenge.getRange() : challenge.getDomain()
			FramedSituation situation = network.g.frame(situationProxy.asVertex(), FramedSituation.class)
			assertNotNull(situation)
			if (Parameters.parameters.penaltyCalculatedOnlyOnce && Utils.iterableSize(situation.getInterpretedLinks()) > 1) {
				logger.debug("Not penalizing agent {} with challenge {} - already used for penalizing once", agent.toString(), challenge.toString())

			} else {

				def benefit = agent.getBenefit()
				assertNotNull(benefit)
				def vector = challenge.getVector()
				def penalty = Utils.deserializeVector(challenge.getVector()).toArray().findAll{it < 0 }.sum() ?:0.0d

				DataHolder penaltyHolder = agent.addPenalty()
				penaltyHolder.setType('penaltyHolder')
				penaltyHolder.setValue(penalty)
				assert penalty != null
				assert penaltyHolder.getValue() == penalty
				agent.setBenefit(benefit + penalty)

				if (Parameters.parameters.gephiVisualization == true ) {
					GephiStreamingServer.postChangeAgent(agent.asVertex())
				}
				assertNotNull(penalty)

				logger.debug("Calculated a penalty {} for an agent {} for challenge {} with identity {}", penalty, agent.toString(), challenge.toString(), challenge.getIdentity())

				// automatically decreasing the link weight due to penalty
				List knowsEdgeS = agent.getLinkSToAgent(challenge.getSource())
				Knows knows;

				FramedAgent sourceAgent = network.g.frame(network.g.baseGraph.V('identity',challenge.getSource()).next(),FramedAgent.class)
				if (knowsEdgeS == []) {
					knows = agent.addKnowsAgent(sourceAgent)
				} else {
					knows = network.g.frame(knowsEdgeS[0], Direction.IN, Knows.class)
				}

				Double learningRate = agent.getLearningRate()
				def newWeight = knows.getWeight() + learningRate * penalty
				logger.debug("Weight calculation: {} = old weight: {} '+', learningRate: {} * penalty {}", newWeight, knows.getWeight(), learningRate, penalty)
				knows.setWeight(newWeight)
				knows.weightHistory.put(System.nanoTime(),newWeight)
				if (Parameters.parameters.gephiVisualization == true ) {
					GephiStreamingServer.postChangeKnowsLink(knows.asEdge())
				}
				logger.debug("Changed 'knows' link weight: ={},{},{},{}=", knows.toString(), System.nanoTime(), "newWeight", newWeight)

				Double reciprocityRate = Parameters.parameters.reciprocityRate
				if (reciprocityRate != 0 && reciprocityRate !=null) {
					Edge reciprocalKnowsEdge = agent.getLinkFromAgent(challenge.getSource())
					Knows reciprocalKnows = network.g.frame(reciprocalKnowsEdge, Direction.OUT, Knows.class)
					def newReciprocalWeight = reciprocalKnows.getWeight() + learningRate * penalty * reciprocityRate
					logger.debug("Reciprocal weight calculation: {} = old weight: {} '+', learningRate: {} * penalty {} * reciprocityRetE: {}", newReciprocalWeight, knows.getWeight(), learningRate, penalty, reciprocityRate)
					reciprocalKnows.setWeight(newReciprocalWeight)
					reciprocalKnows.weightHistory.put(System.nanoTime(),newReciprocalWeight)
					if (Parameters.parameters.gephiVisualization == true ) {
						GephiStreamingServer.postChangeKnowsLink(reciprocalKnows.asEdge())
					}
					logger.debug("Changed 'reciprocalKnows' link weight: ={},{},{},{}=", reciprocalKnows.toString(), System.nanoTime(), "newReciprocalWeight", newReciprocalWeight)

			}
		}

		return challenge
	}
}
