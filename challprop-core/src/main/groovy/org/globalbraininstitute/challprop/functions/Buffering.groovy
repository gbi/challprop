package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * Iterates all agents, retrieves challenges which are interpreted and puts them into buffer or forgets
 * depending on intensity 
 */
public class Buffering implements PipeFunction<FramedAgent, FramedAgent> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(Buffering.class);
	
	public FramedAgent compute(FramedAgent agent) {
		def start = System.nanoTime()
		assertNotNull(agent)
			
		List<Edge> interpretedChallenges = agent.getInterpretedChallengesAsEdges().toList()
		for (Edge interpretedChallengeAsEdge: interpretedChallenges){
			FramedChallenge interpretedChallenge = network.g.frame(interpretedChallengeAsEdge, Direction.OUT, FramedChallenge.class)
			FramedSituation situation = network.g.frame(interpretedChallenge.getRange().asVertex(), FramedSituation.class)
			FramedChallenge worstChallengeInBuffer = network.getLastInBuffer(agent) 
			if ( worstChallengeInBuffer == null) {
				FramedChallenge bufferedChallenge = agent.addBufferedChallenge(situation)
				bufferedChallenge.setIdentity(interpretedChallenge.getIdentity())
				bufferedChallenge.setSource(interpretedChallenge.getSource())
				bufferedChallenge.setVector(interpretedChallenge.getVector())
				bufferedChallenge.setIntensity(interpretedChallenge.getIntensity())
				bufferedChallenge.setType("challenge")
				logger.debug("Buffered challenge {} with identity {} because buffer is empty", bufferedChallenge.toString(),bufferedChallenge.getIdentity())
			} else if (interpretedChallenge.getIntensity() > worstChallengeInBuffer.getIntensity() ){
				FramedSituation worstChallengesSituation = network.g.frame(worstChallengeInBuffer.getRange().asVertex(), FramedSituation.class)
				FramedChallenge sinkedChallenge = agent.addSinkedChallenge(worstChallengesSituation)
				sinkedChallenge.setIdentity(worstChallengeInBuffer.getIdentity())
				sinkedChallenge.setSource(worstChallengeInBuffer.getSource())
				sinkedChallenge.setVector(worstChallengeInBuffer.getVector())
				sinkedChallenge.setIntensity(worstChallengeInBuffer.getIntensity())
				sinkedChallenge.setType("challenge")
				worstChallengeInBuffer.setArchived(true)
				logger.debug("Forgot challenge {} with identity {} because it was worst in buffer", sinkedChallenge.toString(),sinkedChallenge.getIdentity())
				
				
				FramedChallenge bufferedChallenge = agent.addBufferedChallenge(situation)
				bufferedChallenge.setIdentity(interpretedChallenge.getIdentity())
				bufferedChallenge.setSource(interpretedChallenge.getSource())
				bufferedChallenge.setVector(interpretedChallenge.getVector())
				bufferedChallenge.setIntensity(interpretedChallenge.getIntensity())
				bufferedChallenge.setType("challenge")
				
				logger.debug("Buffered challenge {} with identity {} because it is better that others", bufferedChallenge.toString(),bufferedChallenge.getIdentity())
				
				} else {
				logger.debug("Nothing is buffered for some or another reason")
			}
			interpretedChallenge.setArchived(true)
		}
		logger.warn("=BufferingPipe with agent= {}={}", agent.toString(), System.nanoTime()- start)
	return agent
	}
}
