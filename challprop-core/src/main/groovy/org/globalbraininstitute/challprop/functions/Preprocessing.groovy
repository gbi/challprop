package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.PipeFunction
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipe
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import com.tinkerpop.pipes.util.Pipeline
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals

/**
 * 
 * @author vveitas
 * 
 */

class Preprocessing implements PipeFunction<FramedAgent, FramedAgent> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(Preprocessing.class);
	private final String function;
	
	public FramedAgent compute(FramedAgent agent) {
		def start = System.nanoTime()
		Iterable<Edge> challengeEdges = agent.getOpenChallengesAsEdges()
		assertEquals(challengeEdges.toList(),challengeEdges.toList().unique())
		List<FramedChallenge> challenges = network.g.frameEdges(challengeEdges, Direction.IN, FramedChallenge.class).toList()
		logger.debug("Preprocessing challenges {}", challenges)
		
		Pipe interpretationPipe = new TransformFunctionPipe(new Interpretation('subtraction'))
		Pipe intensityPipe = new SideEffectFunctionPipe(new Intensity("absoluteSum"))
		Pipe penaltyPipe = new SideEffectFunctionPipe(new Penalty())
		Pipeline pipeline = new Pipeline(interpretationPipe, intensityPipe, penaltyPipe)
		
		pipeline.setStarts(challenges)
		
		while(pipeline.hasNext()) {
			FramedChallenge challenge = pipeline.next()
			assert (challenge.asEdge().label == "interpreted")
			assert (challenge.asEdge().intensity != null)
			logger.debug("Preprocessed challenge {} with identity {}", challenge.toString(), challenge.getIdentity())
		}
		logger.warn("=PreprocessingPipe with agent= {}={}", agent.toString(), System.nanoTime()- start)
		return agent
	}
}
