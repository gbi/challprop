package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Selects agents which have interpreted challenges
 * To be used with a filter pipe
 */

public class FilterOut implements PipeFunction<Object, Boolean> {

	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(FilterOut.class);
	private String label
	private final Object what
	
	public FilterOut(Object what) {
		this.what = what
	}
	
	public Boolean compute(Object object) {
		def start = System.nanoTime()
		return object != what
		logger.warn("=FilterOutPipe with agent= {}={}", agent.toString(), System.nanoTime()- start)
	}
}
