package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.Pipe
import com.tinkerpop.pipes.PipeFunction
import com.tinkerpop.pipes.transform.TransformFunctionPipe
import org.globalbraininstitute.challprop.frames.FrameMethods
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Functions
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 *  Selects the best challenge from the buffer according to citeria defined in config.conf
 */

class SelectionAgentBased implements PipeFunction<FramedAgent, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private static Logger logger = LoggerFactory.getLogger(SelectionAgentBased.class);
	private final String selectionType;
	
	public SelectionAgentBased(String selectionType) {
		// could be updateBest; updateAll; dropIfModified; noUpdate;
		this.selectionType = selectionType
	}
	
	public FramedChallenge compute(FramedAgent agent) {
		def start = System.nanoTime()
		assertNotNull(agent)
		def sortedBuffer = FrameMethods.getSortedBuffer(agent)
		logger.debug("Sorted challenges in the buffer: {} ", (new TreeMap(sortedBuffer)).toString())
		def candidateChallenge = (new TreeMap(sortedBuffer)).lastEntry()
		FramedChallenge bestChallenge = null
		if (candidateChallenge !=null) {
			logger.debug("Candidate best challenge: {} with identity {}", candidateChallenge, candidateChallenge.value.getIdentity())
			if (this.selectionType.equals("updateBest")) {
				def bool = Functions.isConcurrentlyModified(candidateChallenge.value.asEdge())
				while (bool) {
					logger.debug("Candidate best challenge was concurrently modified")
					Pipe challengeUpdatePipe = new TransformFunctionPipe(new ChallengeUpdate())
					challengeUpdatePipe.setStarts(Arrays.asList(candidateChallenge.value))
					FramedChallenge updatedChallenge = challengeUpdatePipe.next()
					sortedBuffer = FrameMethods.getSortedBuffer(agent)
					candidateChallenge = (new TreeMap(sortedBuffer)).lastEntry()
					bool = Functions.isConcurrentlyModified(candidateChallenge.value.asEdge())
				}
			}
			if (Math.abs(candidateChallenge.key) == 0.0) {
				logger.debug("Not returning - bexp is equal to zero")
			} else {
				bestChallenge = candidateChallenge.value
				logger.debug("Returning best challenge for processing {} with identity {}", candidateChallenge.value, candidateChallenge.value.getIdentity())
			}
		} else if (candidateChallenge == null) {
			logger.debug("No challenge in buffer - not selecting enything")
		} 
		logger.warn("=SelectionPipe with agent= {}={}", agent.toString(), System.nanoTime()- start)
		return bestChallenge
	}
}
