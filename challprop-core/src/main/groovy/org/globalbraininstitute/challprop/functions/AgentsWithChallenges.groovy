package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * Selects agents which have interpreted challenges
 * To be used with a filter pipe
 */

public class AgentsWithChallenges implements PipeFunction<FramedAgent, Boolean> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(AgentsWithChallenges.class);
	private String label
	
	public AgentsWithChallenges(String label) {
		this.label = label
	}
	
	public Boolean compute(FramedAgent agent) {
		assertNotNull(agent)
		Iterable<Edge> challengeEdges = agent.asVertex().query().labels(this.label).has("archived", false).edges()
		List<FramedChallenge> challenges = network.g.frameEdges(challengeEdges, Direction.OUT, FramedChallenge.class).toList()
		if (challenges.size() > 0 ) {
			logger.debug("Returned agent {} for processing it's {} challenges", agent.toString(), this.label)
		} else {
			logger.debug("Agent {} has no {} challenges - not returning", agent.toString(), this.label)
		}
		return challenges.size() > 0 
	}
}
