package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Functions
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * Updates the challenge vector with the new value if the situation has changed
 * 
 */

class ChallengeUpdate implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(ChallengeUpdate.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def agentProxy = challenge.getRange().getType() == "agent" ? challenge.getRange() : challenge.getDomain()
		FramedAgent agent = network.g.frame(agentProxy.asVertex(), FramedAgent.class)
		assertNotNull(agent)

		def situationProxy = challenge.getRange().getType() == "situation" ? challenge.getRange() : challenge.getDomain()
		FramedSituation situation = network.g.frame(situationProxy.asVertex(), FramedSituation.class)
		assertNotNull(situation)
		
		FramedChallenge updatedChallenge = agent.addBufferedChallenge(situation)
		updatedChallenge.setIdentity(challenge.getIdentity())
		updatedChallenge.setSource(challenge.getSource())
		def currentSituationVector = Functions.getCurrentSituationVector(challenge.asEdge())
		def updatedVector = Utils.deserializeVector(currentSituationVector).subtract(Utils.deserializeVector(agent.getNeedVector()))
		def updatedIntensity = updatedVector.getL1Norm()
		updatedChallenge.setVector(Utils.serializeVector(updatedVector))
		updatedChallenge.setIntensity(updatedIntensity)
		updatedChallenge.setType("challenge")
		
		updatedChallenge.asEdge().setProperty("updatedFromChallengeWithId", challenge.asEdge().getId().toString())
		// after committing the challenge Id is changed and and not found in the graph; should be solved in titan-0.4.0   
		
		challenge.setArchived(true)
		
		logger.debug("Updated a challenge {} with identity {} to challenge {}", challenge.toString(),challenge.getIdentity(), updatedChallenge.toString())
		
		return updatedChallenge
	}
}
