package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * Interprets the given situation vector comparing it to the given need vector and outputs a difference of both
 * as a challenge for the agent (owner of the need vector). Comparison is performed simply by subtracting
 * Need vector from Situation vector (the should be of same size).
 * 
 * Should be implemented as a TransformFunctionPipe
 */

class Interpretation implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(Interpretation.class);
	private final String function;
	
	public Interpretation(String function) {
		this.function = function;
	}
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def agentProxy = challenge.getRange().getType() == "agent" ? challenge.getRange() : challenge.getDomain()
		FramedAgent agent = network.g.frame(agentProxy.asVertex(), FramedAgent.class)
		assertNotNull(agent)

		def situationProxy = challenge.getRange().getType() == "situation" ? challenge.getRange() : challenge.getDomain()
		FramedSituation situation = network.g.frame(situationProxy.asVertex(), FramedSituation.class)
		assertNotNull(situation)

		FramedChallenge newChallenge
		if (function == "subtraction") {
			String need = agent.getNeedVector();
			assertNotNull(need)
			RealVector realNeed = Utils.deserializeVector(need);
			RealVector realChallengeVector = Utils.deserializeVector(situation.getVector());
			RealVector interpretation = realChallengeVector.subtract(realNeed);
			newChallenge = agent.addInterpretedChallenge(situation)
			newChallenge.setIdentity(challenge.getIdentity())
			newChallenge.setSource(challenge.getSource())
			newChallenge.setVector(Utils.serializeVector(interpretation))
			newChallenge.setType("challenge")
			challenge.setArchived(true)
		}
		logger.debug("Interpreted a challenge {} and created a newChallenge {} with identity {}", challenge.toString(), newChallenge.toString(), challenge.getIdentity())
		return newChallenge
	}
}
