package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull;

/**
 * 
 * @author vveitas
 * Takes a challenge, decides where to propagate it and 
 * outputs the same situation but linked to an agent.
 * 
 */

public class Propagation implements PipeFunction<FramedChallenge, FramedChallenge>{
	static {
		Gremlin.load()
	  }

	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(Propagation.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		// unimplemented
		// copy from selection Agent based -start
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class)
 		assertNotNull(agent)
		def knowsEdges= agent.getKnowsLinksFromAgents() // challenges are propagated via 'trust' edges from other agents
		List<Knows> knowsLinks = network.g.frameEdges(knowsEdges, Knows.class).toList()
		assertNotNull(knowsLinks)
		if (Parameters.parameters.respectKnowsLinksWeights == true) {
			knowsLinks.sort{a,b -> b.weight <=> a.weight}
			logger.debug("Sorted knowsLinks for the propagation respecting weight strenght")
		} else {
			Collections.shuffle(knowsLinks)
			logger.debug("Randomized knowLinks list for the propagation without respect to weight strenght)")
		}
		List<FramedAgent> listOfAgentsToPropagate = []

		def randomJumpParameter = agent.asVertex().getProperty("randomJump")
		def disableRandomJump = Parameters.parameters.disableRandomJump
		logger.debug("disable randomJump: {}", disableRandomJump)
		def probabilityOfRandomJump = 1 /( 1 + randomJumpParameter * (agent.getKnowsLinksWeightSum() == null ? 0 : agent.getKnowsLinksWeightSum()))
		logger.debug("randomJump calculation: probabilityOfRandomJump = 1 /( 1 + randomJumpParameter * (agent.getKnowsLinksWeightSum() == null ? 0 : agent.getKnowsLinksWeightSum())")
		logger.debug("randomJump calculation: probabilityOfRandomJump={}", probabilityOfRandomJump)
		logger.debug("randomJump calculation: randomJumpParameter={}", randomJumpParameter)
		logger.debug("randomJump calculation: agent.getKnowsLinksWeightSum()={}", agent.getKnowsLinksWeightSum())

		listOfAgentsToPropogateGeneration:
		/*
		* The list is calculated as follows: one random jump (if random jump is enabled) + certain percent of connected agents weighted
		 * by the formula (this is made similar to challprop-matlab implementation.
		 */

		// first generating random jump:
		def random = new Random().nextDouble()
		if (disableRandomJump == false && random <= probabilityOfRandomJump) {
			Boolean generated = false
			FramedAgent randomAgent
			while (generated != true) {
				randomAgent = network.g.frame(network.g.baseGraph.V('type', 'agent')\
						[new Random().nextInt(network.g.baseGraph.V('type', 'agent').count() as int)].next(), FramedAgent.class)
				if (randomAgent.asVertex().id != agent.asVertex().id) generated = true
			}
			agent.getKnowsLinksToAgent(randomAgent.asVertex().getId()) == [] ?
					createDoubleLink(agent,randomAgent) :
					network.g.frame(agent.getLinkToAgent(randomAgent.getIdentity()),Direction.BOTH,Knows.class)
			listOfAgentsToPropagate.add(randomAgent)
			logger.debug("Generated random jump from {} to {} with probabilityOfRandomJump={}", agent.toString(), randomAgent.toString(), probabilityOfRandomJump)
		}
		random = null
		// get a map of agents sorted by their bexps
		HashMap sortedLinksToAgents = new HashMap<Double,FramedAgent>()
		RealVector situationVector = Utils.deserializeVector(challenge.getVector())
		logger.debug("sorting links for propagation for agent {}", agent.toString())
		knowsLinks.each { knowsLink ->
			RealVector preferenceVector = Utils.deserializeVector(knowsLink.getPreferenceVector())
			logger.debug("calculating bexp for link {}", knowsLink.toString())
			// if a vector is zero, cosine cannot be calculated. In this case ad an equal number to each entry
			preferenceVector = preferenceVector.getL1Norm() ? preferenceVector : preferenceVector.mapAdd(0.01d)
			logger.debug('preferenceVector {}',preferenceVector.toString())
			situationVector = situationVector.getL1Norm() ? situationVector : situationVector.mapAdd(0.01d)
			logger.debug('situationVector {}',situationVector.toString())
			def bexp = situationVector.cosine(preferenceVector) * situationVector.getL1Norm()
			logger.debug('bexp={} == situationVector.cosine(preferenceVector)=={} * situationVector.getL1Norm()={}',bexp, situationVector.cosine(preferenceVector), situationVector.getL1Norm())
			logger.debug("Calculated bexp {} for challenge {}", bexp, challenge.toString())
			def agentToPropagateTemp = knowsLink.getDomain()
			FramedAgent agentToPropagate = network.g.frame(agentToPropagateTemp.asVertex(),FramedAgent.class)
			if (!listOfAgentsToPropagate.contains(agentToPropagate)) {
				sortedLinksToAgents.put(bexp,agentToPropagate)
				logger.debug("added agent {} with bexp {} to sortedLinksToAgents", agentToPropagate.toString(),bexp)
			}
			logger.debug("Put agent {} into the list of sorted agents",agentToPropagate)
		}
		logger.debug('unsortedLinksToAgents of size {}: {}', sortedLinksToAgents.size(), sortedLinksToAgents.toString())
		List sortedLinksToAgentsKeys = new ArrayList(sortedLinksToAgents.keySet());
		if (Parameters.parameters.respectKnowsLinksWeights == true) {
			sortedLinksToAgentsKeys.sort { a, b -> b <=> a }
		} else{
			Collections.shuffle(sortedLinksToAgentsKeys)
		}
		logger.debug('sortedLinksToAgentsKeys of size {}: {}', sortedLinksToAgentsKeys.size(), sortedLinksToAgentsKeys.toString())
		def numberOfAgentsToPropagate = Math.ceil((sortedLinksToAgents.size() * Parameters.parameters.propagateRate).toDouble()).toInteger()
		if (numberOfAgentsToPropagate != 0) {
			sortedLinksToAgentsKeys[0..numberOfAgentsToPropagate - 1].each { key ->
				listOfAgentsToPropagate.add(sortedLinksToAgents.get(key))
			}
		}
		logger.debug("List of {} agents to propagate: {}", listOfAgentsToPropagate.size(), listOfAgentsToPropagate.toString())
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		FramedChallenge newChallenge
		for (FramedAgent agentToPropagate : listOfAgentsToPropagate) {
			newChallenge = situation.addChallengeToAgent(agentToPropagate)
			newChallenge.setIdentity(situation.identity)
			newChallenge.setSource(agent.getIdentity())
			newChallenge.setIdentity(challenge.getIdentity())
			newChallenge.setVector(challenge.getVector())
			newChallenge.setType("challenge")
			logger.debug("Propagated a challenge {} with source {} and identity {} to agent {}", challenge.toString(), challenge.getSource().toString(),challenge.getIdentity(), agentToPropagate)
		}
		if (listOfAgentsToPropagate.size() == 0) {
				FramedChallenge sinkedChallenge = agent.addSinkedChallenge(situation)
				sinkedChallenge.setIdentity(challenge.getIdentity())
				sinkedChallenge.setSource(challenge.getSource())
				sinkedChallenge.setVector(challenge.getVector())
				sinkedChallenge.setIntensity(challenge.getIntensity())
				sinkedChallenge.setType("challenge")

		}
		challenge.setArchived(true)
		logger.warn("=PrapagationPipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
		return newChallenge
	}

	public void createDoubleLink(FramedAgent agent, FramedAgent randomAgent) {
		// this is a kind of revers logic:
		// the main link is from agent TO which the challenge is being propagated
		// and the reciprocal link is FROM the same agent
		// this is because links are interpreted as trust between agents.
		randomAgent.addKnowsAgent(agent)
		logger.debug('created a link from randomAgent {} to agent {}',randomAgent,agent)
		if (Parameters.parameters.reciprocityRate !=0) {
			agent.addKnowsAgent(randomAgent)
			logger.debug('created a reciprocal link from agent {} to randomAgent {}',agent, randomAgent)
		}
	}

}
