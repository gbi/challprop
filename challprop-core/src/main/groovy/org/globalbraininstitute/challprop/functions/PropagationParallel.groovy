package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.actors.ChallengeGenerator
import org.globalbraininstitute.challprop.actors.ParallelAgent
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 * Takes a challenge, decides where to propagate it and 
 * outputs the same situation but linked to an agent.
 * 
 */

public class PropagationParallel implements PipeFunction<FramedChallenge, FramedChallenge>{
	static {
		Gremlin.load()
	  }

	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(Propagation.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		// unimplemented
		// copy from selection Agent based -start
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class)
 		assertNotNull(agent)
		List<Knows> knowsLinks = agent.getKnowsLinks().toList()
		assertNotNull(knowsLinks)
		if (Parameters.parameters.respectKnowsLinksWeights == true) {
			knowsLinks.sort{a,b -> b.weight <=> a.weight}
			logger.debug("Sorted knowsLinks for the propagation respecting weight strenght")
		} else {
			Collections.shuffle(knowsLinks)
			logger.debug("Randomized knowLinks list for the propagation without respect to weight strenght)")
		}
		List<ParallelAgent> listOfAgentsToPropagate = []
		def maxBranchingFactor = Parameters.parameters.maxBranchingFactor

		def randomJumpParameter = agent.asVertex().getProperty("randomJump")
		def disableRandomJump = Parameters.parameters.disableRandomJump
		logger.debug("disable randomJump: {}", disableRandomJump)
		def probabilityOfRandomJump = 1 /( 1 + randomJumpParameter * (agent.getKnowsLinksWeightSum() == null ? 0 : agent.getKnowsLinksWeightSum()))
		logger.debug("randomJump calculation: probabilityOfRandomJump = 1 /( 1 + randomJumpParameter * (agent.getKnowsLinksWeightSum() == null ? 0 : agent.getKnowsLinksWeightSum())")
		logger.debug("randomJump calculation: probabilityOfRandomJump={}", probabilityOfRandomJump)
		logger.debug("randomJump calculation: randomJumpParameter={}", randomJumpParameter)
		logger.debug("randomJump calculation: agent.getKnowsLinksWeightSum()={}", agent.getKnowsLinksWeightSum())
		def numberOfAgentsInTheNetwork = network.g.baseGraph.V('type','agent').count() as int
		listOfAgentsToPropogateGeneration:
		while (listOfAgentsToPropagate.size() < maxBranchingFactor && listOfAgentsToPropagate.size() < numberOfAgentsInTheNetwork-1) {
			if (knowsLinks.size() == 0 && disableRandomJump == true) {
				logger.debug("Breaking due to invalid parameters: knowsLinks.size()={} && disableRandomJump={}", knowsLinks.size(),disableRandomJump)
				break listOfAgentsToPropogateGeneration
			}
			def random = new Random().nextDouble()
			if (disableRandomJump == false && random <= probabilityOfRandomJump) {
				Boolean generated = false
				ParallelAgent randomParallelAgent
				FramedAgent randomAgent
				while (generated != true) {
					randomParallelAgent = network.agentsMap.values().toArray()[new Random().nextInt(Parameters.simulationParameters.NUMBER_OF_AGENTS.toInteger())]
					randomAgent = randomParallelAgent.framedAgent				
					if (randomAgent.asVertex().id != agent.asVertex().id) generated = true
				}
				
				// make a jump depending on the parameters (mood could be also inserted here)
				if (!listOfAgentsToPropagate.contains(randomParallelAgent)) {
					Knows link = agent.getKnowsLinksToAgent(randomAgent.asVertex().getId()) == [] ? \
					agent.addKnowsAgent(randomAgent) :\
					network.g.frame(agent.getLinkToAgent(randomAgent.asVertex().getId()),Direction.BOTH,Knows.class)
					listOfAgentsToPropagate.add(randomParallelAgent)
					logger.debug("Generated random jump from {} to {} with probabilityOfRandomJump={}", agent.toString(), randomAgent.toString(), probabilityOfRandomJump)
				}
		
			} else {
				// takes the first value out of the list  
				Knows knowsLink = knowsLinks[0]
				knowsLinks = knowsLinks.tail()
								 
				RealVector situationVector = Utils.deserializeVector(challenge.getVector())
				RealVector preferenceVector = Utils.deserializeVector(knowsLink.getPreferenceVector())
		
				// if a preferenceVector is zero, cosine cannot be calculated. In this case ad an equal number to each entry
				preferenceVector = preferenceVector.getL1Norm() ? preferenceVector : preferenceVector.mapAdd(0.1d)
		
				// selection of agents to propagate according to the formula from paper version 13/5/13
				def bexp = situationVector.cosine(preferenceVector) * situationVector.getL1Norm()
				logger.debug("Calculated bexp {} for challenge {}", bexp, challenge.toString())
				if (bexp > Parameters.parameters.propagationThreshold) {
					logger.debug("Generated jump through knowsLink {} with probabilityOfRandomJump={}", knowsLink, probabilityOfRandomJump)
					def agentToPropagate = knowsLink.getRange()
					def parallelAgentToPropagate = Globals.network.agentsMap.get(agentToPropagate)
					if (!listOfAgentsToPropagate.contains(parallelAgentToPropagate)) {
						listOfAgentsToPropagate.add(parallelAgentToPropagate)
					}
					logger.debug("Added agent {} to listOfAgentsToPropagate", agentToPropagate.toString())
				} else {
					logger.debug("bexp {} > propagationThreshold {}", bexp, Parameters.parameters.propagationThreshold)
				}
			}
		}	
		
		logger.debug("List of {} agents to propagate: {}", listOfAgentsToPropagate.size(), listOfAgentsToPropagate.toString())
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		FramedChallenge newChallenge
		for (ParallelAgent agentToPropagate : listOfAgentsToPropagate) {
			newChallenge = situation.addChallengeToAgent(agentToPropagate.framedAgent)
			newChallenge.setIdentity(situation.identity)
			newChallenge.setSource(agent.asVertex().getId())
			newChallenge.setIdentity(challenge.getIdentity())
			newChallenge.setVector(challenge.getVector())
			newChallenge.setType("challenge")
			agentToPropagate.send()
			logger.debug("Propagated a challenge {} with identity {} to agent {}", challenge.toString(), challenge.getIdentity(), agentToPropagate.framedAgent)
		}
		if (listOfAgentsToPropagate.size() == 0) {
				FramedChallenge sinkedChallenge = agent.addSinkedChallenge(situation)
				sinkedChallenge.setIdentity(challenge.getIdentity())
				sinkedChallenge.setSource(challenge.getSource())
				sinkedChallenge.setVector(challenge.getVector())
				sinkedChallenge.setIntensity(challenge.getIntensity())
				sinkedChallenge.setType("challenge")
				ChallengeGenerator.decrementNumberOfChallenges() // @vveitas: a challenge becomes non-active when it is processed by the network
		}
		
		challenge.setArchived(true)
		logger.warn("=PrapagationPipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
		return newChallenge
	}
}