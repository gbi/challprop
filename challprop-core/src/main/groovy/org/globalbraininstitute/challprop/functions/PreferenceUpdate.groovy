package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.frames.Knows
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * Takes a reinterpeted challenge and updates the preferenceVector of the Knows link between agents involved
 */

class PreferenceUpdate implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(PreferenceUpdate.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		assertNotNull(challenge)
		assert challenge.asEdge().label == "reinterpreted"
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class) // after interpretation so agent is in the domain
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)

		// Edge knowsEdge = agent.asVertex().query().inE('knows').outV().has("id",fromAgentId).edges().next()

		// preference is glued to the same link that carries trust...
		Edge knowsEdge = agent.getLinkToAgent(challenge.getSource())
		Knows knows = network.g.frame(knowsEdge, Direction.OUT, Knows.class)
		
		RealVector preferenceVector = Utils.deserializeVector(knows.getPreferenceVector())
		def numberOfUpdates = knows.getNumberOfUpdates()
		def weightOfNewUpdate = 1/numberOfUpdates
		def weightOfOldUpdates = 1 - weightOfNewUpdate
		RealVector newPreferenceVector = preferenceVector.mapMultiply(weightOfOldUpdates)
			.add(Utils.deserializeVector(situation.getVector()).mapMultiply(weightOfNewUpdate))
		knows.setPreferenceVector(Utils.serializeVector(newPreferenceVector))

		logger.debug("Updated preferenceVector of the Knows link {} to {}", knows.toString(), newPreferenceVector)
		logger.warn("=PreferenceUpdatePipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
		return challenge
	}

}
