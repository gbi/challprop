package org.globalbraininstitute.challprop.functions

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.analysis.function.Abs
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.*
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.globalbraininstitute.challprop.visual.GephiStreamingServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * Checks whether a challenge is  
 */

class ProcessingByMultiplication implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(ProcessingByMultiplication.class);
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		assertNotNull(challenge)
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class) // after interpretation so agent is in the domain
		assertNotNull(agent)
		RealMatrix processingMatrix = Utils.deserializeMatrix(agent.getProcessingMatrix())
		assertNotNull(processingMatrix)
		RealVector vectorBefore = Utils.deserializeVector(challenge.getVector())
		assertNotNull(vectorBefore)
		
		//calculate benefit
		RealVector vectorAfter = processingMatrix.operate(vectorBefore)
		assertNotNull(vectorAfter)
		List processedChallenges = agent.asVertex().outE('processed').identity.toList()
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		def rivalComponents = situation.getRivalComponents()
		def benefit = 0
		def benefitTemp = vectorBefore.getL1Norm() - vectorAfter.getL1Norm()
		if (challenge.getIdentity() in processedChallenges) {
			// if challenge is already processed, include only rival components to the benefit
			benefitTemp.eachWithIndex{value,index -> if (index in rivalComponents) {benefit+=value}}
			logger.debug("Calculated benefit {} for the agent {}: repeated processing", benefit, agent.toString())
		} else {
			// otherwise include everything
			benefit = benefitTemp
			logger.debug("Calculated benefit {} for the agent {}: first processing", benefit, agent.toString())
		}

		//Modify agent
		// benefit of the agent is a simple sum of all extracted benefits

		DataHolder benefitHolder = agent.addBenefit()
		benefitHolder.setType('benefitHolder')
		benefitHolder.setValue(benefit)

		agent.setBenefit(agent.getBenefit()+benefit)
		//assert agent.getBenefit() == benefits.sum()
		if (Parameters.parameters.gephiVisualization == true ) {
			GephiStreamingServer.postChangeAgent(agent.asVertex())
		}
		logger.debug("Updated benefit for agent {} is {}", agent.toString(), agent.getBenefit())
		// skill vector is a weighted average of all challenges, weighted by benefit extracted
		agent.setSkillVector(Utils.serializeVector(Utils.deserializeVector(agent.getSkillVector()).add(vectorBefore.mapMultiply(benefit))))
		logger.debug("Updated skill vector for agent {} is {}", agent.toString(), agent.getSkillVector())
		
		//Create new instance of a challenge - processed

		FramedChallenge processedChallenge = agent.addProcessedChallenge(situation)
		processedChallenge.setIdentity(challenge.getIdentity())
		processedChallenge.setSource(challenge.getSource())

		//reverse the non-rival components of the vector
		def vectorAfterCorrection = vectorBefore
		if (!Parameters.parameters.ignoreNonRivalCorrection) {
			rivalComponents.each { vectorAfterCorrection.setEntry(it, vectorAfter.getEntry(it)) }
		} else {
			vectorAfterCorrection = vectorAfter
		}

		processedChallenge.setVector(Utils.serializeVector(vectorAfterCorrection))
		processedChallenge.setType("challenge")
		challenge.setArchived(true)
		logger.debug("Processed a challenge {} with source {} and created a processedChallenge {} with identity {}", challenge.toString(), challenge.getSource().toString(), processedChallenge.toString(), challenge.getIdentity())
		
		
		// Reinforcement of links between agents
		// what interests me is the link to source agent, not the link through which the challenge has arrived..

		Edge knowsEdge = agent.getLinkToAgent(challenge.getSource())
		assertNotNull(knowsEdge)
		Knows knows = network.g.frame(knowsEdge, Direction.OUT, Knows.class)

		Double learningRate = agent.getLearningRate()
		def newWeight = knows.getWeight() + learningRate * benefit
		logger.debug("Weight calculation: {} = old weight: {} '+', learningRate: {} * benefit {}", newWeight, knows.getWeight(), learningRate, benefit)
		knows.setWeight(newWeight)
		knows.weightHistory.put(System.nanoTime(),newWeight)
		if (Parameters.parameters.gephiVisualization == true ) {
			GephiStreamingServer.postChangeKnowsLink(knows.asEdge())
		}
		logger.debug("Changed 'knows' link weight: ={},{},{},{}=", knows.toString(), System.nanoTime(), "newWeight", newWeight)
		logger.warn("=ProcessingByMultiplicationPipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)

		// Reinforcing reciprocal link because
		// on the other hand, I also want to reinforce the link through which the challenge arrived..
		Double reciprocityRate = Parameters.parameters.reciprocityRate
		if (reciprocityRate != 0 && reciprocityRate !=null) {
			Edge reciprocalKnowsEdge = agent.getLinkFromAgent(challenge.getSource())
			Knows reciprocalKnows = network.g.frame(reciprocalKnowsEdge, Direction.OUT, Knows.class)
			def newReciprocalWeight = reciprocalKnows.getWeight() + learningRate * benefit * reciprocityRate
			logger.debug("Reciprocal weight calculation: {} = old weight: {} '+', learningRate: {} * benefit {} * reciprocityRetE: {}", newReciprocalWeight, knows.getWeight(), learningRate, benefit, reciprocityRate)
			reciprocalKnows.setWeight(newReciprocalWeight)
			reciprocalKnows.weightHistory.put(System.nanoTime(),newReciprocalWeight)
			if (Parameters.parameters.gephiVisualization == true ) {
				GephiStreamingServer.postChangeKnowsLink(reciprocalKnows.asEdge())
			}
			logger.debug("Changed 'reciprocalKnows' link weight: ={},{},{},{}=", reciprocalKnows.toString(), System.nanoTime(), "newReciprocalWeight", newReciprocalWeight)

		}

		return processedChallenge


		// TODO Transitivity learning maybe (listed as possibility in the model and not implemented by Evo);

	}
}
