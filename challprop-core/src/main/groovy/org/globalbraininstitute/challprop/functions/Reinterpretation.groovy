package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.apache.commons.math3.linear.RealVector
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * Reinterprets a challenge vector which is simply adding challenge vector to the need vector of an agent;
 * But reinterpretation means changing the situation's vector
 * So the new vector is written to the situation (creating an instance of an old one before).  
 * Should be implemented as a TransformFunctionPipe
 */

class Reinterpretation implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(Reinterpretation.class);
	private final String function;
	
	public Reinterpretation(String function) {
		this.function = function;
	}
	
	public FramedChallenge compute(FramedChallenge challenge) {
		def start = System.nanoTime()
		FramedAgent agent = network.g.frame(challenge.getDomain().asVertex(), FramedAgent.class) 
		assertNotNull(agent)
		FramedSituation situation = network.g.frame(challenge.getRange().asVertex(), FramedSituation.class)
		FramedChallenge newChallenge
		if (function == "subtraction") {
			String need = agent.getNeedVector();
			RealVector realNeed = Utils.deserializeVector(need);
			RealVector realChallengeVector = Utils.deserializeVector(challenge.getVector());
			RealVector reinterpretation = realChallengeVector.add(realNeed);
			newChallenge = agent.addReinterpretedChallenge(situation)
			newChallenge.setIdentity(challenge.getIdentity())
			newChallenge.setSource(challenge.getSource())
			newChallenge.setVector(Utils.serializeVector(reinterpretation))
			newChallenge.setType("challenge")
			challenge.setArchived(true)
		}
		logger.debug("Reinterpreted a challenge {} and created a newChallenge {} with identitiy {}", challenge.toString(), newChallenge.toString(), challenge.getIdentity())
		logger.warn("=ReinterpretationPipe with challenge= {}={}", challenge.toString(), System.nanoTime()- start)
		return newChallenge
	}
}
