package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.visual.GephiStreamingServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * According to the principle that an agent loses fitness by not taking action
 * (sort of a cost by just sitting on the network)
 * Decay pipe loops through all agents and decreases their 'accruedBenefit' by a certain amount;
 * In principle this should then be used for making agents with negative 'accruedBenefit' to "die".
 */

public class Decay implements PipeFunction<FramedAgent, FramedAgent> {
	Network network = Globals.network
	private Logger logger = LoggerFactory.getLogger(Decay.class);
	private final decayFactor = Parameters.parameters.decayFactor
		
	public FramedAgent compute(FramedAgent agent) {
			assertNotNull(agent)
			int currentBenefit = agent.getBenefit()
			agent.setBenefit(currentBenefit - decayFactor)
			if (Parameters.parameters.gephiVisualization == true ) {
				GephiStreamingServer.postChangeAgent(agent.asVertex())
			}
			logger.debug("Decay: benefit {} of agent {} decreased by {}", currentBenefit, agent.toString(), decayFactor) 
			return null
	}
}
// TODO consider decay on weight links (what is the reason for that?): each generation links are decreased by percentage (global scan of the graph will be needed!!!)