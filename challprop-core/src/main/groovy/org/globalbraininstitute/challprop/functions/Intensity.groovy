package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.globalbraininstitute.challprop.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 
 * @author vveitas
 * This is a SideEffectFunctionPipe as it just calculates intensity and puts it on the challenge as a property value 
 */

class Intensity implements PipeFunction<FramedChallenge, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(Intensity.class);
	private final String function;
	
	public Intensity(String function) {
		this.function = function;
	}

	public FramedChallenge compute(FramedChallenge challenge) {
		if (function == "absoluteSum") {
			challenge.setIntensity(Utils.deserializeVector(challenge.getVector()).getL1Norm())
		}
		logger.debug("Calculated intensity of a challenge {} with identity {}", challenge.toString(), challenge.getIdentity())
		return challenge
	}
}
