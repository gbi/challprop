package org.globalbraininstitute.challprop.functions

import com.tinkerpop.pipes.PipeFunction
import org.globalbraininstitute.challprop.frames.FramedAgent
import org.globalbraininstitute.challprop.frames.FramedChallenge
import org.globalbraininstitute.challprop.frames.FramedGod
import org.globalbraininstitute.challprop.frames.FramedSituation
import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.objects.Network
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull

/**
 * 
 * @author vveitas
 *
 * randomly assigns a challenge to the any agent in the network
 */

class ChallengeGeneration implements PipeFunction<FramedSituation, FramedChallenge> {
	Network network = Globals.network
	FramedGod god = network.god
	private Logger logger = LoggerFactory.getLogger(ChallengeGeneration.class);
	private final String type
	
	public ChallengeGeneration(String type) {
		this.type = type
	}
	
	public ChallengeGeneration() {
		this.type = "random"
	}
	
	public FramedChallenge compute(FramedSituation situation) {
		assertNotNull(situation)
		// get the random agent in the network
		FramedAgent agent
		switch (type) {
			case "random":
				agent = network.g.frame(network.g.baseGraph.V('type','agent')\
				[new Random().nextInt(network.g.baseGraph.V('type','agent').count() as int)].next(),\
				FramedAgent.class)
				break
		}
		
		// challenge it with the new instance of a situation
		def challenge =  situation.addChallengeToAgent(agent)
		challenge.setIdentity(situation.identity)
		challenge.setSource(god.getIdentity())
		logger.debug("Generated a challenge {} with identity {}", challenge.toString(), challenge.getIdentity())
		return challenge
	}
}

// TODO: implement assigning a percentage of agents (believe) a challenge from GOD each generation (to comply to Evo's simulation); just change newSituationsPerCycle parameter ;
