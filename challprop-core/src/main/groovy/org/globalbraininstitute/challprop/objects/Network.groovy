/**
 * A static class which provides a single access point to the graph database for all classes and methods of the
 * simulation during runtime. Intended for sequentialUpdates branch only
 * @author vveitas
 */

package org.globalbraininstitute.challprop.objects

import com.thinkaurelius.titan.core.Order
import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.frames.FramedGraph
import com.tinkerpop.gremlin.groovy.Gremlin
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.commons.math3.linear.RealMatrix
import org.globalbraininstitute.challprop.actors.ParallelAgent
import org.globalbraininstitute.challprop.frames.*
import org.globalbraininstitute.challprop.frames.annotations.TimedAdjacencyAnnotationHandler
import org.globalbraininstitute.challprop.global.Parameters
import org.globalbraininstitute.challprop.visual.GephiStreamingServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertNotNull;

class Network {
	static {
		Gremlin.load()
	}
	private Logger logger = LoggerFactory.getLogger(Network.class)

	private String links  = "UNDIRECTED"
	public Map global = [:]
	public FramedGraph g
	public FramedGod god
	private Map indices = [:]
	public GephiStreamingServer gephi
	Map <FramedAgent, ParallelAgent> agentsMap = new HashMap<FramedAgent, ParallelAgent>()

	public Network() {

		String pathOnDisk = Parameters.parameters.pathOnDisk
		String graphDatabase = Parameters.parameters.graphDatabase
		String simulationID = Parameters.parameters.simulationID
		String simulationHome = Parameters.parameters.simulationHome
		String graphPath = Parameters.parameters.graphPath
		String experimentPath = Parameters.parameters.experimentPath

		boolean gephiVisualization = Parameters.parameters.gephiVisualization

		global.put("graphDatabase", graphDatabase)
		global.put("simulationID", simulationID)
		global.put("pathOnDisk", pathOnDisk)
		global.put("graphPath", graphPath)
		global.put("experimentPath", experimentPath)

		switch ( graphDatabase ) {
			case "TitanEmbedded":
				this.g = new FramedGraph<TitanGraph>(TitanFactory.open(graphPath))
				break
			case "TitanEmbeddedOnNILFS":
				this.g = new FramedGraph<TitanGraph>(TitanFactory.open(graphPath))
				break
			case "TitanCassandraLocal":
				Configuration conf = new BaseConfiguration();
				conf.setProperty("storage.backend","cassandra");
				conf.setProperty("storage.hostname","127.0.0.1");
				conf.setProperty("cache.db-cache", true);
				conf.setProperty("cache.db-cache-size", 0.5);
				conf.setProperty("cache.db-cache-time", 0);
				//conf.setProperty("storage.batch-loading", true);
				//conf.setProperty("autotype","none");
			    this.g = new FramedGraph<TitanGraph>(TitanFactory.open(conf))
				break
		}

			g.registerAnnotationHandler(new TimedAdjacencyAnnotationHandler())

			if (Parameters.parameters.test == false) {
				g.registerFrameInitializer(FrameInitializers.framedAgent)
				g.registerFrameInitializer(FrameInitializers.framedSituation)
				g.registerFrameInitializer(FrameInitializers.framedSituationInstance)
				g.registerFrameInitializer(FrameInitializers.framedGod)
				g.registerFrameInitializer(FrameInitializers.framedChallenge)
				g.registerFrameInitializer(FrameInitializers.knowsLink)
			} else {
				g.registerFrameInitializer(FrameInitializersForTesting.framedAgent)
				g.registerFrameInitializer(FrameInitializersForTesting.framedSituation)
				g.registerFrameInitializer(FrameInitializersForTesting.framedSituationInstance)
				g.registerFrameInitializer(FrameInitializersForTesting.framedGod)
				g.registerFrameInitializer(FrameInitializersForTesting.framedChallenge)
				g.registerFrameInitializer(FrameInitializersForTesting.knowsLink)
			}

			if (g.baseGraph.getType("timestamp") == null) init()
			if (gephiVisualization == true) {this.gephi = new GephiStreamingServer()}

			def gods = g.baseGraph.V("type", "god")
			def godVertex
			if (gods.hasNext()) {
					godVertex = gods.next()
					god = g.frame(godVertex, FramedGod.class)
			} else {
					god = g.addVertex(null, FramedGod.class)
			}

		assertNotNull(g)

	}

	protected void init() {
		// creating time property and making it a primary key for all vertexes
		def timestamp = g.baseGraph.makeKey('timestamp').single().dataType(Long.class).make()

		g.baseGraph.makeLabel("buffered").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("challenged").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("forgot").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("interpreted").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("reinterpreted").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("processed").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("logged").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("knows").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("created").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("penalized").sortKey(timestamp).sortOrder(Order.DESC).make()
		g.baseGraph.makeLabel("benefitted").sortKey(timestamp).sortOrder(Order.DESC).make()

		// manually creating indexed for all properties - to allow for enabling autotype=none property
		g.baseGraph.createKeyIndex("type", Vertex.class)
		g.baseGraph.createKeyIndex("status", Vertex.class)
		g.baseGraph.createKeyIndex("source", Vertex.class)
		g.baseGraph.createKeyIndex("identity", Vertex.class)
		g.baseGraph.createKeyIndex("value", Vertex.class)
		g.baseGraph.createKeyIndex("vector", Vertex.class)
		g.baseGraph.createKeyIndex("rivalComponents", Vertex.class)
		g.baseGraph.createKeyIndex("bufferSize", Vertex.class)
		g.baseGraph.createKeyIndex("needVector", Vertex.class)
		g.baseGraph.createKeyIndex("benefit", Vertex.class)
		g.baseGraph.createKeyIndex("randomJump", Vertex.class)
		g.baseGraph.createKeyIndex("learningRate", Vertex.class)
		g.baseGraph.createKeyIndex("skillVector", Vertex.class)
		g.baseGraph.createKeyIndex("processingMatrix", Vertex.class)
		g.baseGraph.createKeyIndex("maxBranchingFactor", Vertex.class)
		g.baseGraph.createKeyIndex("densityVectorSituation", Vertex.class)
		g.baseGraph.createKeyIndex("test", Vertex.class)
		g.baseGraph.createKeyIndex("percnegSituation", Vertex.class)
		g.baseGraph.createKeyIndex("weightImportance", Vertex.class)
		g.baseGraph.createKeyIndex("graphDatabase", Vertex.class)
		g.baseGraph.createKeyIndex("believe", Vertex.class)
		g.baseGraph.createKeyIndex("densityMatrix", Vertex.class)
		g.baseGraph.createKeyIndex("believe", Vertex.class)
		g.baseGraph.createKeyIndex("percnegNeed", Vertex.class)
		g.baseGraph.createKeyIndex("numberOfCycles", Vertex.class)
		g.baseGraph.createKeyIndex("dimensions", Vertex.class)
		g.baseGraph.createKeyIndex("newSituationsPerCycle", Vertex.class)
		g.baseGraph.createKeyIndex("exponentSituation", Vertex.class)
		g.baseGraph.createKeyIndex("ignoreNonRivalCorrection", Vertex.class)
		g.baseGraph.createKeyIndex("simulationID", Vertex.class)
		g.baseGraph.createKeyIndex("gephiVisualization", Vertex.class)
		g.baseGraph.createKeyIndex("pathOnDisk", Vertex.class)
		g.baseGraph.createKeyIndex("cstNeed", Vertex.class)
		g.baseGraph.createKeyIndex("propagationThreshold", Vertex.class)
		g.baseGraph.createKeyIndex("exponentNeed", Vertex.class)
		g.baseGraph.createKeyIndex("densityVectorNeed", Vertex.class)
		g.baseGraph.createKeyIndex("useDatabaseOfVectors", Vertex.class)
		g.baseGraph.createKeyIndex("respectKnowsLinksWeights", Vertex.class)
		g.baseGraph.createKeyIndex("decayFactor", Vertex.class)
		g.baseGraph.createKeyIndex("useDatabaseOfMatrixes", Vertex.class)
		g.baseGraph.createKeyIndex("experimentPath", Vertex.class)
		g.baseGraph.createKeyIndex("disableRandomJump", Vertex.class)
		g.baseGraph.createKeyIndex("rfactor", Vertex.class)
		g.baseGraph.createKeyIndex("reciprocityRate", Vertex.class)
		g.baseGraph.createKeyIndex("graphPath", Vertex.class)
		g.baseGraph.createKeyIndex("decayRate", Vertex.class)
		g.baseGraph.createKeyIndex("numberOfAgents", Vertex.class)
		g.baseGraph.createKeyIndex("propagateRate", Vertex.class)
		g.baseGraph.createKeyIndex("cstSituation", Vertex.class)
		g.baseGraph.createKeyIndex("tempConfigFile", Vertex.class)
		g.baseGraph.createKeyIndex("generateTestDataOnDisk", Vertex.class)
		g.baseGraph.createKeyIndex("simulationHome", Vertex.class)
		g.baseGraph.createKeyIndex("archived", Vertex.class)
		g.baseGraph.createKeyIndex("weight", Vertex.class)
		g.baseGraph.createKeyIndex("weightHistory", Vertex.class)
		g.baseGraph.createKeyIndex("numberOfUpdates", Vertex.class)
		g.baseGraph.createKeyIndex("preferenceVector", Vertex.class)
		g.baseGraph.createKeyIndex("intensity", Vertex.class)
		g.baseGraph.createKeyIndex("updatedFromChallengeWithId", Vertex.class)
		g.baseGraph.createKeyIndex("processingVector", Vertex.class)
		g.baseGraph.createKeyIndex("linkToGodWeight", Vertex.class)


	}

	public void populate() {
		for (int num = 0; num < god.asVertex().getProperty("numberOfAgents"); num ++) {
			FramedAgent framedAgent = g.addVertex(null,FramedAgent.class)
			god.addAgentKnows(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			if (Parameters.parameters.reciprocityRate != 0) {
				god.addKnowsAgent(framedAgent).setWeight(Parameters.parameters.linkToGodWeight as Double)
			}
		}
		this.g.baseGraph.commit()
	}

	protected boolean loadLinkMatrix(RealMatrix linkMatrix) {
		int dimension = linkMatrix.getRowDimension()
		Iterable<FramedAgent> verticesIterable = g.getVertices("type", "agent", FramedAgent.class)
		List<FramedAgent> verticesList = verticesIterable.toList()
		for (int column = 0; column < dimension; column ++) {
			for (int row = 0; row < dimension; row ++) {
				double linkStrength = linkMatrix.getEntry(row, column)
				if (linkStrength > 0) {
					verticesList.get(row).addKnowsAgent(verticesList.get(column))
				}
			}
		}
	}

	public FramedChallenge getLastInBuffer(FramedAgent agent) {
		List<FramedChallenge> buffer = agent.getBuffer()
		buffer.sort(true) {it.intensity}
		assert buffer.size() <= agent.getBufferSize()
		return buffer.size() == agent.getBufferSize() ? this.g.frame(buffer.get(0),Direction.OUT,FramedChallenge.class) : null
	}

	public Vertex getGodVertex() {
		return this.g.getVertices("type", "god").iterator().next()
	}

  public List getFramedAgentsList() {
    return this.god.getKnowsAgent().toList()
  }

	public void finalize() {
		this.g.shutdown()
	}

	public void commit() {
		this.g.baseGraph.commit()
	}

	/*
	 * Shuts down the graph and, depending on configuration, returns snapshot number of on the NILFS partition
	 * where the graph can be retrieved if needed (this will be needed for the lab notebook)
	 */
	public void gracefulShutdown() {
		this.g.shutdown()
	}

	public void clear() {
		this.g.baseGraph.V.remove()

	}

	public void reinit() {
		this.g.baseGraph.V.hasNot('type','god').remove()
	}

}
